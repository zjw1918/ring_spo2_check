import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppVersion } from '@ionic-native/app-version';
import { BLE } from '@ionic-native/ble';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { Device } from '@ionic-native/device';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SQLite } from '@ionic-native/sqlite';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Zip } from '@ionic-native/zip';

// 新的fileopener2在低版本android系统上有问题，因此用了老的opener，且降低了目标系统级别25 -> 22

// import { DevicesModalPagePage } from '../pages/devices-modal-page/devices-modal-page';

import { RegistPage } from '../pages/regist/regist';
import { LoginPage } from '../pages/login/login';
import { ForgetPasswordPage } from '../pages/forget-password/forget-password';

// import { RunPage } from '../pages/run/run';
// import { MePage } from '../pages/me/me';
// import { HistoryPage } from '../pages/history/history';
// import { TabsPage } from '../pages/tabs/tabs';
// import { MapPage } from '../pages/map/map';
// import { SettingPage } from '../pages/setting/setting';
// import { UserProfilePage } from '../pages/user-profile/user-profile';
// import { FeedbackPage } from '../pages/feedback/feedback';
// import { IntroducePage } from '../pages/introduce/introduce';
// import { RecordDetailPage } from '../pages/record-detail/record-detail';
// import { DeviceManagerPage } from '../pages/device-manager/device-manager';
// import { ModalAvatarPage } from '../pages/modal-avatar/modal-avatar';

// component
import { DfuComponent } from '../components/dfu-component/dfu-component';
import { HrPlusComponent } from '../components/hr-plus/hr-plus';

// shared providers
import { MyConfig } from '../providers/my-config';
import { AVService } from '../providers/av';
import { BleService, DfuService } from '../providers/ble-service';
import { FileService } from '../providers/file-service';
import { MapService } from '../providers/map-service';
import { GpsService } from '../providers/gps-service';
import { CameraService } from '../providers/camera-service';
import { Utils, ToastService } from '../providers/utils';
import { SqliteService } from '../providers/sqlite-service';

@NgModule({
  declarations: [
    MyApp,
    LoginPage, RegistPage, ForgetPasswordPage,
    // DeviceManagerPage,
    // RunPage, HistoryPage, SettingPage, RecordDetailPage,
    // UserProfilePage, FeedbackPage, IntroducePage,
    // MePage, // only for inner test
    // TabsPage,
    // ModalAvatarPage,
    // MapPage,
    DfuComponent, HrPlusComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    // IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage, RegistPage, ForgetPasswordPage,
    // DeviceManagerPage,
    // RunPage, HistoryPage, SettingPage, RecordDetailPage,
    // UserProfilePage, FeedbackPage, IntroducePage,
    // MePage, // only for inner test
    // TabsPage,
    // ModalAvatarPage,
    // MapPage
  ],
  providers: [
    StatusBar, SplashScreen,
    AppVersion,
    BLE,
    Camera,
    Crop,
    Device,
    File,
    FileChooser,
    FilePath,
    Geolocation,
    Network,
    PhotoViewer,
    SQLite,
    FileTransfer,
    Zip,
    ToastService, SqliteService,
    MyConfig, AVService, BleService, FileService, Utils, MapService, GpsService, CameraService, DfuService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
