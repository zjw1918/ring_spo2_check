import { Component, NgZone } from '@angular/core';
import { App, Platform, AlertController, Events, ToastController } from 'ionic-angular';
// import { StatusBar, Splashscreen } from 'ionic-native'; //, Transfer
// import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

// 将引入的native service初始化，例如ble, file
import { AVService } from '../providers/av';
import { BleService } from '../providers/ble-service';
import { FileService } from '../providers/file-service';
import { MyConfig } from '../providers/my-config';
import { Utils } from '../providers/utils';

// env config
import { EventsMsg } from '../config/portAndcmd';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = null;
  confirm = null;

  constructor(
    private statusBar: StatusBar, splashScreen: SplashScreen,
    private utils: Utils,
    private toastCtrl: ToastController,
    private zone:NgZone,
    private events: Events,
    private av: AVService,
    private appCtrl: App,
    private alertCtrl: AlertController,
    private cfg: MyConfig,
    private fs: FileService,
    private ble: BleService,
    private platform: Platform)
  {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.registerBackButtonListener();

      // 初始化一些方法，类似检查更新
      // if (!this.utils.offline) {
        // this.cfg.checkUpdate();
      // }

      WifiWizard.setWifiEnabled(true, (data) => {
        console.log('setWifiEnabled ok: ', data)
      }, err => console.error('setWifiEnabled error: ', err));
    });
    
    this.av.init();
    this.checkPreviousAuth();
  }

  ngOnInit() {
    this.events.subscribe(EventsMsg.userEvent, (type) => {
      switch (type) {
        case EventsMsg.userEvent_login:
          this.zone.run(() => this.rootPage = 'SpoTabs');
          break;
        case EventsMsg.userEvent_logout:
          console.log('收到退出请求！');
          this.zone.run(() => this.rootPage = LoginPage);
          break;

        default:
          break;
      }
    });
  }

  // 检查登录与否
  checkPreviousAuth() {
    let currentUser = this.av.getCurrentUser();
    if (currentUser) {
      this.rootPage = 'SpoTabs';
    } else {
      // this.rootPage = LoginPage;
      this.rootPage = 'SpoHome';
    }
  }

  registerBackButtonListener() {
    this.platform.registerBackButtonAction(() => {
      if (this.appCtrl.getRootNav().canGoBack()) {
        this.appCtrl.getRootNav().pop();
      } else if (this.appCtrl.getActiveNav().canGoBack()){
        this.appCtrl.getActiveNav().pop();
      } else {
        this.fs.logSaveRest('');
        this.confirmExitApp();
      }
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

// clear method
  confirmExitApp() {
    if (this.confirm != null) {
      return;
    }
    this.confirm = this.alertCtrl.create({
      title: '退出',
      message: '确认退出？',
      buttons: [
        {
          text: '否',
          handler: () => {
            this.confirm = null;
            console.log('Disagree clicked');
          }
        },
        {
          text: '是',
          handler: () => {
            console.log('Agree clicked');
            setTimeout(() => {
              this.ble.disconnect(this.ble.currentDeviceId).then(() => console.log('执行退出前断开ble成功')).catch(err => console.error(err));
              this.platform.exitApp();
            }, 1000);
          }
        }
      ]
    });
    this.confirm.present();
  }


}
