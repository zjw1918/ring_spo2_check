import { Component } from '@angular/core';
import { BleService } from '../../providers/ble-service';

const DfuType = {
  bootloader: 2,
  application: 4
}

@Component({
  selector: 'dfu-component',
  templateUrl: 'dfu-component.html'
})
export class DfuComponent {
  constructor(private ble: BleService) {
    console.log('Hello DfuComponent');
  }

  private payload;

  updateFwOnline() {
    this.ble.isDfuing = true;
    this.ble.checkFwUpdatePromise().then((res) => {
      this.startDfu(res[0], res[1], res[2], res[3]);
    }).catch(err => console.error(err));
  }

  startDfu(updateType, sentLength, payload, initPack) {
    this.payload = payload;
    console.log('初始化DFU中...');
    if (this.ble.currentDevice['services'] && this.ble.currentDevice.services.length === 3) {
      // 设备目前正处于dfu模式，直接进行dfu
      this.runDfu(updateType, sentLength, initPack);
    } else {
      // 设备目前处于普通模式，需要重启，以进入dfu模式
      this.ble.readDfu().then(buffer => {
        console.log('Version: ', new Uint8Array(buffer));
        this.ble.listenDfuOn().subscribe(
          () => {},
          err => console.error(err)
        );

        setTimeout(() => {
          this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
            console.log('第1次写入成功: ', [0x01, DfuType[updateType]]);
          }).catch(err => console.error(err));

          setTimeout(() => {
            this.secondConnect(updateType, sentLength, payload, initPack);
          }, 2000);
        }, 1000);
      }).catch(err => console.error(err));
    }
  }
  secondConnect(updateType, sentLength, payload, initPack) {
    this.ble.scanDfu().then(deviceId => {
      this.ble.connect(deviceId).subscribe(peripheral => {
        console.log('第2次连上OK: ', peripheral);
        setTimeout(() => {
          this.runDfu(updateType, sentLength, initPack);
        }, 2000);
      }, err => console.error(err));
    }).catch(err => console.error(err));
  }

  runDfu(updateType, sentLength, initPack) {
    console.log('DFU process started >>>');
    this.ble.readDfu().then(buffer => {
      console.log('版本：', new Uint8Array(buffer));
      this.ble.listenDfuOn().subscribe((buffer) => {
        if (buffer) {
          let info = new Uint8Array(buffer);
          console.log('ble 返回的状态：', info);
          switch (info.toString()) {
            case [0x10, 0x01, 0x01].toString():
              console.log('进入第一个noti判断了。');
              this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x00]).buffer).then(()=>{
                console.log('Writing Initialize DFU Parameters...', [2, 0]);
                this.ble.writeDfuPack(initPack.buffer).then(()=>{
                  console.log('Sent header package OK: ', initPack);
                  this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x01]).buffer).then(() => {
                    console.log('Sent cmd OK: ', [2, 1]);
                  }).catch(err => console.error(err));
                }).catch(err => console.error(err));
              }).catch(err => console.error(err));
              break;
            case [0x10, 0x02, 0x01].toString():
                this.ble.writeDfuCtrl(new Uint8Array([0x03]).buffer).then(()=>{
                  console.log('Sent cmd OK: ', [3]);

                  // ble, 终于要给你丫送数据了，接着吧
                  this.sendPayload();
                }).catch(err => console.error(err));
                break;
            case [0x10, 0x03, 0x01].toString():
              this.ble.writeDfuCtrl(new Uint8Array([0x04]).buffer).then(()=>{
                console.log('Sent cmd OK: ', [4]);
              }).catch(err => console.error(err));
              break;
            case [0x10, 0x04, 0x01].toString():
              this.ble.writeDfuCtrl(new Uint8Array([0x05]).buffer).then(()=>{
                console.log('Sent cmd OK: ', [5]);
                this.ble.listenDfuOff().then(() => console.log('关闭notify成功，DFU完成'))
                  .catch(err => console.error(err));

                // this.dfuT2 = Date.now();
                // this.dfuLog += '共用时：' + (this.dfuT2 - this.dfuT1)/1000 + 's';
                // console.log('共用时：' + (this.dfuT2 - this.dfuT1)/1000 + 's');
                
                // this.dfuLog = null;
                // this.currentDevice = null;
              }).catch(err => console.error(err));
              break;

            default:
              if (info[0] === 0x11) {
                let alreadyReceiveLength = info[1] | (info[2] << 8) | (info[3] << 16) | (info[4] << 24);
                console.log('ble报告-已收到：', alreadyReceiveLength, '总：' + sentLength);
                this.sendPayload();
              } else {
                console.error('dfu出现未识别命令');
              }
              break;
            }
          }
        }, err => console.error(err));

      setTimeout(() => {
        this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
          console.log('第2次写入成功:', [0x01, DfuType[updateType]]);
          let data;
          if (updateType.startsWith('a')) {
            data = new Uint32Array([0, 0, sentLength]);
          } else if(updateType.startsWith('b')) {
            data = new Uint32Array([0, sentLength, 0]);
          }
          this.ble.writeDfuPack(data.buffer).then(() => console.log('Sent length pack OK.', data)).catch(err => console.error(err));
        }).catch(err => console.error(err));
      }, 1000);
    }).catch(err => console.error(err));
  }

  sendPayload () {
    let t1 = Date.now();
    let count = 0;
    // let totalLength = this.payload.length;

    let onError = (err) => {
      console.error(err);
    }
    let callBack = () => {
      if (!this.payload[count]) {
        console.log('All sent.');
        this.payload = null;
        this.ble.isDfuing = false;
        console.log((Date.now() - t1)/1000 + 's');
        return;
      }
      let tt1 = Date.now();
      this.ble.writeDfuPackNoResp(this.payload[count++].buffer).then(() => {
        console.log(Date.now() - tt1);
        callBack();
      }).catch(onError);
    }
    this.ble.writeDfuPackNoResp(this.payload[count++].buffer).then(() => callBack()).catch(onError);
  }
}
