import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoModalDevices } from './spo-modal-devices';

@NgModule({
  declarations: [
    SpoModalDevices,
  ],
  imports: [
    IonicPageModule.forChild(SpoModalDevices),
  ],
  exports: [
    SpoModalDevices
  ]
})
export class SpoModalDevicesModule {}
