import { Component } from '@angular/core';
import { IonicPage, ViewController, Events } from "ionic-angular";
import { BleService } from "../../providers/ble-service";

import { EventsMsg } from '../../config/portAndcmd';
import { ToastService } from "../../providers/utils";

@IonicPage()
@Component({
  selector: 'spo-modal-devices',
  templateUrl: 'spo-modal-devices.html'
})
export class SpoModalDevices {
  text: string;

  constructor(
    private toastSvc: ToastService,
    private events: Events,
    private ble: BleService,
    private viewCtrl: ViewController
  ) {
    console.log('Hello SpoModalDevices Component');
    this.text = 'Hello World';
  }

 dismiss() {
   this.ble.clearScannerInfinite();
   this.ble.listDevices = [];
   this.viewCtrl.dismiss();
 }

 startScan() {
   this.ble.startScanInfinite();
 }

 selectDecice(device) {
    console.log(device);
    this.dismiss();
    this.toastSvc.createLoader('连接中...', 8000);
    this.toastSvc.loader.present();

    setTimeout(() => {
      this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, device.mac);
    }, 1000);
 }

}
