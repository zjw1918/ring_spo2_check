export const presentToast = (toastCtrl, msg: string, seconds?: number) => {
  let toast = toastCtrl.create({
    message: msg,
    duration: seconds || 2000
  });
  toast.present();
}


// const configs
export const CmdPort = {
  fakeBind      : 0xb0,
  reset         : 0xe2,
  liveCtrl      : 0xed,
  logSwitch     : 0xf0,
  setTime       : 0xe0,
  setUserInfo   : 0xe3, //  30, 0, 170, 60, 0
  findMe        : 0xb1,
  runCtrl       : 0xd0,
  runSpoLive    : 0xd7,
  syncData      : 0xeb,

  notiBatt      : 0xd2,
  notiStep      : 0xe9,
  heartBeat     : 0xd3,
}

export const CmdCtrl = {
  liveStart : 0,
  liveStop  : 1,

  runOff    : 0,
  runOn     : 1,
  runPause  : 2,

  sportData : 0xef,
}

export const BlePort = {
  // dfu port
  dfu_service_uuid  : '00001530-1212-efde-1523-785feabcd123',
  dfu_pack_uuid     : '00001532-1212-efde-1523-785feabcd123',
  dfu_ctrl_uuid     : '00001531-1212-efde-1523-785feabcd123',
  dfu_version_uuid  : '00001534-1212-efde-1523-785feabcd123',

  // ring port
  ring_service_uuid : 'FAB1',
  ring_w_uuid       : 'FAB2',
  ring_wn_uuid      : 'FAB3',
  ring_ind_uuid     : 'FAB4',
  ring_not_uuid     : 'FAB5',
  ring_r_uuid       : 'FAB6',
}

// export const CmdBuffer = {
//   startLivebuffer: new Uint8Array([0xed, 0, 0, 0]),
//   stopLivebuffer: new Uint8Array([0xed, 0, 0, 1])
// }

export const StorageKey = {
  appVersion        : 'appVersion',
  bootloaderVersion : 'bootloaderVersion',
  boundDevice       : 'boundDevice',
  boundDeviceRandom : 'boundDeviceRandom'
}

export const EventsMsg = {
  eventUpdateUserInfo       : 'eventUpdateUserInfo',

  eventScan                 : 'scan',

  eventConnect              : 'connection',
  eventConnect_connected    : 'connected',
  eventConnect_disConnected : 'disConnected',

  userEvent                 : 'userevent',
  userEvent_login           : 'login',
  userEvent_logout          : 'logout',

  globalSteps               : 'globalsteps',
  globalSteps_connect       : 'connect',
  globalSteps_bindOk        : 'bindOk',
  globalSteps_readOk        : 'readOk',
  globalSteps_setTimeOk     : 'setTimeOk',
  globalSteps_setUserInfoOk : 'setUserInfoOk',
  globalSteps_toUpdateFw    : 'toUpdateFw',
  
  live                      : 'live',
  log                       : 'log',

  // 临时， 在设备管理页，手动点击升级，后续按理要做成后台升级
  updateProgressFw          : 'updateProgressFw',
  updateDeviceManagerPage   : 'updateDeviceManagerPage'
}

// ionViewDidLoad	
// ionViewWillEnter	
// ionViewDidEnter	
// ionViewWillLeave	
// ionViewDidLeave	
// ionViewWillUnload	
// ionViewCanEnter
// ionViewCanLeave