export class UserInfo {
  userId     = null;
  avatarPath = null;
  name       = null;
  gender     = null;
  age        = null;
  height     = null;
  weight     = null;

  // constructor(userId, avatarPath, name, gender, age, height, weight) {
  //   this.userId     = userId;
  //   this.avatarPath = avatarPath;
  //   this.name       = name;
  //   this.gender     = gender;
  //   this.age        = age;
  //   this.height     = height;
  //   this.weight     = weight;
  // }

  // 持久化用户信息到本地
  persistUserInfo() {
    let obj = {
      userId     : this.userId,
      avatarPath : this.avatarPath,
      name       : this.name,
      gender     : this.gender,
      age        : this.age,
      height     : this.height,
      weight     : this.weight
    }
    localStorage.setItem(this.userId + '.info', JSON.stringify(obj));
  }

  // 清空本地持久化用户信息
  trancateUserInfo() {
    localStorage.removeItem(this.userId + '.info');
  }

  // 从本地缓存中恢复哟用户身体信息
  loadLocalUserInfo(userId: string) {
    let userInfoStr: string = localStorage.getItem(userId + '.info');
    if (userInfoStr) {
      let obj = JSON.parse(userInfoStr);
      this.userId     = obj.userId;
      this.avatarPath = obj.avatarPath;
      this.name       = obj.name;
      this.gender     = obj.gender;
      this.age        = obj.age;
      this.height     = obj.height;
      this.weight     = obj.weight;
    }
  }

}