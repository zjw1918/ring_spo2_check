export class BoundInfo {
    userId  = null;
    mac     = null;
    random  = null;

    persistBoundInfo() {
        let obj = {
            userId : this.userId,
            mac    : this.mac,
            random : this.random
        }
        localStorage.setItem(this.userId + '.dvc', JSON.stringify(obj));
    }

    // 清空本地持久化用户信息
    trancateBoundInfo() {
        localStorage.removeItem(this.userId + '.dvc');
    }

    // 从本地缓存中恢复哟用户身体信息
    loadLocalBoundInfo(userId: string) {
        let boundInfoStr: string = localStorage.getItem(userId + '.dvc');
        if (boundInfoStr) {
            let obj = JSON.parse(boundInfoStr);
            this.userId = obj.userId;
            this.mac    = obj.mac;
            this.random = obj.random;
        }
    }

}