import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, InfiniteScroll, Refresher } from 'ionic-angular';

import { SqliteService, TABLES } from '../../providers/sqlite-service';
// import { RecordDetailPage } from '../record-detail/record-detail';
import { AVService } from '../../providers/av';
import { ToastService, parseBleSpo, Utils } from '../../providers/utils';

@IonicPage()
@Component({
  selector: 'page-spo-history',
  templateUrl: 'spo-history.html',
})
export class SpoHistory {
  records;

  constructor(
    private utils: Utils,
    private toastSvc: ToastService,
    private avSvc: AVService,
    private sqliteSvc: SqliteService,
    private zone: NgZone,
    public navCtrl: NavController)
  {

  }

  ngOnInit() {
    
  }

  openDetail(record) {
    console.log('startAt: ' + record.startAt + ' endAt:' + record.endAt);
    let a = this.sqliteSvc.getBleRecord(record.startAt, record.endAt).then((res) => {
      console.log(res);
      // return res ? parseBleSpo(res['data']) : null;
      if (res) {
        console.log('本地sql db 有相应记录');
        return res['data'] ? parseBleSpo(res['data']) : null;
      } else {
        // 本地数据库没有相应的ble记录，判断是否需要联网获取？
        console.log('本地sql db 无相应记录，联网获取中。');
        if (this.utils.offline) {
          // 不要弹出网络错误，没有就没有算了，可以不展示ble运动
          console.log('无网络');
          return null;
        }

        return this.sqliteSvc.getOneRemoteBleRecord(record.startAt, record.endAt).then(bleRecord => {
          return bleRecord['data'] ? parseBleSpo(bleRecord['data']) : null;
        }).catch(err => console.error(err));
      }
    }).catch(err => console.error(err));

    let b = this.sqliteSvc.getAppRecord(record.startAt).then(res => {
      console.log(res);
      return res;
    }).catch(err => console.error(err));

    Promise.all([a, b]).then(resArr => {
      console.log(resArr);
      
      let res = resArr[1];
      console.log(res);
      if (!res['sportRoute']) {
        this.toastSvc.createLoader("加载中...", 5000);
        this.toastSvc.loader.present();
        if (this.utils.offline) {
          this.toastSvc.loader.setContent('未联网');
          setTimeout(() => {
            this.toastSvc.loader.dismiss();
          }, 300);
          return;
        }
        console.log('no sportRoute, get from servering...');
        this.avSvc.getOneRemoteAppRecordRoute(res['startAt']).then(routeArr => {
          let sportRoute: Array<any> = routeArr[0].get('sportRoute');
          res['sportRoute'] = sportRoute;
          console.log(sportRoute);
          this.sqliteSvc.updateRoute(TABLES.app_records, record.startAt, sportRoute);
          this.toastSvc.loader.dismiss();
          this.navCtrl.push('SpoDetail', {data: res, ble: resArr[0]});
        }).catch(err => console.error(err));
      } else {
        console.log('sqlite has sportRoute.');
        this.navCtrl.push('SpoDetail', {data: res, ble: resArr[0]});
      }

    }).catch(err => console.error(err));
  }

  doRefresh(refresher: Refresher) {
    console.log('Begin async operation...');
    this.sqliteSvc.queryFirst(TABLES.app_records).then((res) => {
      let length = res.rows.length;
      console.log('记录长度：', length);
      if (length > 0) {
        let latestRecord = res.rows.item(0);
        console.log(latestRecord);

        if (this.utils.offline) {
          refresher.complete();
          this.toastSvc.makeToast('网络出了点问题');
          return;
        }
        this.sqliteSvc.loadRemoteAppRecordsNoRoute_10_upToLatest(latestRecord.startAt).then((remoteHasData) => {
          // refresher.complete();
          if (remoteHasData) {
            // 数据多，再次进行refresh 更新
            this.doRefresh(refresher);
          } else {
            // 远程数据被同步光了，这时候停止更新 |=> 是否要上传同步记录，请思考
            console.log('远程数据被同步光了, 等待本地未同步数据上传...');
            this.sqliteSvc.syncAppRecordsLater();
            this.sqliteSvc.syncBleRecordsLater();
            refresher.complete();
            // refresher.complete();
          }
        }).catch(err => {
          refresher.complete();
          console.error(err);
        });
      } else {
        console.log('未查询到本地首条记录！');
        refresher.complete();
      }
    }).catch(err => console.error(err));
  }

  doInfinite(infiniteScroll: InfiniteScroll) {
    console.log('Infinite load data starts...');
    this.sqliteSvc.find_10_app_records().then(localHasData => {
      if (localHasData) {
        infiniteScroll.complete();
      } else {
        // 此时本地数据库没有更多数据了，请判断是否要联网获取更多数据；
        if (this.utils.offline) {
          infiniteScroll.complete();
          infiniteScroll.enable(false);
          console.log('没网，无法联网加载记录');
          this.toastSvc.makeToast('网络出了点问题');
          setTimeout(() => {
            infiniteScroll.enable(true);
          }, 2000);
          return;
        }

        this.sqliteSvc.loadRemoteAppRecordsNoRoute_10().then((remoteHasData) => {
          if (remoteHasData) {
            infiniteScroll.complete();
          } else {
            infiniteScroll.enable(false);
            console.log('数据加载完毕');
          }
        }).catch(err => {
          console.error(err);
        })
      }
    }).catch(err => console.error(err));
  }


}
