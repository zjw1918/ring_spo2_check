import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoHistory } from './spo-history';

@NgModule({
  declarations: [
    SpoHistory,
  ],
  imports: [
    IonicPageModule.forChild(SpoHistory),
  ],
  exports: [
    SpoHistory
  ]
})
export class SpoHistoryModule {}
