import { Component } from '@angular/core';
import { NavController, ToastController, ViewController, Events } from 'ionic-angular';

// provider
import { AVService } from '../../providers/av';
import { Utils } from '../../providers/utils';
import { FileService } from '../../providers/file-service';

// config
import { EventsMsg } from '../../config/portAndcmd';

@Component({
  selector: 'page-regist',
  templateUrl: 'regist.html'
})
export class RegistPage {
  // type;
  // title;
  // btnTitle;
  user = {
    phoneNumber:null,
    captcha:null,
    password:null
  };
  showCaptchaBtn = true;
  countDown: number;

  constructor(
    private fs: FileService,
    private utils: Utils,
    public events: Events,
    public toastCtrl: ToastController,
    public av: AVService,
    public navCtrl: NavController,
    public viewCtrl: ViewController
  ) { 

  }

  ionViewDidLoad() {
    console.log('Hello RegistPage Page');
  }

  ionViewDidLeave() {

  }

  // 注册前请求验证码。先验证该手机号码注册过没
  getCaptcha(user) {
    if (this.utils.offline) {
      this.toast('无网络，请检查网络连接', 2000);
      return;
    }

    this.delayCaptcha(10);
    console.log(user);

    this.av.isRegister(user.phoneNumber).then(registed => {
      console.log(registed);
      
      if (registed) {
        this.toast('该号码已注册', 2000);
      } else {
        this.av.requestSmsCode({"mobilePhoneNumber": user.phoneNumber}).subscribe(
          () => console.log('请求-注册-验证码已发送'),
          err => {
            console.log(err);
            alert(JSON.stringify(err));
          }
        );
      }
    }).catch(err => console.error(err));
  }

  isLoginning = false;
  createUser(user) {
    if (this.utils.offline) {
      this.toast('无网络，请检查网络连接', 2000);
      return;
    }

    this.isLoginning = true;
    let postContent = {
      "mobilePhoneNumber" : user.phoneNumber,
      "smsCode"           : user.captcha,
      "password"          : user.password
    }
    this.av.createUser(postContent).subscribe(() => {
      // 创建用户成功，立即登录
      this.av.login(user.phoneNumber, user.password).then(() => {
        this.av.createMultiRemoteSingletonClass().then(() => {
          this.events.publish(EventsMsg.userEvent, EventsMsg.userEvent_login);
        }).catch(err => this.fs.logSaveErr(JSON.stringify(err)));
      }, err => {
        console.error(err);
        alert(JSON.stringify(err));
      });
    }, err => {
      this.isLoginning = false;
      console.error(err);
      alert(JSON.stringify(err));
    });
    
  }
  
  /**
   * @param sec 请求验证码间隔秒数
   */
  delayCaptcha(sec: number) {
    this.countDown = sec;
    this.showCaptchaBtn = false;
    let timer = setInterval(() => {
      this.countDown--;

      if (this.countDown <= 0) {
        clearInterval(timer);
        this.countDown = sec;
        this.showCaptchaBtn = true;
      }
    }, 1000);
  }

  // utils
  // presentLoading(msg: string) {
  //   this.loader = this.loadingCtrl.create({
  //     content: msg,
  //     duration: 10000
  //   });
  //   this.loader.present();
  // }

  toast(msg: string, sec: number) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: sec,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
