import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

// provider
@IonicPage()
@Component({
  selector: 'page-spo-tabs',
  templateUrl: 'spo-tabs.html'
})

export class SpoTabs {

  tab1Root: any = 'SpoHome';
  tab2Root: any = 'SpoHistory';
  tab3Root: any = 'SpoSettings';
  // tab4Root: any = MePage;
  
  constructor() {

  }


}
