import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoTabs } from './spo-tabs';

@NgModule({
  declarations: [
    SpoTabs,
  ],
  imports: [
    IonicPageModule.forChild(SpoTabs),
  ],
  exports: [
    SpoTabs
  ]
})
export class SpoTabsModule {}
