import { Component } from '@angular/core';
import { App, NavController, Events, ToastController } from 'ionic-angular';

import { RegistPage } from '../../pages/regist/regist';
import { ForgetPasswordPage } from '../../pages/forget-password/forget-password';

// provider
import { FileService } from "../../providers/file-service";
import { AVService } from '../../providers/av';
import { EventsMsg } from '../../config/portAndcmd';
import { Utils } from '../../providers/utils';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  user = {
    phoneNumber: null, password: null
  }

  formHidden;

  constructor(
    private utils: Utils,
    private toastCtrl: ToastController,
    private events: Events,
    private appCtrl: App,
    private av: AVService,
    private fs: FileService,
    public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello LoginPage Page');
  }

  ionViewDidEnter() {
    this.formHidden = false;
  }

  isLoginning = false;
  async login() {
    if (this.utils.offline) {
      this.toast('无网络，请检查网络连接', 2000);
      return;
    }

    this.isLoginning = true;
    try {
      this.fs.logSaveInfo('Login ready to start >>>');
      await this.av.login(this.user.phoneNumber, this.user.password);
      this.fs.logSaveInfo('Login complete <<<');
      setTimeout(() => {
        this.events.publish(EventsMsg.userEvent, EventsMsg.userEvent_login); 
      }, 1000);
    } catch (err) {
      this.isLoginning = false;
      this.handleAVError(err);
    }

    // this.av.login(this.user.phoneNumber, this.user.password).then(() => {
    //   // 基本登录 ok，暂不能跳转，需要初始化用户[身体信息，绑定信息]
    //   setTimeout(() => {
    //     this.events.publish(EventsMsg.userEvent, EventsMsg.userEvent_login); 
    //   }, 1000);
    // }, err => {
    //   this.isLoginning = false;
    //   this.handleAVError(err);
    // })
  }

  resetPassword() {
    this.formHidden = true;
    this.navCtrl.push(ForgetPasswordPage);
  }

  createAccount() {
    this.formHidden = true;
    this.navCtrl.push(RegistPage, {}); //  { animate: false }
  }

  handleAVError(err) {
    console.error(err);
    if (err.hasOwnProperty('code')) {
      alert(this.av.AVError[err.code]);
    } else {
      alert(JSON.stringify(err));
    }
  }

  toast(msg: string, sec: number) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: sec,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
