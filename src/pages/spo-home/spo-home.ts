import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, Events } from 'ionic-angular';

import { AVService } from "../../providers/av";
import { FileService } from "../../providers/file-service";
import { SqliteService, AppRecord, SYNC } from "../../providers/sqlite-service";
import { BleService } from "../../providers/ble-service";
import { GpsService } from "../../providers/gps-service";

import { CmdPort, CmdCtrl, EventsMsg } from "../../config/portAndcmd";
import { ToastService, splitArray, drawOneEchart } from '../../providers/utils';

// 3rd libs
import moment from 'moment';
import echarts from 'echarts';

@IonicPage()
@Component({
  selector: 'page-spo-home',
  templateUrl: 'spo-home.html',
})
export class SpoHome {
  sportType = 0; // default 0: outroom

  loader = null;
  currentDevice = null;
  totalSteps;
  hrValue = null;
  o2Value = null;
  myChartO2;
  myChartHr;
  subscriberRunningHr;

  trainningToggle;
  bar1;

  isLiving: boolean;

  // 运动状态下需要的参数
  dynamicDuration: string;  // 跑了多长时间 formate: 00:01:12
  // dynamicDist;      // 跑了多少距离 formate: 1.88km
  // dynamicHr;        // 跑步实时心率 formate: 109
  // dynamicPace;      // 跑步实时配速 formate: 10'21"
  sportIntervalWatcher;
  start_time: number;
  runningTime: number;
  pauseCounter: number;

  runningStatus = 'running-prepare'; // running status
  runningStatusChoose = {
    'running_prepare'   : 'running-prepare',
    'running_running'   : 'running-running',
    'running_pause'     : 'running-pause',
    'running_stop'      : 'running-stop'
  }

  constructor(
    private events: Events,
    private gpsSvc: GpsService,
    private zone: NgZone,
    private toastSvc: ToastService,
    private ble: BleService,
    private sqliteSvc: SqliteService,
    private av: AVService,
    private fs: FileService,
    public modalCtrl: ModalController, private platform: Platform,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpoHome');
    this.platform.ready().then(async () => {
      // this.av.initUserPath();
      // this.fs.initLocalLog();
      // this.sqliteSvc.initDB();
      // await this.av.initUserInfo();
    }).catch(err => console.error(err));

    // let startAt = localStorage.getItem('startAt');
    // if (startAt) {
    //   this.recoverRunning();
    // }

    this.events.subscribe(EventsMsg.live, () => {
      this.liveSwitch('on');
    });
  }

  ionViewDidEnter() {
    console.log('SpoHome ionViewDidEnter');
    if (this.myChartO2) {
      this.myChartO2.resize();
      this.myChartHr.resize();
    }
  }

  openScan() {
    let profileModal = this.modalCtrl.create('SpoModalDevices');
    profileModal.present();
  }

  openUpdate() {
    this.navCtrl.push('SpoSettings');
  }

  liveSwitch(arg: string) {
    switch (arg) {
      case 'on':
        this.isLiving = true;
        this.ble.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStart);
        setTimeout(() => {
          this.ble.exec(CmdPort.runSpoLive+'')
        }, 1);
        this.initChart();
        break;
    
      case 'off':
        this.isLiving = false;
        this.ble.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStop);
        break;
    
      default:
        break;
    }
  }

  initChart() {
    let keys1 = [], values1 = [];
    let index = 100;
    for (let i = 0; i < index; i++) {
      keys1.push(i);
      values1.push('');
    }
    let keys2 = keys1.concat([]), values2 = values1.concat([]);
    // 初始化echarts
    this.myChartO2 = echarts.init(document.getElementById('chart_o2'));
    this.myChartHr = echarts.init(document.getElementById('chart_pr'));
    drawOneEchart(this.myChartO2, keys1, values1, 40, 100, 30, '#ff0000', null); // spo2
    drawOneEchart(this.myChartHr, keys2, values2, 30, 240, 105, '#387ef5', null); // pr

    // 开始监听
    this.ble.$_live.subscribe(a => {
      this.zone.run(() => {
        this.o2Value = a[3];
        this.hrValue = a[4];
      });

      keys1.shift();        values1.shift();
      keys1.push(index++);  values1.push(a[3]);

      keys2.shift();        values2.shift();
      keys2.push(index++);  values2.push(a[4]);

      if (this.myChartO2) {
        this.myChartO2.setOption({
          xAxis:{
            data: keys1
          },
          series: [{
            data: values1
          }]
        });
        this.myChartHr.setOption({
          xAxis:{
            data: keys2
          },
          series: [{
            data: values2
          }]
        });
      }
    }, err => console.error(err));
  }

  //recover running
  recoverRunning() {
    this.runningStatus = this.runningStatusChoose.running_running;
    this.subscriberRunningHr = this.ble.$_live.subscribe(a => {
      this.zone.run(() => {
        this.o2Value = a[3];
        this.hrValue = a[4];
      });
    });

    this.start_time = +localStorage.getItem('startAt');

    setTimeout(() => {
      this.fs.readAsText(this.fs.appRootPath, this.start_time+'.obj').then((objStr) => {
        let persistSportObj = JSON.parse(objStr);
        this.fs.logSaveInfo("未结束运动缓存的对象名： " + this.start_time);
        
        this.zone.run(() => {
          this.pauseCounter = persistSportObj.pausrCounter;
          this.gpsSvc.rawArr = persistSportObj.rawArr;
          this.gpsSvc.baiduArr = persistSportObj.baiduArr;
          this.gpsSvc.currentDist = persistSportObj.currentDist;
          this.gpsSvc.currentDist_show = persistSportObj.currentDist_show;
          this.gpsSvc.currentPace_show = persistSportObj.currentPace_show;
        });

        this.sportIntervalWatcher = setInterval(() => {
          this.runningTime = Math.round(Date.now() / 1000) - this.start_time - this.pauseCounter;
          this.dynamicDuration = moment().startOf('day').seconds(this.runningTime).format('HH:mm:ss');
          console.log(this.dynamicDuration);
          
        }, 1000);

        this.gpsSvc.recoverGps();
        this.persistPer3Second(this.start_time, true);

      }).catch(err => console.error(err));
    }, 1000);
  }

  intervalWatcherPersist = null;
  persistPer3Second(startAt: number, flag: boolean) {
    if (flag) {
      if (this.intervalWatcherPersist !== null) {
        return;
      }
      this.intervalWatcherPersist = setInterval(() => {
        let persistSportObj = {
          pausrCounter : this.pauseCounter,
          rawArr : this.gpsSvc.rawArr,
          baiduArr : this.gpsSvc.baiduArr,
          currentDist : this.gpsSvc.currentDist,
          currentDist_show : this.gpsSvc.currentDist_show,
          currentPace_show : this.gpsSvc.currentPace_show
        }

        this.fs.writeFile(this.fs.appRootPath, startAt + '.obj', JSON.stringify(persistSportObj), {replace: true}).catch(err => console.error(err));
      }, 3000);
    } else {
      clearInterval(this.intervalWatcherPersist);
      this.intervalWatcherPersist = null;
    }
  }

  changeRunningStatus() {
    if (this.runningStatus === this.runningStatusChoose.running_prepare) {
      // 进入运动
      this.runningStatus = this.runningStatusChoose.running_running;

      // 运动时监听实时心率
      this.subscriberRunningHr = this.ble.$_live.subscribe(a => {
        this.zone.run(() => {
          this.o2Value = a[3];
          this.hrValue = a[4];
        });
      });

      // 开启ring sport运动
      if (this.ble.currentDevice) {
        this.ble.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStart);
        setTimeout(() => {
          this.ble.exec(CmdPort.runCtrl+'', CmdCtrl.runOn);
        }, 500);
      }

      // 计时开始：
      this.pauseCounter = 0;
      this.start_time = Math.round(Date.now() / 1000);
      localStorage.setItem('startAt', this.start_time+'');

      this.runningTime = 0; // seconds
      this.gpsSvc.currentPace_show = `00'00"`;
      this.dynamicDuration = moment().startOf('day').seconds(0).format('HH:mm:ss');
      this.sportIntervalWatcher = setInterval(() => {
        this.runningTime = Math.round(Date.now() / 1000) - this.start_time - this.pauseCounter;
        this.dynamicDuration = moment().startOf('day').seconds(this.runningTime).format('HH:mm:ss');
      }, 1000);

      this.gpsSvc.openGps();
      this.persistPer3Second(this.start_time, true);

    } else if (this.runningStatus === this.runningStatusChoose.running_running) {
      this.runningStatus = this.runningStatusChoose.running_prepare;

      // UI 提示
      this.toastSvc.createInfiniteLoader('正在保存数据...');
      this.toastSvc.loader.present();

      // 首先清除运动计时
      clearInterval(this.sportIntervalWatcher);
      this.sportIntervalWatcher = null;

      this.gpsSvc.closeGps();
      if (this.subscriberRunningHr) {
        this.subscriberRunningHr.unsubscribe();
        this.subscriberRunningHr = null;
      }

      localStorage.removeItem('startAt');

      if (this.ble.currentDevice) {
        this.ble.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStop);
        setTimeout(() => {
          this.ble.exec(CmdPort.runCtrl+'', CmdCtrl.runOff);
          setTimeout(() => {
            this.ble.exec(CmdPort.syncData+'', CmdCtrl.sportData);
          }, 500);
        }, 500);
      } else {
        setTimeout(() => {
          this.toastSvc.loader.dismiss();
        }, 2000);
      }

      // deprecated 当前运动距离大于0才记录 
      // new 当前运动时间 > 60秒才产生记录， 不管有没有gps
      if (this.runningTime > 60) {
        let end_time = Math.round(Date.now() / 1000);
        let gps = splitArray(this.gpsSvc.rawArr, null);
        let speed = this.gpsSvc.currentDist / this.runningTime; // m / s
        let avg_pace = speed === 0 ? 0 : 1000 / (60 * speed); //  min/km

        let appRecord: AppRecord = {
          sportType   : this.sportType,
          userId      : this.av.getCurrentUser().id,
          startAt     : this.start_time,
          endAt       : end_time,
          sportRoute  : gps,                                //splitArray(this.gpsSvc.baiduArr, null)
          distance    : this.gpsSvc.currentDist,            // m
          sportTime   : this.runningTime,
          pace        : avg_pace,                           // min
          sync        : SYNC.unsaved                        // 0 || 1
        };
        this.sqliteSvc.createOneAppRecord(appRecord);
      }

      this.persistPer3Second(this.start_time, false);

      setTimeout(() => {
        this.zone.run(() => {
          this.dynamicDuration = null;
          this.start_time = null;
          this.hrValue = null;
          this.pauseCounter = null;
        });
        this.gpsSvc.clearGpsHistory();
      }, 1100);
    }
  }

}

