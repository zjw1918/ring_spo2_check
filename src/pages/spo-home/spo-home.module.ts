import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoHome } from './spo-home';

@NgModule({
  declarations: [
    SpoHome,
  ],
  imports: [
    IonicPageModule.forChild(SpoHome),
  ],
  exports: [
    SpoHome
  ]
})
export class SpoHomeModule {}
