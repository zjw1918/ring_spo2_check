import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

// provider
import { AVService } from '../../providers/av';
import { Utils } from '../../providers/utils';


@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html'
})
export class ForgetPasswordPage {
  user = {
    phoneNumber:null,
    captcha:null,
    password:null
  };
  showCaptchaBtn = true;
  countDown: number;

  constructor(
    private utils: Utils,
    public toastCtrl: ToastController,
    public av: AVService,
    public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello ForgetPasswordPage Page');
  }

  getCaptcha(user) {
    if (this.utils.offline) {
      this.toast('无网络，请检查网络连接', 2000);
      return;
    }

    this.delayCaptcha(10);
    console.log(user);

    // if (this.type === 'create') { // 提供注册的功能
      this.av.isRegister(user.phoneNumber).then(registed => {
        console.log(registed);
        
        if (registed) {
          this.av.requestResetPasswordSms({"mobilePhoneNumber": user.phoneNumber}).subscribe(
            () => console.log('请求-重置密码-验证码已发送'),
            err => {
              console.log(err);
              err = err.json();
              alert(JSON.stringify(err.error));
            }
          );
        } else {
          this.toast('该号码尚未注册', 2000);
        }
      }).catch(err => console.error(err));

    // } else {  // 提供忘记密码重置密码功能
      // this.av.requestResetPasswordSms({"mobilePhoneNumber": user.phoneNumber}).subscribe(
      //   () => console.log('请求-重置密码-验证码已发送'),
      //   err => {
      //     console.log(err);
      //     err = err.json();
      //     alert(JSON.stringify(err.error));
      //   }
      // );
    // }
    
  }


  resetPassword(user) {
    console.log(user);
    if (this.utils.offline) {
      this.toast('无网络，请检查网络连接', 2000);
      return;
    }
    
    this.av.resetPassword(user.captcha, {"password": user.password}).subscribe(
      () => {
        // 改密成功
        this.navCtrl.pop();
      },
      err => {
        console.error(err);
        alert(JSON.stringify(err));
      }
    )
    
  }

  // help func
  delayCaptcha(sec: number) {
    this.countDown = sec;
    this.showCaptchaBtn = false;
    let timer = setInterval(() => {
      this.countDown--;

      if (this.countDown <= 0) {
        clearInterval(timer);
        this.countDown = sec;
        this.showCaptchaBtn = true;
      }
    }, 1000);
  }
  toast(msg: string, sec: number) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: sec,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
