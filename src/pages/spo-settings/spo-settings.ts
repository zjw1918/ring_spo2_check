import { Component, NgZone } from '@angular/core';
import { IonicPage, Events, NavController, NavParams, LoadingController } from 'ionic-angular';

// providers
import { MyConfig } from '../../providers/my-config';
import { BleService } from '../../providers/ble-service';
import { FileService } from '../../providers/file-service';
import { AVService } from '../../providers/av';
import { GpsService } from '../../providers/gps-service';

import { CmdPort, CmdCtrl, EventsMsg } from '../../config/portAndcmd';
import { AppRecord, TABLES, SYNC, SqliteService } from '../../providers/sqlite-service';
import { ToastService, Utils } from "../../providers/utils";

// 3rd libs
import moment from 'moment';
declare var Socket: any;

const Interval = 12;
const DfuType = {
  bootloader: 2,
  application: 4
}

@IonicPage()
@Component({
  selector: 'page-spo-settings',
  templateUrl: 'spo-settings.html',
})
export class SpoSettings {
  currentDevice;
  deviceVersion;
  btnLive = false;
  dfuOver: boolean;
  appDownloadLog: string;

  constructor(
    private utils: Utils,
    private toastSvc: ToastService,
    private gpsSvc: GpsService,
    private sqliteSvc: SqliteService,
    private events: Events,
    private loadingCtrl: LoadingController,
    private av: AVService,
    public fs: FileService,
    public cfg: MyConfig,
    private zone: NgZone,
    private ble: BleService,
    public navParams: NavParams,
    public navCtrl: NavController) {

  }

  ngOnInit() {
    // only for reLog of rawdata
    this.events.subscribe(EventsMsg.log, () => {
      this.logSwitch(false);
      setTimeout(() => {
        this.logSwitch(true);
      }, 5000);
    });
  }

  ionViewDidEnter() {

  }
  

  async checkAppUpdate() {
    this.toastSvc.createLoader('正在检查App更新', 5000);
    this.toastSvc.loader.present();

    try {
      let res = await this.av.queryUpdate('AppUpdate');
      if (!res) {
        this.toastSvc.makeToast('无更新');
        return;
      }
      let remoteVersion = res.get('version');
      if (this.cfg.localAppVersion !== remoteVersion) {
        let url = res.get('file').get('url');
        let name = "ring_spo2.apk";

        let transfer = this.utils.getTransfer();
        transfer.onProgress((progressEvent) => {

          if (progressEvent.lengthComputable) {
            // console.log(Math.ceil(progressEvent.loaded * 100 / progressEvent.total) + '%');
            this.zone.run(() => this.appDownloadLog = Math.ceil(progressEvent.loaded * 100 / progressEvent.total) + '%');
          } else {
            // loadingStatus.increment();
          }
        });
        try {
          let entry = await transfer.download(url, this.fs.appRootPath + name);
          let path = entry.toURL();
          console.log('download complete: ' + path);
          setTimeout(() => {
            cordova.plugins.FileOpener.openFile(path, (data) => {
              console.log('message: ' + data.message);
            }, (error) => {
              console.log('message: ' + error.message);
            });
            this.appDownloadLog = null;
          }, 1000);

        } catch (err) {
          this.appDownloadLog = null;
          console.error(err);
          this.toastSvc.makeToast('网络出了点儿问题');
        }
      } else {
        this.toastSvc.makeToast('已经是最新版');
      }

    } catch (err) {
      console.error(err);
      this.toastSvc.makeToast('网络出了点儿问题');
    }
  }

  logout() {
    // let userId = this.av.getCurrentUser().id;
    this.av.clearLocalUserInfo();
    this.av.clearLocalBoundInfo();

    // 临时方法
    if (this.ble.isScanning) {
      this.ble.isScanning = false;
      this.ble.clearScanner();
    }

    // 不能先clearCurrentDevice，那样的话，连接是断不开的。
    if (this.ble.currentDevice) {
      this.ble.disconnect(this.ble.currentDevice.id).then(() => this.ble.clearCurrentDevice()).catch(err => {
        console.error(err);
      });
    } else {
      this.ble.clearCurrentDevice();
    }

    this.av.logout().then(() => {
      console.log('logout ok');
      this.events.publish(EventsMsg.userEvent, EventsMsg.userEvent_logout);
    }, err => console.error(err));
  }

  switchLive(onOff: string) {
    if (onOff === 'on') {

    } else if (onOff === 'off') {
      this.ble.exec(CmdPort.liveCtrl + '', CmdCtrl.liveStop);
    }
  }

  cmdExec(cmdStr: string, cmdCtrl ? ) {
    this.ble.exec(cmdStr, cmdCtrl);
  }

  disconn() {
    if (this.ble.currentDevice) {
      this.ble.disconnect(this.ble.currentDevice.id).then(() => this.ble.clearCurrentDevice()).catch(err => {
        console.error(err);
      });
    } else {
      this.ble.clearCurrentDevice();
    }
  }

  cutBuffer(arr: ArrayBuffer, offset: number) {
    var obj = [];
    var t = Math.ceil(arr.byteLength / offset);

    for (var i = 0; i < t; i++) {
      obj.push(arr.slice(offset * (i), offset * (i + 1)));
    }
    return obj;
  }

  totalPack = null;
  sentLength;
  payload;
  initPack;
  updateType: string;

  chooseDfuFile() {
    this.fs.chooseFile().then(uri => {
      console.log(uri);
      this.fs.resolveNativePath(uri).then(path => {
        console.log(path);
        let dfuDir = this.fs.fs + '/DFU/';
        this.fs.unZip(path, dfuDir, (progress) => console.log('Unzipping, ' + Math.round((progress.loaded / progress.total) * 100) + '%'))
          .then(result => {
            if (result === 0) {
              console.log('解压成功');
              // this.toast('解压完毕');

              this.fs.readAsText(dfuDir, 'manifest.json').then(res => {
                console.log('read dfu files: ', res);
                if (typeof res === 'string') {
                  let indexObj = JSON.parse(res);
                  let binFile, datFile;

                  if ('application' in indexObj.manifest) {
                    this.updateType = 'application';
                    binFile = indexObj.manifest.application.bin_file;
                    datFile = indexObj.manifest.application.dat_file;
                  } else if ('bootloader' in indexObj.manifest) {
                    this.updateType = 'bootloader';
                    binFile = indexObj.manifest.bootloader.bin_file;
                    datFile = indexObj.manifest.bootloader.dat_file;
                  }


                  // 读bin_file文件，获取：文件总长度，和每20字节分包的结果
                  let a = this.fs.readAsArrayBuffer(dfuDir, binFile).then(buffer => {
                    if (buffer instanceof ArrayBuffer) {
                      console.log(buffer.byteLength);
                      let devidedBuffer = this.cutBuffer(buffer, 20);
                      devidedBuffer = devidedBuffer.map((i) => new Uint8Array(i));
                      return [buffer.byteLength, devidedBuffer];
                    }
                  })
                  // read dat_file, get init header package
                  let b = this.fs.readAsArrayBuffer(dfuDir, datFile).then(buffer => {
                    if (buffer instanceof ArrayBuffer) {
                      let a = new Uint8Array(buffer);
                      return a;
                    }
                  })

                  // 上面两个文件都读完了之后，开始连接蓝牙，发包
                  Promise.all([a, b]).then(res => {
                    console.log(res);

                    this.totalPack = res;
                    this.sentLength = this.totalPack[0][0];
                    this.payload = this.totalPack[0][1];
                    this.initPack = this.totalPack[1];
                  }).catch(err => console.error(err));
                }
              }).catch(err => console.error(err));
            } else if (result === -1) {
              console.log('解压失败');
              this.toastSvc.makeToast('解压失败');
            }
          }).catch(err => console.error(err));
      }).catch(err => console.error(err))
    }).catch(err => console.error(err));
  }

  dfuLog;
  dfuT1;
  dfuT2;
  startDfu() {
    this.dfuOver = false;
    this.ble.isDfuing = true;
    this.dfuT1 = Date.now();
    this.dfuLog = '初始化固件升级中...请耐心等待，若长时间无反应，请退出重试。';
    console.log('dfu 开始');
    if (this.ble.currentDevice['services'] && this.ble.currentDevice.services.length === 3) {
      // 设备目前正处于dfu模式，直接进行dfu
      this.runDfu();
    } else {
      // 设备目前处于普通模式，需要重启，以进入dfu模式
      this.ble.readDfu().then(buffer => {
        console.log('Version: ', new Uint8Array(buffer));
        this.ble.listenDfuOn().subscribe(
          () => {},
          err => console.error(err)
        );

        setTimeout(() => {
          this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[this.updateType]]).buffer).then(() => {
            console.log('第1次写入成功: ', [0x01, DfuType[this.updateType]]);
          }).catch(err => console.error(err));

          setTimeout(() => {
            this.secondConnect();
          }, 2000);
        }, 1000);
      }).catch(err => console.error(err));
    }
  }

  secondConnect() {
    this.ble.scanDfu().then(deviceId => {
      this.ble.connect(deviceId).subscribe(peripheral => {
        // this.ble.changeBleStatus(peripheral);
        console.log('第2次连上OK: ', peripheral);
        setTimeout(() => {
          this.runDfu();
        }, 2000);
      }, err => {
        console.error('Dfu secondConnect: ' + JSON.stringify(err));
        if (!this.dfuOver) {
          setTimeout(() => {
            console.log('dfutag二次连接失败，500ms重连');
            this.secondConnect();
          }, 500);
        }
      });
    }).catch(err => console.error(err));
  }

  // 检测当前是否处于dfu模式完了之后，执行真正的dfu
  runDfu() {
    console.log('DFU process started >>>');

    this.ble.readDfu().then(buffer => {
      console.log('版本：', new Uint8Array(buffer));
      this.ble.listenDfuOn().subscribe(
        this.notiSuc2th.bind(this),
        err => console.error(err)
      );

      setTimeout(() => {
        this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[this.updateType]]).buffer).then(() => {
          console.log('第2次写入成功:', [0x01, DfuType[this.updateType]]);
          let data;
          if (this.updateType.startsWith('a')) {
            data = new Uint32Array([0, 0, this.sentLength]);
          } else if (this.updateType.startsWith('b')) {
            data = new Uint32Array([0, this.sentLength, 0]);
          }
          this.ble.writeDfuPack(data.buffer).then(() => console.log('Sent length pack OK.', data)).catch(err => console.error(err));
        }).catch(err => console.error(err));
      }, 1000);

    }).catch(err => console.error(err));
  }

  async notiSuc2th(buffer) {
    if (buffer) {
      let info = new Uint8Array(buffer);
      console.log('ble 返回的状态：', info);
      switch (info.toString()) {
        case [0x10, 0x01, 0x01].toString():
          console.log('进入第一个noti判断了。');
          await this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x00]).buffer);
          console.log('Writing Initialize DFU Parameters...', [2, 0]);
          await this.ble.writeDfuPack(this.initPack.buffer);
          console.log('Sent header package OK: ', this.initPack);
          await this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x01]).buffer);
          console.log('Sent cmd OK: ', [2, 1]);

          // this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x00]).buffer).then(() => {
          //     console.log('Writing Initialize DFU Parameters...', [2, 0]);
          //     this.ble.writeDfuPack(this.initPack.buffer).then(()=>{
          //         console.log('Sent header package OK: ', this.initPack);
          //         this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x01]).buffer).then(() => {
          //             console.log('Sent cmd OK: ', [2, 1]);
          //         }).catch(err => console.error(err));
          //     }).catch(err => console.error(err));
          // }).catch(err => console.error(err));
          break;
        case [0x10, 0x02, 0x01].toString():
          // 0c 代表一次发12包
          await this.ble.writeDfuCtrl(new Uint8Array([0x08, Interval, 0x00]).buffer);
          console.log('Sent cmd OK: ', [0x08, Interval, 0x00]);
          await this.ble.writeDfuCtrl(new Uint8Array([0x03]).buffer);
          console.log('Sent cmd OK: ', [3]);
          // ble, 终于要给你丫送数据了，接着吧
          this.sendPayload();


          // this.ble.writeDfuCtrl(new Uint8Array([0x08, Interval, 0x00]).buffer).then(() => {
          //     console.log('Sent cmd OK: ', [0x08, Interval, 0x00]);
          //     this.ble.writeDfuCtrl(new Uint8Array([0x03]).buffer).then(()=>{
          //         console.log('Sent cmd OK: ', [3]);

          //         // ble, 终于要给你丫送数据了，接着吧
          //         this.sendPayload();
          //     }).catch(err => console.error(err));
          // }).catch(err => console.error(err));
          break;
        case [0x10, 0x03, 0x01].toString():
          this.payloadCount = 0;
          this.ble.isDfuing = false;
          // this.dfuSeeker = 0;
          await this.ble.writeDfuCtrl(new Uint8Array([0x04]).buffer);
          console.log('Sent cmd OK: ', [4]);

          // this.ble.writeDfuCtrl(new Uint8Array([0x04]).buffer).then(()=>{
          //     console.log('Sent cmd OK: ', [4]);
          // }).catch(err => console.error(err));
          break;
        case [0x10, 0x04, 0x01].toString():
          await this.ble.writeDfuCtrl(new Uint8Array([0x05]).buffer);
          console.log('Sent cmd OK: ', [5]);
          await this.ble.listenDfuOff();
          console.log('关闭notify成功，DFU完成');
          this.dfuT2 = Date.now();
          this.dfuLog += '共用时：' + (this.dfuT2 - this.dfuT1) / 1000 + 's。请返回首页！';
          this.zone.run(() => {
            this.ble.currentDeviceId = null;
            this.ble.currentDevice = null;
            this.currentDevice = null;
            this.dfuOver = true;
          });

          setTimeout(() => {
            this.ble.startScanForManager();
          }, 2000);

          // this.ble.writeDfuCtrl(new Uint8Array([0x05]).buffer).then(()=>{
          //     console.log('Sent cmd OK: ', [5]);
          //     this.ble.listenDfuOff().then(() => console.log('关闭notify成功，DFU完成'))
          //         .catch(err => console.error(err));

          //     this.dfuT2 = Date.now();
          //     this.dfuLog += '共用时：' + (this.dfuT2 - this.dfuT1)/1000 + 's';
          //     this.zone.run(() => {
          //         // this.dfuLog = null;
          //         this.ble.currentDeviceId = null;
          //         this.ble.currentDevice = null;
          //         this.currentDevice =  null;
          //     });
          // }).catch(err => console.error(err));
          break;

        default:
          if (info[0] === 0x11) {
            let alreadyReceiveLength = info[1] | (info[2] << 8) | (info[3] << 16) | (info[4] << 24);
            console.log('ble报告-已收到：', alreadyReceiveLength, '总：' + this.sentLength);
            // setTimeout(() => {
            //     this.sendPayload();
            // }, 0);
          } else {
            console.error('dfu出现未识别命令');
          }
          break;
      }
    }
  }

  // test new pach sendPayload |\\\\\|
  payloadCount = 0;
  async sendPayload() {
    let len = this.payload.length;
    for (let i = 0; i < len; i++) {
      this.zone.run(() => {
        this.dfuLog = Math.ceil(this.payloadCount / this.payload.length * 100) + '%';
      });
      // if (!this.payload[this.payloadCount]) {
      //     // this.payloadCount = 0;
      //     break;
      // }
      await this.ble.writeDfuPackNoResp(this.payload[this.payloadCount++].buffer);
    }
  }

  // dfuSeeker = 0;
  // sendPayload () {
  //     let totalLength = this.payload.length;
  //     this.zone.run(() => {
  //         let currentPercent = Math.ceil(this.dfuSeeker / totalLength * 100) + '%';
  //         this.dfuLog = currentPercent;
  //         console.log(currentPercent);
  //     });
  //     for (let i = 0; i < Interval; i++) {
  //         this.ble.writeDfuPackNoResp(this.payload[this.dfuSeeker++].buffer)
  //             // .then().catch(err => {
  //             //     console.error(err);
  //             //     // this.ble.writeDfuPackNoResp(this.payload[this.dfuSeeker - 1].buffer);
  //             // });
  //     }
  // }

  // sendPayload () {
  //     let count = 0;
  //     let totalLength = this.payload.length;
  //     let callBack = () => {
  //         let tt1 = Date.now();
  //         // console.log(count);
  //         this.zone.run(() => {
  //             let currentPercent = Math.ceil(count / totalLength * 100) + '%';
  //             this.dfuLog = currentPercent;
  //             // console.log(currentPercent);
  //         });
  //         // console.log(Math.ceil(count / totalLength * 100) + '%');
  //         // console.log(this.payload[count]);
  //         if (!this.payload[count]) {
  //             console.log('All sent.');
  //             this.ble.isDfuing = false;
  //             this.dfuOver = true;
  //             return;
  //         }
  //         this.ble.writeDfuPackNoResp(this.payload[count++].buffer).then(() => {
  //             console.log(Date.now() - tt1);
  //             callBack();
  //         }).catch(err => console.error(err));
  //     }
  //     // console.log(this.payload[count]);
  //     this.ble.writeDfuPackNoResp(this.payload[count++].buffer)
  //         .then(() => callBack()).catch(err => console.error(err));
  // }

  dfuOnline() {
    this.ble.isDfuing = true;
    this.ble.checkFwUpdatePromise().then((res) => {
      this.updateType = res[0];
      this.sentLength = res[1];
      this.payload = res[2];
      this.initPack = res[3];
      this.startDfu();
    }).catch(err => console.error(err));
  }

  loadProgress() {
    let percent = 0;
    let loader = this.loadingCtrl.create({
      content: percent + '%',
      duration: null
    });
    loader.present();
    setInterval(() => {
      // this.zone.run(() => percent++)
      loader.setContent(percent++ + '%')
    }, 500)
  }

  // ========================================================================
  // devMode
  devMode = true;
  toogleLogFile = false;
  toogleLogMatlab = false;
  matlabIp = '192.168.3.27';
  matlabPort = '30001';
  btnLog = true;
  ws = null;
  //   canLog = true;
  timer = null;
  logname = null;
  cacheArr = null;
  rawdataFullPath = null;
  log = '';

  // 专门测试写文件 append 问题，已解决
  // writerRunner = null;
  // logSwitch(on: boolean) {
  //     if (on) {
  //         this.btnLog = false;
  //         let filename = 'log_' + moment().format('MM_DD_HH_mm_ss') + '.log';
  //         let writeOption = 0;
  //         this.writerRunner = setInterval(() => {
  //             this.fileSvc.writeFile(this.fileSvc.fs, filename, new Uint8Array([0,1,2,3]).buffer, writeOption++ === 0 ? true : {append: true}).then(() => console.log('write ok.')).catch(err => console.error(err));
  //         }, 1000);

  //     } else {
  //         this.btnLog = true;
  //         if (this.writerRunner !== null) {
  //             clearInterval(this.writerRunner);
  //             this.writerRunner = null;
  //             console.log('write runner stopped ok.');

  //         }
  //     }
  // }

  /**
   * old log ctrl
   */
  logSwitch(on: boolean) {
    console.log(this.toogleLogFile)
    console.log(this.toogleLogMatlab)
    console.log(this.matlabIp)
    console.log(this.matlabPort)

    if (on) {
      // 开启log
      this.ble.isLoging = true;
      this.btnLog = false;
      this.ble.closeGlobalIndiAndNoti();

      setTimeout(() => {
        this.ble.listenOn('indi').subscribe((buffer) => {
          console.log(new Uint8Array(buffer));
        }, err => console.error(err));

        // 重点
        setTimeout(() => {
          let counter = 0;

          if (this.toogleLogFile) {
            // 只有开了filelog， 才能写文件。
            this.logname = 'log_' + moment().format('MM_DD_HH_mm_ss') + '.log';
            console.log('即将开始记录log(rawdata), 文件名：' + this.logname);
            this.rawdataFullPath = 'Android/data/cn.megahealth.ringspo2/files/' + this.logname;
            this.cacheArr = [];
            let writeOption = 0;
            this.timer = setInterval(() => {
              let tmpLength = this.cacheArr.length;
              if (tmpLength > 0) {
                console.log(this.fs.appRootPath, this.logname);

                this.fs.writeFile(this.fs.appRootPath, this.logname, new Uint8Array(this.cacheArr).buffer, writeOption++ === 0 ? true : {
                    append: true
                  })
                  .then(() => {
                    this.cacheArr.splice(0, tmpLength);
                  }).catch(err => {
                    console.error(err)
                  });
              }
            }, 5000);
          }

          if (this.toogleLogMatlab) {
            this.ws = new Socket();
            this.ws.onData = (data) => {};
            this.ws.onError = (errorMessage) => {
              console.log(errorMessage)
            };
            this.ws.onClose = (err) => {
              console.log(err)
            };

            this.ws.open(this.matlabIp, this.matlabPort, () => {
              console.log('socket open ok');

            }, (errorMessage) => {
              console.log('socket open faild');
            });
          }

          let errorCount = 0;
          this.ble.listenOn('noti').subscribe((buffer) => {
            let a = new Uint8Array(buffer);
            if (a[0] === 0xf0) {
              a[0] = 0x5b;
            }
            if (this.toogleLogMatlab && (this.ws !== null)) {
              this.ws.write(a, null, (err) => {
                console.error('Socket write error');
                errorCount++;
              });
            }
            if (this.toogleLogFile && (this.cacheArr !== null)) {
              this.cacheArr = this.cacheArr.concat(Array.from(a));
              // this.fileSvc.writeFile(this.fileSvc.fs, logname, buffer, {append: true}); // 频繁写文件是不合适的
            }

            this.zone.run(() => {
              this.log = 'hr: ' + a[18] + ' spo2: ' + a[19] + ' 收到总包数： ' + (counter++) + ' Socket err 包数：' + errorCount + ' ble发了: ' + ((a[17] << 8) | a[18]);
            })

          }, err => console.error(err))
        }, 500);

        setTimeout(() => {
          this.ble.write(this.cfg.cmd_LogOn.buffer).then(() => {
            //   this.canLog = true;
          }).catch(err => {
            //   this.canLog = true;
          });
        }, 1000);

      }, 2000);



    } else {
      // 关闭log
      this.ble.isLoging = false;
      this.btnLog = true;
      this.ble.write(this.cfg.cmd_LogOff.buffer).then(() => {}).catch(err => console.error(err));

      setTimeout(() => {
        this.ble.openGlobalIndiAndNoti();
      }, 3000);

      setTimeout(() => {
        this.ble.listenOff('indi').then(() => {
          console.log('stop ind ok');
        }).catch(err => {
          console.error(err)
        });
      }, 1000);

      setTimeout(() => {
        this.ble.listenOff('noti').then(() => {
          console.log('stop not ok');
        }).catch(err => {
          console.error(err)
        });

        if (this.ws !== null) {
          this.ws.shutdownWrite();
          this.ws = null;
        }

        if (this.timer !== null) {
          // this.canLog = true;
          clearInterval(this.timer);
          this.timer = null;

          if (this.cacheArr !== null && this.logname !== null) {
            this.fs.writeFile(this.fs.appRootPath, this.logname, new Uint8Array(this.cacheArr).buffer, {
                append: true
              })
              .then(() => {
                // this.cacheArr.splice(0, tmpLength);
                console.log(`${this.logname}-日志已经完成，地址: Android/data/cn.megahealth.ringspo2/files/`);
                this.cacheArr = null;
                this.logname = null;

              }).catch(err => {
                console.error(err)
              });
          }
        }

      }, 1500);
    }
  }

  /**
   * New log ctrl, the ble indi and noti are in ble-service's global callback.
   */
  //   logSwitch(on: boolean) {
  //       console.log(this.toogleLogFile)
  //       console.log(this.toogleLogMatlab)
  //       console.log(this.matlabIp)
  //       console.log(this.matlabPort)

  //       if (on) {
  //         this.btnLog = false;
  //         let counter = 0;

  //         if (this.toogleLogFile) {
  //             // 只有开了filelog， 才能写文件。
  //             this.logname = 'log_' + moment().format('MM_DD_HH_mm_ss') + '.log';
  //             console.log('即将开始记录log(rawdata), 文件名：' + this.logname);
  //             this.rawdataFullPath = 'Android/data/cn.megahealth.ringsport/files/' + this.logname;
  //             this.cacheArr = [];
  //             let writeOption = 0;
  //             this.timer = setInterval(() => {
  //                 let tmpLength = this.cacheArr.length;
  //                 if ( tmpLength > 0) {
  //                     console.log(this.fileSvc.fs, this.logname);

  //                     this.fileSvc.writeFile(this.fileSvc.fs, this.logname, new Uint8Array(this.cacheArr).buffer, writeOption++ === 0 ? true : {append: true})
  //                         .then(() => {
  //                             this.cacheArr.splice(0, tmpLength);
  //                         }).catch(err => {console.error(err)});
  //                 }
  //             }, 5000);
  //         }

  //         if (this.toogleLogMatlab) {
  //             this.ws = new Socket();
  //             this.ws.onData = (data) => {};
  //             this.ws.onError = (errorMessage) => {console.log(errorMessage)};
  //             this.ws.onClose = (err) => {console.log(err)};

  //             this.ws.open(this.matlabIp, this.matlabPort, () => {
  //                 console.log('socket open ok');

  //             }, (errorMessage) => {
  //                 console.log('socket open faild');
  //             });
  //         }

  //         this.events.subscribe(EventsMsg.log, a => {
  //             if (this.toogleLogFile && (this.cacheArr !== null)) {
  //                 this.cacheArr = this.cacheArr.concat(Array.from(a));
  //             }
  //             if (this.toogleLogMatlab && (this.ws !== null)) {
  //                 this.ws.write(a);
  //             }

  //             this.zone.run(() => {
  //                 this.log = 'hr: ' + a[19] + ' 收到总包数： ' + (counter++);
  //             })
  //         });

  //           setTimeout(() => {
  //             // this.ble.write(this.cfg.cmd_LogOn.buffer).then(() => {}).catch(err => {});
  //             this.ble.exec(CmdPort.logSwitch+'', 0);
  //           }, 1000);

  //       } else {
  //           // 关闭log
  //         this.btnLog = true;
  //         // this.ble.write(this.cfg.cmd_LogOff.buffer).then(() => {
  //         // }).catch(err => console.error(err));
  //         this.ble.exec(CmdPort.logSwitch+'', 1);
  //         setTimeout(() => {
  //             this.events.unsubscribe(EventsMsg.log);

  //             if (this.ws !== null) {
  //                 this.ws.shutdownWrite();
  //                 this.ws = null;
  //             }

  //             if (this.timer !== null) {
  //                 // this.canLog = true;
  //                 clearInterval(this.timer);
  //                 this.timer = null;

  //                 if (this.cacheArr !== null && this.logname !== null) {
  //                     this.fileSvc.writeFile(this.fileSvc.fs, this.logname, new Uint8Array(this.cacheArr).buffer, {append: true})
  //                         .then(() => {
  //                             // this.cacheArr.splice(0, tmpLength);
  //                             console.log(`${this.logname}-日志已经完成，地址: Android/data/cn.megahealth.ringsport/files/`);
  //                             this.cacheArr = null;
  //                             this.logname = null;

  //                         }).catch(err => {console.error(err)});
  //                 }
  //             }

  //         }, 1000);
  //       }
  //   }

  //   getHugeData(cmd, sync) {
  //       this.ble.getHugeDataFromBle(cmd, sync);
  //   }

  /**
   * fake bind and unbind
   * bind => exec 0xb0
   * unbind => this
   */
  unBind() {
    let currentUser = this.av.getCurrentUser();
    currentUser.unset('deviceRandom');
    currentUser.save();
  }


  /**
   * loki.js db only test
   */

  //   add() {
  //     let gpsArr = [];
  //     let hrArr = [];
  //     for (let i = 0; i < 300; i++) {
  //         hrArr.push({
  //             hr: Math.round(Math.random() * 40 + 60),
  //             timestamp: (Date.now() / 1000 + i) * 1000
  //         });
  //     }

  //     for (let i = 0; i < 100; i++) {
  //         gpsArr.push({
  //             longitude: 121.9807 + (Math.random() / 100),
  //             latitude: 31.0019 + (Math.random() / 100),
  //             speed: (Math.random() * 100),
  //             timestamp: (Date.now() / 1000 + i) * 1000
  //         })
  //     }
  //     this.db.addDocument({
  //         timestamp   : Date.now(),
  //         dist        : (Math.random() * 4 + 1).toFixed(2),
  //         duration    : `00:25:16`,
  //         avgPace     : `15'27"`,
  //         gps         : gpsArr,
  //         hrArray     : hrArr
  //     });
  //   }
  //   delete() {
  //     // this.db.deleteDocument();
  //   }
  //   saveAll() {
  //     //   this.db.saveAll();
  //   }
  //   importAll() {
  //     //   this.db.importAll();
  //   }

  //   saveTodo() {
  //     this.av.test();
  //   }
  //   getTodo(){
  //     this.av.testFetchObj();
  //   }

  /**
   * sqlite db test
   */
  initSqlite() {
    this.sqliteSvc.initDB();
  }

  add() {
    let gps = [];
    for (let i = 0; i < 100; i++) {
      // gps.push({
      //     longitude: 121.9807 + (Math.random() / 100),
      //     latitude: 31.0019 + (Math.random() / 100),
      //     speed: (Math.random() * 100),
      //     timestamp: (Date.now() / 1000 + i) * 1000
      // })
      gps.push([121.9807 + (Math.random() / 100), 31.0019 + (Math.random() / 100), (Math.random() * 100), (Date.now() / 1000 + i) * 1000]);
    }

    let t1 = Math.round(Date.now() / 1000 - Math.random() * 172800);
    let t2 = t1 + Math.round(Math.random() * 1200 + 600);
    let sport_type = Date.now() % 4;
    if (sport_type === 1) { // 0室外,1室内,2骑行,3游泳， 1的时候无gps
      gps = [];
    }
    let appRecord: AppRecord = {
      sportType: sport_type, // 模拟运动记录类型
      userId: "58870256128fe10068442eb2",
      startAt: t1, // 大概半小时前 s
      endAt: t2, // 现在 s
      sportRoute: [gps],
      distance: Math.round((Math.random() * 4) * 1000), // m
      sportTime: t2 - t1,
      pace: Math.round(Math.random() * 1200 + 600), // s
      sync: SYNC.unsaved,
    };
    this.sqliteSvc.createOneAppRecord(appRecord);
  }

  importAll() {
    this.sqliteSvc.findAllRecords(TABLES.app_records);
  }
  getTotalCount() {
    this.sqliteSvc.count(TABLES.app_records).then(length => {
      console.log(length);
    }).catch(err => console.error(err));
  }
  get10Items() {
    this.sqliteSvc.find_10_app_records();
  }
  dropTableApp() {
    this.sqliteSvc.dropTable(TABLES.app_records);
  }

  // ble 本地记录
  importAllBle() {
    this.sqliteSvc.findAllRecords(TABLES.ble_records);
  }
  getTotalCountBle() {
    this.sqliteSvc.count(TABLES.ble_records).then(length => {
      console.log(length);
    }).catch(err => console.error(err));
  }
  dropTableBle() {
    this.sqliteSvc.dropTable(TABLES.ble_records);
  }

  /**
   * 获取远程app记录
   */
  getRemoteAppRecords_10() {
    let t1 = Date.now() / 1000;
    this.av.getRemoteAppRecordsWithoutRoute_10(this.sqliteSvc.currentSeaker).then(res => {
      console.log('请求时间： ' + (Date.now() / 1000 - t1) + 's');
      console.log(res);
      if (res instanceof Array && res.length > 0) {
        let objArray: Array < AppRecord > = [];
        res.map((i) => {
          let item: AppRecord = {
            sportType: i.get('sportType') === undefined ? 0 : i.get('sportType'),
            userId: i.get('userId'),
            startAt: i.get('startAt'),
            endAt: i.get('endAt'),
            sportRoute: i.get('sportRoute'),
            distance: i.get('distance'), // m
            sportTime: i.get('sportTime'),
            pace: i.get('pace'), // s
            sync: SYNC.saved // 0 || 1
          }
          console.log(item);
          objArray.push(item);
        });
        this.sqliteSvc.recoverAppRecordsBatchWithoutRoute(objArray)
      }
    }).catch(err => console.error(err));
  }
}