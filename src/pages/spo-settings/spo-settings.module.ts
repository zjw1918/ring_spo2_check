import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoSettings } from './spo-settings';

@NgModule({
  declarations: [
    SpoSettings,
  ],
  imports: [
    IonicPageModule.forChild(SpoSettings),
  ],
  exports: [
    SpoSettings
  ]
})
export class SpoSettingsModule {}
