import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// providers
// import { gpsToBaiduFinalPointsArr } from '../../providers/gps-service';
// import { MapService } from '../../providers/map-service';
// import { drawTwoEcharts } from '../../providers/utils';

// 3rd js libs
import echarts from 'echarts';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-spo-detail',
  templateUrl: 'spo-detail.html',
})
export class SpoDetail {
  data = {};
  title;

  record;
  // totalDist;      // 运动总距离    ok
  // totalDuration;  // 运动总时长    ok
  // totalPace;      // 运动平均配速   ok
  // totalCal;       // 运动总卡路里
  // chartKey = [];
  // chartPace = [];
  // chartHr = [];
  // flat_routine = [];
  // gps: Array<any>;

  constructor(
    // private mapSvc: MapService,
    private navParams: NavParams,
    public navCtrl: NavController)
  { }

  ngOnInit() {

  }

  spo2_chart_data_catagory(data:any){
      let result_x = [];
      let result_y = [];
      let o2Arr = data.o2Arr;
      for(let i = 0,j = o2Arr.length;i<j;i++){
          let t = new Date(data.time[0]*1000 + i*1000);
          result_x.push(this.format(t));
          result_y.push(o2Arr[i]);
      }
      return {
          result_x : result_x,
          result_y : result_y
      }
  }
  spo2_chart_catagory(data:any,id:string){
      let chart_div = document.getElementById(id);
      chart_div.style.height=chart_div.offsetWidth*0.5+'px';
      // 基于准备好的dom，初始化echarts实例
      let myChart = echarts.init(chart_div);
      // 指定图表的配置项和数据
      let option = {
          grid:{
              x:30,
              y:25,
              x2:25,
              y2:20,
              borderColor:'rgba(0,0,0,0)'
          },
          calculable : true,
          xAxis : [
              {
                  type : 'category',
                  boundaryGap : false,
                  axisTick:{
                      show:true,
                      inside:true,
                      length:6,
                      lineStyle:{
                          color: '#999',
                          width: 2
                      }
                  },
                  splitLine:{
                      show:false
                  },
                  data : data.result_x
              }
          ],
          yAxis : [
              {
                  type : 'value',
                  min:40,
                  max:100,
                  splitNumber:6,
                  axisTick:{
                      show:true,
                      inside:true,
                      length:6,
                      lineStyle:{
                          color: '#999',
                          width: 2
                      }
                  },
                  splitLine:{
                      show:false
                  }
              }
          ],
          series : [
              {
                  name:'联盟广告',
                  type:'line',
                  symbol:'none',
                  stack: '总量',
                  smooth:true,
                  data:data.result_y
              }
          ]
      };
      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
  }
  format(date:Date){
      let h:any = date.getHours();
      h = h < 10 ? ('0'+h ): h;
      let m:any = date.getMinutes();
      m = m < 10 ? ('0'+m ): m;
      return h + ':' + m ;
  }

  ionViewDidLoad() {
    this.record = this.navParams.get('data');
    this.title = moment(this.record.startAt * 1000).format('YYYY-MM-DD HH:mm');
    // this.totalPace = this.record.pace ? moment().startOf('day').seconds(this.record.pace * 60).format(`mm'ss"`) : null;
    // this.totalDuration = moment().startOf('day').seconds(this.record.sportTime).format('HH:mm:ss');
    // this.totalDist = (this.record.distance / 1000).toFixed(2);
    setTimeout(() => {
    //   // gps
    //   if (typeof(this.record.sportRoute) === typeof("hello")) {
    //     this.gps = JSON.parse(this.record.sportRoute);
    //   } else {
    //     this.gps = this.record.sportRoute;
    //   }
    //   if (this.gps.length > 0) {
    //     this.gps.map(routine => {
    //       this.flat_routine = this.flat_routine.concat(routine);
    //     });
    //   }
    //   // console.log(this.flat_routine);
    //   let paceMap = {};
    //   this.flat_routine.map(i => {
    //     let timestampKey = Math.round(i[3]);
    //     paceMap[timestampKey] = (1 / (i[2] / 1000)) / 60; // min/km
    //   });

      // 计算全程平均配速
      // let t1 = this.record.startAt;
      // let t2 = this.record.endAt;
      // // 横坐标分析：心率、配速横坐标其实是同一个。
      // let latest = null;

      let hrObj = this.navParams.get('ble');
      console.log(hrObj);
      this.data = hrObj;
      let result = this.spo2_chart_data_catagory(this.data);
      this.spo2_chart_catagory(result,'spo2-chart');
      
      // for (let i = t1; i <= t2; i++) {
      //   latest = paceMap[i] ? paceMap[i] : '';
      //   this.chartPace.push(latest);
      //   // this.chartHr.push(null);
      //   hrObj && this.chartHr.push(hrObj[i]);
      //   this.chartKey.push(moment(i * 1000).format('HH:mm'));
      // }
      // // this.keyHrArr = [];
      // // this.hrArray = [];

      // if (this.gps.length > 0) {
      //   let map = this.mapSvc.init('map-detail');
      //   let flatBaiduRoutine = [];
      //   this.gps.map((routine, i) => {
      //     let baiduArr = gpsToBaiduFinalPointsArr(routine);
      //     flatBaiduRoutine = flatBaiduRoutine.concat(baiduArr);
      //     if (i === 0) {
      //       this.mapSvc.drawMarker(map, baiduArr[0]);
      //     }
      //     this.mapSvc.drawPolyline(map, baiduArr);
      //   });
      //   this.mapSvc.setZoom(map, flatBaiduRoutine);
      // }

      // let chartsPace = echarts.init(document.getElementById('chart_pace'));
      // drawTwoEcharts( chartsPace, this.chartKey,
      //   this.chartHr, 30, 240, 42, '#feb913', null, // hr
      //   this.chartPace, 0, 30, 6, '#3364f3', null   // pace
      // );
    }, 350);
  }
}
