import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpoDetail } from './spo-detail';

@NgModule({
  declarations: [
    SpoDetail,
  ],
  imports: [
    IonicPageModule.forChild(SpoDetail),
  ],
  exports: [
    SpoDetail
  ]
})
export class SpoDetailModule {}
