export const GLOBAL_STATUS = {
  _isMegaEmc : false,

  bufSpo: [],
  bufHr: [],

  defaultSpo : [95, 96, 97],
  defaultHr : [75, 76],
};
