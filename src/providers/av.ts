import { Injectable, NgZone } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Platform } from 'ionic-angular';

// third part lib
import AV from 'leancloud-storage';
import { FileService } from './file-service';
import { AppRecord, BleRecord } from '../providers/sqlite-service';
import { Utils } from '../providers/utils';
import { UserInfo } from '../models/UserInfo';
import { BoundInfo } from '../models/BoundInfo';

@Injectable()
export class AVService {
  userId: string;
  remoteUserInfo: AV.Object;
  remoteBoundDevice: AV.Object;

  userInfo: UserInfo = new UserInfo();
  boundInfo: BoundInfo = new BoundInfo();

  private requestSmsCodeUrl = 'https://api.leancloud.cn/1.1/requestSmsCode';
  private usersByMobilePhoneUrl = 'https://api.leancloud.cn/1.1/usersByMobilePhone';
  private requestResetPasswordUrl = "https://api.leancloud.cn/1.1/requestPasswordResetBySmsCode";
  private resetPasswordUrl = "https://api.leancloud.cn/1.1/resetPasswordBySmsCode";

  // ringSpo2_check
  private appId = 'MSKbpTDKeLXOMTfNfkYdDGQv-gzGzoHsz';
  private appKey = 'cCB9bXlTTcPqwSsD10xh5S5J';

  constructor(
    private utils: Utils,
    private platform: Platform,
    public zone: NgZone,
    private fs: FileService,
    public http: Http) 
  {
    console.log('Hello Auth Provider');

    platform.resume.subscribe(() => {
      console.log('resume');
      // this.uploadLocalLog('App entered background');
      this.utils.offline = this.utils.network.type === 'none' ? true : false;
      this.fs.logSaveInfo('Current network is offline? ' + this.utils.offline);
    }, err => console.error(err));

    platform.pause.subscribe(() => {
      console.log('pause');
    }, err => console.error(err));
  }

  init() {
    AV.init({ appId: this.appId, appKey:this.appKey });
  }

  initUserPath() {
    this.fs.fs = this.fs.appRootPath + this.getCurrentUser().id + '/';
    this.fs.createDir(this.fs.appRootPath, this.getCurrentUser().id, false).then(() => console.log('当前用户文件夹创建成功。')).catch(err => console.log('当前用户文件夹已存在。'));
    console.log(this.fs.fs);
  }

  initUserInfo() {
    this.userId = this.getCurrentUser().id;
    this.userInfo = new UserInfo();
    this.boundInfo = new BoundInfo();
    this.userInfo.userId = this.userId;
    this.boundInfo.userId = this.userId;

    // 1. load local cache
    this.userInfo.loadLocalUserInfo(this.userId);
    this.boundInfo.loadLocalBoundInfo(this.userId);

    // 2. load local boundInfo
    return this.getMultiRemoteSingletonObj();
  }

  // loadLocalUserInfo() {
  //   let user = this.getCurrentUser();
  //   if (user) {
  //     let userInfoStr: string = localStorage.getItem(user.id + '.info');
  //     if (userInfoStr) {
  //       let obj = JSON.parse(userInfoStr);
  //       this.UserInfo.avatarPath = obj.avatarPath;
  //       this.UserInfo.name = obj.name;
  //       this.UserInfo.gender = obj.gender;
  //       this.UserInfo.age = obj.age;
  //       this.UserInfo.height = obj.height;
  //       this.UserInfo.weight = obj.weight;
  //     }
  //   }
  // }

  // loadLocalBoundDevice() {
  //   let user = this.getCurrentUser();
  //   if (user) {
  //     let deviceStr: string = localStorage.getItem(user.id + '.dvc');
  //     if (deviceStr) {
  //       let obj = JSON.parse(deviceStr);
  //       this.BoundInfo.mac = obj.mac;
  //       this.BoundInfo.random = obj.random;
  //     }
  //   }
  // }

  // persistUserInfo() {
  //   localStorage.setItem(this.getCurrentUser().id + '.info', JSON.stringify(this.UserInfo));
  // }
  
  // persistBoundInfo() {
  //   localStorage.setItem(this.getCurrentUser().id + '.dvc', JSON.stringify(this.BoundInfo));
  // }

  clearLocalUserInfo() {
    this.userInfo.trancateUserInfo();
    this.zone.run(() => this.userInfo = new UserInfo());
    // this.userInfo = null;
    // let userId = this.getCurrentUser().id;
    // this.zone.run(() => {
    //   this.UserInfo = {
    //     avatarPath  : null,
    //     name        : null,
    //     gender      : null,
    //     age         : null,
    //     height      : null,
    //     weight      : null,
    //   }
    //   this.remoteUserInfo = null;
    // });
    // localStorage.removeItem(userId + '.info');
  }

  clearLocalBoundInfo() {
    this.boundInfo.trancateBoundInfo();
    this.zone.run(() => this.boundInfo = new BoundInfo());
    // this.zone.run(() => {
    //   this.BoundInfo = {
    //     mac : null,
    //     random : null,
    //   }
    //   this.remoteBoundDevice = null;
    // });
    // localStorage.removeItem(this.getCurrentUser().id + '.dvc');
  }

  getCurrentUser() {
    return AV.User.current();
  }

  // 重要，临时注释掉。
  // sessionTokenLogin() {
  //   let sessionToken = AV.User.current().getSessionToken();
  //   console.log(sessionToken);
  //   AV.User.become(sessionToken).then(userObj => {
  //     console.log('session token login ok: ', userObj);
  //   }).catch(err => {
  //     console.error('session token login failed: ', err);
  //   })
  // }

  // createRemoteUserInfo() {
  //   this.remoteUserInfo = new AV.Object('UserInfo');
  //   // 新建一个 ACL 实例
  //   var acl = new AV.ACL();
  //   acl.setPublicReadAccess(true);
  //   acl.setWriteAccess(AV.User.current(), true);
  //   this.remoteUserInfo.setACL(acl);
  //   this.remoteUserInfo.set('userId', this.getCurrentUser().id);
  //   this.remoteUserInfo.save();
  // }

  createMultiRemoteSingletonClass() {
    let acl = new AV.ACL();
    acl.setPublicReadAccess(true);
    acl.setWriteAccess(AV.User.current(), true);

    let classNameArr = ['UserInfo', 'BoundDevice'];
    let objArr = classNameArr.map(className => {
      let obj = new AV.Object(className);
      obj.setACL(acl);
      obj.set('userId', this.getCurrentUser().id);
      return obj;
    });

    return AV.Object.saveAll(objArr);
  }

  // 此处是单例模式
  getRemoteUserInfo(): Promise<AV.Object> {
    return new Promise((resolve, reject) => {
      if (!this.remoteUserInfo) {
        let query = new AV.Query('UserInfo');
        query.equalTo('userId', this.getCurrentUser().id);
        query.find().then(res => {
          if (res['length'] === 0) {
            // 服务器上无该用户的UserInfo信息
            this.remoteUserInfo = new AV.Object('UserInfo');
            var acl = new AV.ACL();  // 新建一个 ACL 权限实例
            acl.setPublicReadAccess(true);
            acl.setWriteAccess(AV.User.current(), true);
            this.remoteUserInfo.setACL(acl);
            this.remoteUserInfo.set('userId', this.getCurrentUser().id);
            resolve(this.remoteUserInfo);
          } else {
            // 服务器上已有该用户的UserInfo信息
            this.remoteUserInfo = res[0];
            resolve(this.remoteUserInfo);
          }
        }).catch(err => {
          reject(err);
        });
      } else {
        resolve(this.remoteUserInfo);
      }
    });
  }

  getRemoteBoundInfo(): Promise<AV.Object> {
    return new Promise((resolve, reject) => {
      if (!this.remoteBoundDevice) {
        let query = new AV.Query('BoundDevice');
        query.equalTo('userId', this.getCurrentUser().id);
        query.find().then(res => {
          if (res['length'] === 0) {
            // 服务器上无该用户的device信息
            this.remoteBoundDevice = new AV.Object('BoundDevice');
            var acl = new AV.ACL();  // 新建一个 ACL 权限实例
            acl.setPublicReadAccess(true);
            acl.setWriteAccess(AV.User.current(), true);
            this.remoteBoundDevice.setACL(acl);
            this.remoteBoundDevice.set('userId', this.getCurrentUser().id);
            resolve(this.remoteBoundDevice);
          } else {
            // 服务器上已有该用户的device信息
            this.remoteBoundDevice = res[0];
            resolve(this.remoteBoundDevice);
          }
        }).catch(err => {
          reject(err);
        });
      } else {
        resolve(this.remoteBoundDevice);
      }
    });
  }

  getMultiRemoteSingletonObj() {
    let classArr = ['UserInfo', 'BoundDevice'];
    let userId = this.getCurrentUser().id;
    let queryArr = classArr.map(className => {
        let query = new AV.Query(className);
        query.equalTo('userId', userId);
        return query;
    });
    let promiseArr = queryArr.map(query => {
        return query.find().then(res => {
            return res[0];
        }).catch(err => console.error(err));
    })
    
    return Promise.all(promiseArr).then(res => {
      this.remoteUserInfo = res[0];
      this.remoteBoundDevice = res[1];
      
      this.recoverRemoteUserInfo(this.remoteUserInfo);
      this.recoverRemoteBoundDevice(this.remoteBoundDevice);
    }).catch(err => {
      console.error(err);
      this.fs.logSaveErr(JSON.stringify(err));
    });
  }

  // recoverRemoteUserInfo(obj) {
  //   if (!obj) {
  //     return
  //   }
  //   let avatarObj = obj.get('avatar');
  //   if (avatarObj) {
  //     let avatarUrl = avatarObj.get('url');
  //     this.downloadAvatar(avatarUrl).then((path) => {
  //       console.log('download avatar ok: ' + path);
  //       this.zone.run(() => {
  //         this.UserInfo.avatarPath = path;
  //         this.persistUserInfo();
  //       })
  //     }).catch(err => {
  //       console.error('download avatar error: ', err);
  //     });
  //   }
  //   this.zone.run(() => {
  //     this.UserInfo.name = obj.get('name');
  //     this.UserInfo.gender = obj.get('gender');
  //     this.UserInfo.age = obj.get('age');
  //     this.UserInfo.height = obj.get('height');
  //     this.UserInfo.weight = obj.get('weight');
  //     this.persistUserInfo();
  //   });
  // }

  recoverRemoteUserInfo(obj) {
    if (!obj) {
      return
    }
    let avatarObj = obj.get('avatar');
    if (avatarObj) {
      let avatarUrl = avatarObj.get('url');
      this.downloadAvatar(avatarUrl).then((path) => {
        this.fs.logSaveInfo('Download avatar ok: ' + path);
        this.zone.run(() => {
          this.userInfo.avatarPath = path;
          this.userInfo.persistUserInfo();
        })
      }).catch(err => {
        console.error('Download avatar error: ', err);
        this.fs.logSaveErr('Download avatar error: ' + JSON.stringify(err));
      });
    }

    this.zone.run(() => {
      this.userInfo.userId  = obj.get('userId');
      this.userInfo.name    = obj.get('name');
      this.userInfo.gender  = obj.get('gender');
      this.userInfo.age     = obj.get('age');
      this.userInfo.height  = obj.get('height');
      this.userInfo.weight  = obj.get('weight');
      this.userInfo.persistUserInfo();
    });
  }

  // recoverRemoteBoundDevice(obj) {
  //   if (!obj) {
  //     return;
  //   }
  //   this.BoundInfo.mac = obj.get('mac');
  //   this.BoundInfo.random = obj.get('random');
  //   this.persistBoundInfo();
  // }

  recoverRemoteBoundDevice(obj) {
    if (!obj) {
      return;
    }
    this.boundInfo.mac = obj.get('mac');
    this.boundInfo.random = obj.get('random');
    this.boundInfo.persistBoundInfo();
  }

  /**
   * test
   */
  // test() {
  //   return new Promise((resolve, reject) => {
  //     var Todo = AV.Object.extend('Todo');
  //     var todo = new Todo();
  //     // let arr = {
  //     //   name: "aaa",
  //     //   age: 19,
  //     //   location: [[121, 30, Date.now(), 13], [121, 30, Date.now(), 13]]
  //     // };

  //     let arr = [[-121.874683277, 31.843749374, 123123, 19.0101], [121.874683277, 31.843749374, 123, 19.98]]
  //     todo.set('jsArr2D', arr);
  //     // let str = base64(arr);

  //     todo.save().then((todo) => {
  //       console.log('New object created with objectId: ' + todo.id);
  //       resolve(todo.id);
  //     }, function (error) {
  //       console.error('Failed to create new object, with error message: ' + error.message);
  //       reject(error);
  //     });
  //   });
  // }

  // testFetchObj() {
  //   var query = new AV.Query('Todo');
  //   query.get('58a6de3961ff4b0062af2f3d').then((todo) => {
  //     // 成功获得实例
  //     // data 就是 id 为 57328ca079bc44005c2472d0 的 Todo 对象实例
  //     console.log(todo);
      
  //   }, (error) => {
  //     console.error(error);
  //   });
  // }

  isRegister(phone: any) {
    return new Promise((resolve, reject) => {
        AV.Cloud.run('isRegister', {'phoneNumber': phone})
          .then(res => resolve(res), err => reject(err));
    })
  }

  /**
   * 注册时请求验证码
   */
  requestSmsCode(jsonObj) {
    let myHeaders = new Headers();
    myHeaders.append('X-LC-Id', this.appId);
    myHeaders.append('X-LC-Key', this.appKey);
    myHeaders.append('Content-Type', 'application/json');

    return this.http.post(this.requestSmsCodeUrl, JSON.stringify(jsonObj), {headers: myHeaders});
  }

  /**
   * 执行注册
   */
  createUser(userObj) {
    let myHeaders = new Headers();
    myHeaders.append('X-LC-Id', this.appId);
    myHeaders.append('X-LC-Key', this.appKey);
    myHeaders.append('Content-Type', 'application/json');

    return this.http.post(this.usersByMobilePhoneUrl, JSON.stringify(userObj), {headers: myHeaders});
  }

  /**
   * 执行登录
   */
  login(ph, pw) {
    return AV.User.logInWithMobilePhone(ph, pw);
  }

  /**
   * 执行登出
   */
  logout() {
    return AV.User.logOut();
  }
  
  /**
   * 请求重置密码的验证码
   */
  requestResetPasswordSms(jsonObj) {
    let myHeaders = new Headers();
    myHeaders.append('X-LC-Id', this.appId);
    myHeaders.append('X-LC-Key', this.appKey);
    myHeaders.append('Content-Type', 'application/json');
    return this.http.post(this.requestResetPasswordUrl, JSON.stringify(jsonObj), {headers: myHeaders});
  }
  /**
   * 重置密码
   */
  resetPassword(smsCode, jsonObj) {
    let myHeaders = new Headers();
    myHeaders.append('X-LC-Id', this.appId);
    myHeaders.append('X-LC-Key', this.appKey);
    myHeaders.append('Content-Type', 'application/json');
    return this.http.put(this.resetPasswordUrl + '/' + smsCode, JSON.stringify(jsonObj), {headers: myHeaders});
  }

  saveFeedback(content: string) {
    return new Promise((resolve, reject) => {
      let Feedback = AV.Object.extend('Feedback');
      var feed = new Feedback();
      feed.set('content', content);
      feed.save().then(feed => {
        // console.log('New object created with objectId: ' + feed.id);
        resolve(feed);
      }, err => {
        // console.error('AV Failed to create new object, with error message: ' + err.message);
        reject(err);
      });
    })

  }
  

  /**
   * general method
   */

  /**
   * 文件 api 封装
   */

  // saveAll(arr: Array<any>, options:any) {
  //   for (let i = 0, length = arr.length; i < length; i++) {
  //     var file = new AV.File(arr[i].name, arr[i]);
  //   }
  //   return AV.Object.saveAll(arr, options);
  // }

  saveAll(files: Array<any>) {
    // 下面两种从input文件对象File构造AV.File对象的方法，在浏览器ok，在android上不行。
    // 暂时不用，而用第三种base64
    // map 方法
    // arr = arr.map(file => {
    //   return new AV.File(file.name, file);
    // })

    // 普通for
    // let AVObj = [];
    // for (let i = 0, length = arr.length; i < length; i++) {
    //   AVObj.push(new AV.File(arr[i].name, arr[i]));
    // }
    // console.log(AVObj);

    // 到目前为止，实测，用base64方法在android上能行。
    // var data = { base64: '6K+077yM5L2g5Li65LuA5LmI6KaB56C06Kej5oiR77yf' };
    // var file1 = new AV.File('resume.txt', data);
    // var file2 = new AV.File('resume.txt', data);
    // var file3 = new AV.File('resume.txt', data);
    // let AVObj:any = [file1, file2, file3];

      // arr = arr.map((base64Str) => {
      //   var data = { base64: base64Str };
      //   return new AV.File('haha.bin', data);
      // });
      // return AV.Object.saveAll(arr);

    return this.fs.fileToBase64All(files).then((arr) => {
      arr = arr.map((base64Str, i) => {
        var data = { base64: base64Str };
        return new AV.File(files[i].name, data);
      });
      return AV.Object.saveAll(arr);
    })

    // return this.fs.fileToBase64All(files).then((res) => {
    //   let base64StrList = res[0];
    //   let nameList = res[1];

    //   let AVFileList = [];
    //   for(let i = 0, length = base64StrList.length; i < length; i++) {
    //     AVFileList.push(new AV.File(nameList[i], { base64: base64StrList[i] }))
    //   }
    //   return AV.Object.saveAll(AVFileList);
    // })
    // return AV.Object.saveAll(arr, options);
  }

  /**
   * query update info
   */
  queryUpdate(arg: string): Promise<AV.Object> {
    var query = new AV.Query(arg);
    query.descending('createdAt'); // 获取最新的一条，降序排列；
    return query.first();
  }

  /**
   * cordova Transfer upload
   */
  uploadAvatar(filepath: string) {
    return new Promise((resolve, reject) => {
      let trim = filepath.indexOf('?');
      if (trim !== -1) {
        filepath = filepath.slice(0, trim);
      }

      let avatarEndpoint = "https://api.leancloud.cn/1.1/files/avatar"

      let fileTransfer = this.utils.getTransfer();
      let options = {
        headers: {
          'X-LC-Id': this.appId,
          'X-LC-Key': this.appKey,
          'X-LC-Session': AV.User.current().getSessionToken(),
          'Content-Type': 'application/octet-stream'
        }
      }

      fileTransfer.upload(filepath, avatarEndpoint, options).then(obj => {
        let fileObj = JSON.parse(obj['response']);
        this.remoteUserInfo.set('avatar', AV.Object.createWithoutData('_File', fileObj.objectId));
        this.remoteUserInfo.save().then((res) => {
          // this.persistUserInfo();
          this.userInfo.persistUserInfo();
          resolve(res);
        }, (err) => {
          reject(err);
        });
      }, (err) => {
        reject(err)
      });
    });
  }

  downloadAvatar(url: string) {
    return new Promise((resolve, reject) => {
      let fileTransfer = this.utils.getTransfer();
      fileTransfer.download(url, this.fs.fs + 'avatar.jpg').then((entry) => {
        resolve(entry.toURL());
      }, err => {
        reject(err);
      });
    });
  }

  updateRemoteUserInfo(field: string, data) {
    return new Promise((resolve, reject) => {
      this.remoteUserInfo.set(field, data);
      this.remoteUserInfo.save().then(res => {
        // this.persistUserInfo();
        this.userInfo.persistUserInfo();
        resolve(res);
      }, err => {
        reject(err);
      });
    })
  }

  /************************************************************************** *
  *上传一条app运动记录
  ************************************************************************** */
  uploadOneAppRecord (obj: AppRecord) {
    return new Promise((resolve, reject) => {
      var AppSport = AV.Object.extend('AppSport');
      var todo = new AppSport();
      todo.set('sportType', obj.sportType);
      todo.set('userId', obj.userId);
      todo.set('startAt', obj.startAt);
      todo.set('endAt', obj.endAt);
      todo.set('sportRoute', obj.sportRoute);
      todo.set('distance', obj.distance);
      todo.set('sportTime', obj.sportTime);
      todo.set('pace', obj.pace);

      todo.save().then((todo) => {
        console.log('New AppSport created with objectId: ' + todo.id);
        resolve(todo.id);
      },  (error) => {
        console.error('Failed to create new AppSport, with error message: ' + error.message);
        reject(error);
      });
    });
  }

  /**
   * 批量上传app运动记录
   */
  uploadBatchAppRecords(arr: Array<AppRecord>) {
    let todos = [];
    arr.map((obj: AppRecord) => {
      let AppSport = AV.Object.extend('AppSport');
      let todo = new AppSport();
      todo.set('sportType', obj.sportType);
      todo.set('userId', obj.userId);
      todo.set('startAt', obj.startAt);
      todo.set('endAt', obj.endAt);
      todo.set('sportRoute', obj.sportRoute);
      todo.set('distance', obj.distance);
      todo.set('sportTime', obj.sportTime);
      todo.set('pace', obj.pace);

      todos.push(todo);
    });

    return AV.Object.saveAll(todos);
  }

  uploadOneBleRecord (obj: BleRecord) {
    return new Promise((resolve, reject) => {
      var RingSport = AV.Object.extend('RingSport');
      var todo = new RingSport();
      todo.set('type', obj.sportType);
      todo.set('userId', obj.userId);
      todo.set('startAt', obj.startAt);
      todo.set('stopType', obj.stopType);
      todo.set('data', {"__type": "Bytes", "base64": obj.data });

      todo.save().then((todo) => {
        console.log('New RingSport created with objectId: ' + todo.id);
        resolve(todo.id);
      },  (error) => {
        console.error('Failed to create new RingSport, with error message: ' + error.message);
        reject(error);
      });
    });
  }

  uploadBatchBleRecords(arr: Array<BleRecord>) {
    let todos = [];
    arr.map((obj: BleRecord) => {
      let RingSport = AV.Object.extend('RingSport');
      let todo = new RingSport();
      todo.set('type', obj.sportType);
      todo.set('userId', obj.userId);
      todo.set('startAt', obj.startAt);
      todo.set('stopType', obj.stopType);
      todo.set('data', {
        "__type": "Bytes",
        "base64": obj.data
      });

      todos.push(todo);
    });

    return AV.Object.saveAll(todos);
  }

  /**
   * 下载最新10条记录
   */
  getRemoteAppRecordsWithoutRoute_10(startAt?: number) {
    var query = new AV.Query('AppSport');
    query.select('sportType', 'userId', 'startAt', 'endAt', 'distance', 'sportTime', 'pace'); // , 'sportRoute'
    query.equalTo('userId', this.getCurrentUser().id);
    if (startAt) {
      query.lessThan('startAt', startAt);
    }
    query.descending('startAt');
    query.limit(10);// 最多返回 10 条结果
    return query.find();
  }
  
  getRemoteAppRecordsWithoutRoute_10_upToLatest(startAt: number) {
    var query = new AV.Query('AppSport');
    query.select('sportType', 'userId', 'startAt', 'endAt', 'distance', 'sportTime', 'pace'); // , 'sportRoute'
    query.equalTo('userId', this.getCurrentUser().id);
    query.greaterThan('startAt', startAt);
    query.descending('startAt');
    query.limit(10);// 最多返回 10 条结果
    return query.find();
  }

  getOneRemoteAppRecordRoute(startAt: number) {
    var query = new AV.Query('AppSport');
    query.select('sportRoute'); // , 'sportRoute'
    query.equalTo('userId', this.getCurrentUser().id);
    query.equalTo('startAt', startAt);
    return query.find();
  }

  getOneRemoteBleRecordFull(startAt: number, endAt: number) {
    var query = new AV.Query('RingSport');
    query.equalTo('userId', this.getCurrentUser().id);
    query.greaterThanOrEqualTo('startAt', startAt);
    query.lessThan('startAt', endAt);
    query.descending('startAt');
    return query.find();
  }

  // bind mac to userId in server
  saveBoundInServer(mac: string, random: string) {
    return new Promise((resolve, reject) => {
      this.getRemoteBoundInfo().then(info => {
        info.set('mac', mac);
        info.set('random', random);
        info.save().then(res => resolve(res)).catch(err => reject(err));
      }).catch(err => reject(err));
    });
  }

  // query mac. before new bind step
  // 记录长度为空，代表没查到该mac有绑定，则允许向mac发起蓝牙连接
  queryMac(mac: string): Promise<any> {
    var query = new AV.Query('BoundDevice');
    query.equalTo('mac', mac);
    return query.find();
  }
  
  /**
   * 上传log到服务器
   */
  uploadLocalLog(arg: string) {
    // 先清理 残余log
    this.fs.logSaveRest(arg).then(() => {
      if (this.utils.network.type !== 'wifi') {
        console.log('不在wifi下，本地log未上传。');
        return;
      }

      this.fs.readAsArrayBuffer(this.fs.fs, this.fs.localLogName).then(buffer => {
        let byteArrayFile = new AV.File(this.fs.localLogName, Array.from(new Uint8Array(buffer)));

        var Todo = AV.Object.extend('LogAndroid');
        var todo = new Todo();
        todo.set('userId', this.getCurrentUser().id);
        todo.set('content', byteArrayFile);

        todo.save().then((todo) => {
          console.log('New object created with objectId: ' + todo.id);
          this.fs.deleteFile(this.fs.fs, this.fs.localLogName).then(() => {
            // 本地log文件上传成功
            this.fs.initLocalLog();
          }).catch(err => console.error(err));

        }, error => {
          console.error('Failed to create new object, with error message: ' + error.message);
        });
      })
    });

  }

  /**
   * error
   */
  AVError = {
      200: "没有提供用户名，或者用户名为空",                    // Username is missing or empty
      201: "没有提供密码，或者密码为空",                       // Password is missing or empty
      202: "用户名已经被占用" ,                              // 信息 - Username has already been taken.
      203: "电子邮箱地址已经被占用",                          // 信息 - Email has already been taken.
      204: "没有提供电子邮箱地址",                            // 信息 - The email is missing, and must be specified.
      205: "找不到电子邮箱地址对应的用户",                     // 信息 - A user with the specified email was not found.
      207: "只能通过注册创建用户，不允许第三方登录",             // 信息 - Users can only be created through sign up.
      208: "第三方帐号已经绑定到一个用户，不可绑定到其他用户",     // 信息 - An existing account already linked to another user.
      210: "用户名和密码不匹配",                             // 信息 - The username and password mismatch.
      211: "找不到用户",                                   // 信息 - Could not find user.
      212: "请提供手机号码",                                // 信息 - The mobile phone number is missing, and must be specified.
      213: "手机号码对应的用户不存在",                        // 信息 - A user with the specified mobile phone number was not found.
      214: "手机号码已经被注册",                            // 信息 - Mobile phone number has already been taken.
      215: "未验证的手机号码",                              // 信息 - Mobile phone number isn't verified.
      216: "未验证的邮箱地址",                              // 信息 - Email address isn't verified.
      217: "无效的用户名，不允许空白用户名",                   // 信息 - Invalid username, it must be a non-blank string.
      218: "无效的密码，不允许空白密码",                      // 信息 - Invalid password, it must be a non-blank string.
      250: "连接的第三方账户没有返回用户唯一标示 id",           // 信息 - Linked id missing from request
      251: "无效的账户连接，一般是因为 access token 非法引起的",// 信息 - Invalid linked session或者Invalid Weibo session
      252: "无效的微信授权信息",                             // 信息 - Invalid Weixin session

      601: "发送短信过于频繁",
      602: "发送短信或者语音验证失败，运营商返回错误",
      603: "无效的短信验证码",
  };

}
