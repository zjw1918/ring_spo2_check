import { Injectable, NgZone } from '@angular/core';
import { Events, Platform } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';

import { AVService } from './av';
import { FileService } from "../providers/file-service";

@Injectable()
export class MyConfig {
  localAppVersion; // 本地app版本
  ifAppHasNew = false; // 远程最新app版本
  remoteAppVersion: string; // eg: 0.0.1024
  remoteFwVersion: string; // eg: 4026

  cmd_LogOn         = new Uint8Array([0xf0, 0, 0, 0]);
  cmd_LogOff        = new Uint8Array([0xf0, 0, 0, 1]);

  constructor(
    private zone: NgZone,
    private fs: FileService,
    private appVersionCtrl: AppVersion,
    private device: Device,
    private platform: Platform,
    private events: Events,
    private av: AVService) 
  {
    this.platform.ready().then(async () => {
      this.localAppVersion = await this.getAppVersion(); // 这一步是不会有错的
      this.fs.logSaveInfo('Local App ver: ' + this.localAppVersion);
      this.checkAppUpdate();
      this.getBleRemote();
    });
  }

  // ==========================================================
  // version.split('.').pop(); // eg: 1.0.3265 => 3265
  getAppVersion() {
    return this.appVersionCtrl.getVersionNumber();
  }

  async checkAppUpdate() {
    try {
      let res = await this.av.queryUpdate('AppUpdate');
      this.remoteAppVersion = res.get('version');
      this.fs.logSaveInfo("Remote latest App ver: " + this.remoteAppVersion );
      if (this.localAppVersion !== this.remoteAppVersion)
        this.zone.run(() => this.ifAppHasNew = true);
    } catch (err) {
      this.fs.logSaveErr('checkAppUpdate ' + JSON.stringify(err));
    }
  }

  async getBleRemote() {
    try {
      let res = await this.av.queryUpdate('FwUpdate');
      this.remoteFwVersion = res.get('version');
      this.fs.logSaveInfo("Remote Fw ver: " + this.remoteFwVersion);
    } catch (err) {
      this.fs.logSaveErr('getBleRemote ' + JSON.stringify(err));
    }
  }

  //============================= user profile config
  /**
    计算卡路里的方案：
    http://keisan.casio.com/exec/system/1350959101
    基礎代謝率:bmr
    公式：
    BMR(男)=(13.7×體重(公斤))+(5.0×身高(公分))-(6.8×年齡)+66
    BMR(女)=(9.6×體重(公斤))+(1.8×身高(公分))-(4.7×年齡)+655
    BMR = 6
    (6.0 jog/walk combination (jogging component of less than 10 minutes) (Taylor Code 180))
    kcal =  BMR X Mets/24 X hour
 */
  // 获得当前用户基础代谢率
  getBMR (): number {
    return 1479; // 默认是我的基础代谢率
  }
  
  /**
   *  mock data from LightBlue
   * */
  lb_service = '180d';
  lb_not = '2a37';

}
