import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import loki from 'lokijs';
import localforage from 'localforage';

// import LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter'; // 暂时不用adapter
// ionic2 暂时不能用，兼容性问题：cordova.js:426 Wrong type for parameter "uri" of resolveLocalFileSystemURI: Expected String, but got Null.
// import LokiCordovaFSAdapter from "loki-cordova-fs-adapter"; 


// provider
import { AVService } from '../providers/av';

@Injectable()
export class DbService {
  private db: any;
  private records: any;

  dbName = null;
  collectionName = 'records';

  private _recordsSource = new BehaviorSubject<Array<any>>([]);
  _records$ = this._recordsSource.asObservable();
  changeRecordsStatus(record) {
    this._recordsSource.next(record);
  }

  constructor(
    private av: AVService,
    public http: Http) {
    console.log('Hello DbService Provider');
    // this.init();
  }


//   暂时不用indexdb的 adapter，采用localforage
//   init_indexDb() {
//     console.log('db-service initing...');
//     let user = this.av.getCurrentUser();
//     this.dbName = user.id + '.db';
//     // let adapter = new LokiCordovaFSAdapter({"prefix": "loki_" + this.dbName + '_'});
//     let adapter = new LokiIndexedAdapter("loki_" + this.dbName + '_');
//     this.db = new loki(this.dbName, {
//         autoload: true,
//         autoloadCallback : () => {
//             // if database did not exist it will be empty so I will intitialize here
//             this.records = this.db.getCollection(this.collectionName);
//             if (this.records === null) {
//                 console.log("initializing new database");
//                 this.records = this.db.addCollection(this.collectionName);
//             } else {
//                 console.log("found existing database");
//             }
//             console.log(this.records);
            
//         },
//         autosave: true, 
//         autosaveInterval: 5000, // 10 seconds
//         adapter: adapter
//       });
//     console.log(this.records);
//   }

  init() {
    console.log('db-service initing...');
    let user = this.av.getCurrentUser();
    this.dbName = user.id + '.db';
    this.db = new loki(this.dbName);
    
    // 取出数据库
    this.importAll_forage();
  }

  addDocument(obj) {
      this.records.insert(obj);

      // LokiJS is one's-based, so the final element is at <length>, not <length - 1>
      // console.log("inserted document: " + this.records.get(length));
      console.log("records.data.length: " + this.records.data.length);
      console.log(this.records);

      // 存疑：是不是要每次add记录时都要持久化，按理来说是的。且add的操作并不频繁。
      this.saveAll_forage();
      this.changeRecordsStatus(this.records.data); // .map(record => record.month = new Date(record).getMonth() + 1)
  }

  deleteDocument(record) {
      console.log("record to delete: ", record);

      // $loki is the document's index in the collection
      console.log("targeting document at collection index: ", record.$loki);
      this.records.remove(record.$loki);
  }

  saveAll_forage() {
      localforage.setItem(this.dbName, JSON.stringify(this.db)).then(() => {
          // 返回一个文本内容：value；暂时就不打印出来了。
          console.log('database successfully saved');
      }).catch(err => {
          console.error('error while saving: ', err);
      });
  }

  importAll_forage() {
    localforage.getItem(this.dbName).then((value) => {
        console.log('the full database has been retrieved');
        this.db.loadJSON(value);
        this.records = this.db.getCollection(this.collectionName);
        console.log(this.records);
        
        if (this.records === null) {
            console.log("initializing new database");
            this.records = this.db.addCollection(this.collectionName);
        } else {
            console.log("found existing database");
        }
        this.changeRecordsStatus(this.records.data);
        
    }).catch(err => {
        console.log('Error importing database: ', err);
        this.records = this.db.addCollection(this.collectionName);
        console.log(this.records);
        this.changeRecordsStatus(this.records.data);
    });

  }


}
