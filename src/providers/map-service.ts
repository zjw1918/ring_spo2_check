import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// providers
import { GpsService } from '../providers/gps-service';

@Injectable()
export class MapService {

  constructor(
    public gpsSvc: GpsService,
    public http: Http) {
    console.log('Hello MapService Provider');
  }

  init(container: string) {
    let map = new BMap.Map(container);          // 创建地图实例
    let point = new BMap.Point(116.404, 39.915);  // 创建点坐标
    if (this.gpsSvc.baiduArr !== null && this.gpsSvc.baiduArr.length >= 2) {
      point = this.gpsSvc.baiduArr[this.gpsSvc.baiduArr.length - 1];
    }
    console.log(point);
    map.centerAndZoom(point, 15);             // 初始化地图，设置中心点坐标和地图级别
    console.log('初始化地图完成');
    return map;
  }

  setZoom(map: any, bPoints){
    var view = map.getViewport(eval(bPoints));
    if(map.oldView != JSON.stringify(view)){
      var mapZoom = view.zoom;
      var centerPoint = view.center;
      map.centerAndZoom(centerPoint,mapZoom);
      map.oldView = JSON.stringify(view);
    }
  }

  addLine(map, bPoints: Array<any>){
    if(bPoints.length === 0) {
      return;
    }
    // 创建标注对象并添加到地图
    // for(i = 0;i <pointsLen;i++) {
    // 	linePoints.push(new BMap.Point(points[i].lng,points[i].lat));
    // }
    let polyline = new BMap.Polyline(bPoints, {strokeColor:"blue", strokeWeight:4, strokeOpacity:0.5});   //创建折线
    map.addOverlay(polyline);   //增加折线
  }

  drawMarker(map, bPoint) {
    var myIcon = new BMap.Icon("http://webapi.amap.com/theme/v1.3/markers/n/start.png",
       new BMap.Size(19, 31), { imageOffset: new BMap.Size(0, 0) });
    // var myIcon = new BMap.Icon("http://webapi.amap.com/theme/v1.3/markers/n/start.png");
    // {icon:myIcon}
    // var marker = new BMap.Marker(new BMap.Point(116.404, 39.915));
    var marker = new BMap.Marker(bPoint, {icon:myIcon});
    map.addOverlay(marker); //增加点
  }

  drawPolyline(map, arr) {
    // this.map.centerAndZoom(new BMap.Point(116.404, 39.915), 15);
    // this.map.enableScrollWheelZoom();
    let polyline = new BMap.Polyline(arr, {strokeColor:"blue", strokeWeight:4, strokeOpacity:0.5});   //创建折线
    map.addOverlay(polyline);   //增加折线
  }
}
