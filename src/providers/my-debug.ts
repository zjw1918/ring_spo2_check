import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// third lib
import moment from 'moment';

/*
  Generated class for the MyDebug provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MyDebug {
  private ifDebug;

  constructor(public http: Http) {
    console.log('Hello MyDebug Provider');
    this.ifDebug = true;
  }

  log(total, arg) {
    if (this.ifDebug){
      let now = moment().format('HH:mm:ss.SSS');
      total += `
        [${now}]-${arg}<br>
      `;
      return total;
    }
  }

}
