import { Injectable, NgZone } from '@angular/core';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

// import { Geolocation } from 'ionic-native';
import { Geolocation } from '@ionic-native/geolocation';

// 3rd lib
import coordtransform from 'coordtransform';
import distance from 'gps-distance';
// import moment from 'moment';

@Injectable()
export class GpsService {
  isPausing: boolean;

  rawArr: Array<any> = null;
  baiduArr: Array<any> = null;
  currentDist: number = null;
  currentDist_show: string = null;
  currentPace_show: string = null;

  gps_$: Subject<any> = new Subject();

  constructor(
    private geolocation: Geolocation,
    private zone: NgZone) {
    console.log('Hello GpsService Provider');
  }

  getCurrentPosition() {
    // return this.geolocation.getCurrentPosition();
  }

  watchPosition(option?:any) {
    return this.geolocation.watchPosition(option);
  }

  private watcher: Subscription = null;
  openGps() {
    if (this.watcher !== null) {
      console.error('重复初始化Geolocation error');
      return;
    }
    this.rawArr = [];
    this.baiduArr = [];
    this.currentDist = 0;

    // let option = { maximumAge: 1000, timeout: 5000, enableHighAccuracy: true };
    // this.watcher = this.geolocation.watchPosition(option).subscribe(position => {
    //   if (this.isPausing) {
    //     console.log('You are pausing');
    //     return;
    //   }
    //   console.log(position);
    //   if (!('coords' in position)) {
    //     console.error('收到了position但是没有coords, ', position);
    //     return;
    //   }
    //   if (position.coords.speed <= 0) {
    //     console.error('收到了position,有coords,但speed为0，忽略', position);
    //     this.zone.run(() => {
    //       this.currentPace_show = `00'00"`;
    //     });
    //     return;
    //   }

    //   if (position.coords.accuracy > 80) {
    //     console.error('收到了position,有coords,但accuracy太大，忽略', position);
    //     return;
    //   }

    //   this.rawArr.push([
    //     position.coords.latitude,
    //     position.coords.longitude,
    //     position.coords.speed,
    //     Math.round(position.timestamp / 1000)
    //   ]);
    //   console.log('当前gps长度：' + this.rawArr.length);

    //   this.zone.run(() => {
    //     this.currentDist += this.calculate2PointDist(this.rawArr.slice(-2));
    //     this.currentDist_show = (this.currentDist / 1000).toFixed(2);
    //     let speed = position.coords.speed;
    //     if (speed !== 0 && ((1 / (speed / 1000)) / 60) < 30) { // 1 / (speed / 1000) : s/km
    //       this.currentPace_show = moment().startOf('day').seconds(1 / (speed / 1000)).format(`mm'ss"`);
    //     } else {
    //       this.currentPace_show = `00'00"`;
    //     }
    //   });

    //   let baiduRawP = gpsToBaiduRawPoint([position.coords.latitude, position.coords.longitude]);
    //   let baiduP = new BMap.Point(baiduRawP[0], baiduRawP[1]);
    //   this.gps_$.next(baiduP);
    //   this.baiduArr.push(baiduP);
    // } , err => console.error('watchPosition Error： ', err));
  }

  recoverGps() {
    if (this.watcher !== null) {
      console.error('重复初始化Geolocation error');
      return;
    }

    // let option = { maximumAge: 1000, timeout: 5000, enableHighAccuracy: true };
    // this.watcher = this.geolocation.watchPosition(option).subscribe(position => {
    //   if (this.isPausing) {
    //     console.log('You are pausing');
    //     return;
    //   }
    //   console.log(position);
    //   if (!('coords' in position)) {
    //     console.error('收到了position但是没有coords, ', position);
    //     return;
    //   }
    //   if (position.coords.speed <= 0) {
    //     console.error('收到了position,有coords,但speed为0，忽略', position);
    //     this.zone.run(() => {
    //       this.currentPace_show = `00'00"`;
    //     });
    //     return;
    //   }

    //   if (position.coords.accuracy > 80) {
    //     console.error('收到了position,有coords,但accuracy太大，忽略', position);
    //     return;
    //   }

    //   this.rawArr.push([
    //     position.coords.latitude,
    //     position.coords.longitude,
    //     position.coords.speed,
    //     Math.round(position.timestamp / 1000)
    //   ]);
    //   console.log('当前gps长度：' + this.rawArr.length);

    //   this.zone.run(() => {
    //     this.currentDist += this.calculate2PointDist(this.rawArr.slice(-2));
    //     this.currentDist_show = (this.currentDist / 1000).toFixed(2);
    //     let speed = position.coords.speed;
    //     if (speed !== 0 && ((1 / (speed / 1000)) / 60) < 30) { // 1 / (speed / 1000) : s/km
    //       this.currentPace_show = moment().startOf('day').seconds(1 / (speed / 1000)).format(`mm'ss"`);
    //     } else {
    //       this.currentPace_show = `00'00"`;
    //     }
    //   });

    //   let baiduRawP = gpsToBaiduRawPoint([position.coords.latitude, position.coords.longitude]);
    //   let baiduP = new BMap.Point(baiduRawP[0], baiduRawP[1]);
    //   this.gps_$.next(baiduP);
    //   this.baiduArr.push(baiduP);
    // } , err => console.error('watchPosition Error： ', err));
  }

  closeGps() {
    if (this.watcher !== null) {
      this.watcher.unsubscribe();
      this.watcher = null;
    }
  }

  clearGpsHistory() {
    this.rawArr = null;
    this.baiduArr = null;

    this.currentDist = null;
    this.currentDist_show = null;
    this.currentPace_show = null;
    this.isPausing = false;
  }

  // =================================================================fake
  // fake gps stream
  // getGpsStreamFake() {
  //   let basePoint = {x:116.405, y:39.920};

  //   return new Observable(observer => {
  //     let timer = setInterval(() => {
  //       basePoint.x += 0.00008;
  //       basePoint.y += 0.00008;
  //       this.changeGpsStatus(basePoint);
  //       observer.next(basePoint);
  //     }, 800);

  //     return () => {
  //         console.log('clear ok');
  //         clearInterval(timer);
  //     }
  //   })
  // }

  /**
   * gps => baidu
   * raw gps => raw baidu
   * 参数正确顺序： 经度、纬度
   */
  // gpsToBaiduRawPoint(rawP: Array<any>) {
  //   let gcjP =  coordtransform.wgs84togcj02(rawP[1], rawP[0]); // 1, lon; 0, lat
  //   let baiduP = coordtransform.gcj02tobd09(gcjP[0], gcjP[1]);
  //   return baiduP;
  // }

  // gpsToBaiduFinalPointsArr(rawPointsArr: Array<any>) {
  //   let baiduFinalPointsArr = [];
  //   if (rawPointsArr.length > 0) {
  //     baiduFinalPointsArr = rawPointsArr.map(p => {
  //       let baiduP = this.gpsToBaiduRawPoint(p);
  //       return new BMap.Point(baiduP[0], baiduP[1]);
  //     })
  //   }
  //   return baiduFinalPointsArr;
  // }

  /**
   * gaode => baidu
   * @param rawGpsPointArr
   */
  // gaodeToBaiduRawPoint(rawP: Array<any>) {
  //   let baiduP = coordtransform.gcj02tobd09(rawP[1], rawP[0]);
  //   return baiduP;
  // }

  // gaodeToBaiduFinalPointsArr(rawPointsArr: Array<any>) {
  //   let baiduFinalPointsArr = [];
  //   if (rawPointsArr.length > 0) {
  //     baiduFinalPointsArr = rawPointsArr.map(p => {
  //       let baiduP = this.gaodeToBaiduRawPoint(p);
  //       return new BMap.Point(baiduP[0], baiduP[1]);
  //     })
  //   }
  //   return baiduFinalPointsArr;
  // }

  // 获取两点间距离方法: 默认是km， 转换成m, 暂时保留小数
  calculate2PointDist(rawGpsPointArr: Array<any>): number  {
    let sum: number = 0;
    console.log(rawGpsPointArr);
    if (rawGpsPointArr.indexOf(null) !== -1) {
      return 0;
    }
    if (rawGpsPointArr.length >= 2) {
      let tmpArr = rawGpsPointArr.map(p => [ p[0], p[1] ]); // order: latitude, longitude
      sum = distance(tmpArr) * 1000; // m
    }
    return sum;
  }
}

/*************************************************************************************************************************
 * independed function
 *************************************************************************************************************************/
export const gpsToBaiduRawPoint = (rawP: Array<any>) => {
    let gcjP =  coordtransform.wgs84togcj02(rawP[1], rawP[0]); // 1, lon; 0, lat
    let baiduP = coordtransform.gcj02tobd09(gcjP[0], gcjP[1]);
    return baiduP;
}
export const gpsToBaiduFinalPointsArr = (rawPointsArr: Array<any>) => {
    let baiduFinalPointsArr = [];
    if (rawPointsArr.length > 0) {
      baiduFinalPointsArr = rawPointsArr.map(p => {
        let baiduP = gpsToBaiduRawPoint(p);
        return new BMap.Point(baiduP[0], baiduP[1]);
      })
    }
    return baiduFinalPointsArr;
}
