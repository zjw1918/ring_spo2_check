import { Injectable, NgZone } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AVService } from '../providers/av';
import { Utils } from '../providers/utils';
import moment from 'moment';

import { FileService } from "./file-service";

export const TABLES = {
  app_records: "app_records",
  ble_records: "ble_records",
}

export enum SYNC {
  unsaved = 0,
  saved
}

export interface AppBriefRecord {
  id?           : number
  sportType     : number
  startAt       : number
  endAt         : number
  distance      : string
  pace          : string
  sportTime     : string
  month         : string
  showTitleTime : string
}

export interface AppRecord {
  sportType : number,
  userId    : string,
  startAt   : number,
  endAt     : number,
  sportRoute: Array<any>,
  distance  : number,     // m
  sportTime : number,
  pace      : number,     // s
  sync      : number      // 0 || 1
}

export interface BleRecord {
  sportType  : number,
  userId     : string,
  startAt    : number,
  stopType   : number,
  data       : string,
  sync       : number
}

const CMD_DB = {
  creare_app_records: `CREATE TABLE IF NOT EXISTS app_records (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sportType integer,
    userId text,
    startAt integer,
    endAt integer,
    sportRoute text,
    distance integer,
    sportTime integer,
    pace integer,
    sync integer
  );`,

  create_ble_records: `CREATE TABLE IF NOT EXISTS ble_records (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sportType integer,
    userId text,
    startAt integer,
    stopType integer,
    data text,
    sync integer
  );`,
}

@Injectable()
export class SqliteService {
  currentSeaker;
  private db: SQLiteObject;
  appRecords: AppBriefRecord[] = [];

  constructor(
    private fs: FileService,
    private utils: Utils,
    private sqlite: SQLite,
    private zone: NgZone,
    private av: AVService) {
    console.log('Hello SqliteService Provider');
  }

  initDB() {
    this.appRecords = [];
    this.currentSeaker = null;
    let userId = this.av.getCurrentUser().id;
    // this.db = new SQLite();
    this.sqlite.create({
      name    : userId + '_data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      this.db = db;
      this.db.sqlBatch([CMD_DB.creare_app_records, CMD_DB.create_ble_records]).then(() => {
        console.log('同时创建app,ble表 ok');
        // 先加载10条
        this.find_10_app_records().then(localHasData => {
          if (!localHasData) {
            // 此时本地数据库没有更多数据了，请判断是否要联网获取更多数据；
            if (this.utils.offline) {
              console.log('没网，无法联网加载记录');
              return;
            }

            this.loadRemoteAppRecordsNoRoute_10().then((remoteHasData) => {
              if (remoteHasData) {
                // infiniteScroll.complete();
              } else {
                // infiniteScroll.enable(false);
                console.log('数据加载完毕');
              }
            }).catch(err => {
              console.error(err);
            })
          } else {
            // 自动上传本地未同步过的记录
            setTimeout(() => {
              this.doRefreshBackground();           
            }, 1000);
          }
        }).catch(err => console.error(err));
      }).catch(err => console.error('同时创建app,ble表失败', JSON.stringify(err)));
    }).catch(err => console.error(err));
  }

  createOneAppRecord(obj: AppRecord) {
    let sportRoute = JSON.stringify(obj.sportRoute);
    let sql = `INSERT INTO ${TABLES.app_records} VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
    let params = [
      obj.sportType, 
      obj.userId, 
      obj.startAt, 
      obj.endAt, 
      sportRoute,
      obj.distance, 
      obj.sportTime,
      obj.pace,   // 默认是分钟min
      obj.sync
    ];

    this.db.executeSql(sql, params).then(() => {
      console.log('插入一条app记录 ok');
      if (!this.currentSeaker) {
        this.currentSeaker = obj.startAt;
      }

      // 记录插入成功后，需要更新记录列表，插入头部
      let newRecord: AppBriefRecord = {
        startAt: obj.startAt,
        endAt: obj.endAt,
        sportType: obj.sportType,
        distance : (obj.distance / 1000).toFixed(2),
        sportTime :  moment().startOf('day').seconds(obj.sportTime).format('HH:mm:ss'),
        pace :  moment().startOf('day').seconds(obj.pace * 60).format(`mm'ss"`),
        month : new Date(obj.startAt * 1000).getFullYear() + '年' + (new Date(obj.startAt * 1000).getMonth() + 1) + '月',
        showTitleTime : moment(obj.startAt * 1000).format('DD日 HH:mm'),
      };
      this.zone.run(() => this.appRecords.unshift(newRecord));

      // 检查有没有网
      if (!this.utils.offline) {
        this.av.uploadOneAppRecord(obj).then(() => {
          this.updateSyncStatus(TABLES.app_records, obj.startAt, SYNC.saved);
        }).catch(err => console.error(err));
      }

    }, (err) => {
      console.error('插入一条app记录 err, ' + JSON.stringify(err));
    });
  }

  // 批量恢复app运动记录 no route（不带gps）
  recoverAppRecordsBatchWithoutRoute(objs: Array<AppRecord>) {
    let bachSqlArr = [];
    objs.map(record => {
      let param = [
        record.sportType, 
        record.userId, 
        record.startAt, 
        record.endAt, 
        null,
        record.distance, 
        record.sportTime,
        record.pace,
        record.sync
      ];
      bachSqlArr.push([`INSERT INTO ${TABLES.app_records} VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?);`, param]);
    });

    this.db.sqlBatch(bachSqlArr).then(() => {
      console.log('批量恢复app运动记录 ok');
      this.find_10_app_records();
    }).catch(err => console.error('批量恢复app运动记录 failed' + JSON.stringify(err)));
  }

  recoverAppRecordsBatchWithoutRoute_upToLatest(objs: Array<AppRecord>, startAt: number) {
    let bachSqlArr = [];
    objs.map(record => {
      let param = [
        record.sportType, 
        record.userId, 
        record.startAt, 
        record.endAt, 
        null,
        record.distance, 
        record.sportTime,
        record.pace,
        record.sync
      ];
      bachSqlArr.push([`INSERT INTO ${TABLES.app_records} VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?);`, param]);
    });

    this.db.sqlBatch(bachSqlArr).then(() => {
      console.log('批量恢复app运动记录 ok (upToLatest)');
      this.find_10_app_records_upToLatest(startAt);
    }).catch(err => console.error('批量恢复app运动记录 failed' + JSON.stringify(err)));
  }

  // ble record是一条完整的记录
  recoverBleRecord(obj: BleRecord) {
    let sql = `INSERT INTO ${TABLES.ble_records} VALUES (null, ?, ?, ?, ?, ?, ?);`;
    let params = [
      obj.sportType,
      obj.userId,
      obj.startAt,
      obj.stopType,
      obj.data,
      obj.sync
    ];
    this.db.executeSql(sql, params).then(() => {
      console.log('恢复一条ble记录 ok');
    }, (err) => {
      console.error('恢复一条ble记录 err, ' + JSON.stringify(err));
    });
  }

  createOneBleRecord(obj: BleRecord) {
    let sql = `INSERT INTO ${TABLES.ble_records} VALUES (null, ?, ?, ?, ?, ?, ?);`;
    let params = [
      obj.sportType,
      obj.userId,
      obj.startAt,
      obj.stopType,
      obj.data,
      obj.sync
    ];
    this.db.executeSql(sql, params).then(() => {
      console.log('插入一条ble记录 ok');

      // 检查有没有网
      if (!this.utils.offline) {
        this.av.uploadOneBleRecord(obj).then(() => {
          this.updateSyncStatus(TABLES.ble_records, obj.startAt, SYNC.saved);
        }).catch(err => console.error(err));
      }
    }, (err) => {
      console.error('插入一条ble记录 err, ' + JSON.stringify(err));
    });
  }

  // deleteOneRecord() {

  // }

  // updateOneRecord() {

  // }

  // findOneRecord(table: string, startTime: number) {
    
  // }

  // findTenRecords() {
  // }

  updateRoute(table: string, startAt: number, sportRoute: Array<any>) {
    let str = JSON.stringify(sportRoute);
    let sql = `UPDATE ${table} SET sportRoute=? WHERE startAt=?;`;
    this.db.executeSql(sql, [str, startAt]).then(rs => {
      console.log(startAt + '记录-route更新 ok');
    }, (err) => {
      console.error(startAt + '记录-route更新 error', err);
    });
  }

  updateSyncStatus(table: string, startAt: number, flag: SYNC) {
    let sql = `UPDATE ${table} SET sync=${flag} WHERE startAt=${startAt};`;
    this.db.executeSql(sql, []).then(rs => {
      console.log(startAt + '记录更新 ok');
    }, (err) => {
      console.error(startAt + '记录更新 error', err);
    });
  }

  updateSyncStatusBach(table: string, objArr: Array<any>, flag: SYNC) {
    let startAtArray: Array<number> = objArr.map(obj => obj.startAt);
    let sql = `UPDATE ${table} SET sync=${flag} WHERE startAt IN (${startAtArray.join()});`;
    this.db.executeSql(sql, []).then(rs => {
      console.log(table + ':' + startAtArray.join() + ' sync记录更新 ok');
    }, (err) => {
      console.error(table + ':' + startAtArray.join() + ' sync记录更新 error', err);
    });
  }

  getAppRecord(startAt: number) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT * FROM ${TABLES.app_records} WHERE startAt=${startAt};`;
      this.db.executeSql(sql, []).then(rs => {
        resolve(rs.rows.item(0));
      }, (err) => {
        reject(err)
      });
    });
  }

  getBleRecord(startAt: number, endAt: number) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT * FROM ${TABLES.ble_records} WHERE startAt<? AND startAt>=? ORDER BY startAt DESC;`;
      this.db.executeSql(sql, [endAt, startAt]).then(rs => {
        // let len = rs.rows.length;
        // for (let i = 0; i < len; i++) {
        //   console.log(rs.rows.item(i));          
        // }
        resolve(rs.rows.item(0));
      }, (err) => {
        reject(err);
      });
    });
  }

  find_10_app_records () {
    return new Promise((resolve, reject) => {
      console.log('分页seeker: '+ this.currentSeaker);
      let sql = `SELECT id, sportType, startAt, endAt, distance, sportTime, pace FROM ${TABLES.app_records} ${this.currentSeaker ? 'WHERE startAt<' + this.currentSeaker : '' } ORDER BY startAt DESC LIMIT 0, 10;`;
      this.db.executeSql(sql, []).then(resultSet => {
        console.log('find_10_records ok:');
        console.log(resultSet);
        let length = resultSet.rows.length;
        console.log(length);
        let tmpArr = [];
        if (length > 0) {
          for (let i = 0; i < length; i++) {
            console.log(resultSet.rows.item(i));
            tmpArr.push(resultSet.rows.item(i));
          }
          tmpArr.map(record => {
            record.distance = (record.distance / 1000).toFixed(2);
            record.pace =  moment().startOf('day').seconds(record.pace * 60).format(`mm'ss"`);
            record.sportTime =  moment().startOf('day').seconds(record.sportTime).format('HH:mm:ss');
            record.month = new Date(record.startAt * 1000).getFullYear() + '年' + (new Date(record.startAt * 1000).getMonth() + 1) + '月';
            record.showTitleTime = moment(record.startAt * 1000).format('DD日 HH:mm');
            return record;
          });
          
          this.zone.run(() => {
            this.appRecords = this.appRecords.concat(tmpArr);
          });
          this.currentSeaker = resultSet.rows.item(length - 1).startAt;
          console.log('分页-current seaker: ' + this.currentSeaker);
          console.log(this.appRecords);
          // 本地有记录
          resolve(true);
        } else {
          console.log('Blank table, no data.');
          // 本地记录空了。这时候请注意，是否要联网获取更多记录。
          resolve(false);
          // this.currentSeaker = 0;
        }
      }, err => {
        console.error('find 10 local Records err, ' + err);
        reject(err);
      });
    });
  }

  find_10_app_records_upToLatest (startAt: number) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT id, sportType, startAt, endAt, distance, sportTime, pace FROM ${TABLES.app_records} WHERE startAt> ${ startAt } ORDER BY startAt DESC LIMIT 0, 10;`;
      this.db.executeSql(sql, []).then(resultSet => {
        console.log('find_10_records ok:');
        console.log(resultSet);
        let length = resultSet.rows.length;
        console.log(length);
        let tmpArr = [];
        if (length > 0) {
          for (let i = 0; i < length; i++) {
            console.log(resultSet.rows.item(i));
            tmpArr.push(resultSet.rows.item(i));
          }
          tmpArr.map(record => {
            record.distance = (record.distance / 1000).toFixed(2);
            record.pace =  moment().startOf('day').seconds(record.pace * 60).format(`mm'ss"`);
            record.sportTime =  moment().startOf('day').seconds(record.sportTime).format('HH:mm:ss');
            record.month = new Date(record.startAt * 1000).getFullYear() + '年' + (new Date(record.startAt * 1000).getMonth() + 1) + '月';
            record.showTitleTime = moment(record.startAt * 1000).format('DD日 HH:mm');
            return record;
          });
          
          this.appRecords = tmpArr.concat(this.appRecords);
          this.zone.run(() => {
            // this.appRecords = this.appRecords.concat(tmpArr);
            this.appRecords.sort((a, b) => b.startAt - a.startAt);
          });
          console.log(this.appRecords);
          // 本地有记录
          resolve(true);
        } else {
          console.log('Blank table, no data.');
          // 本地记录空了。这时候请注意，是否要联网获取更多记录。
          resolve(false);
          // this.currentSeaker = 0;
        }
      }, err => {
        console.error('find 10 local Records err, ' + err);
        reject(err);
      });
    });
  }

  loadRemoteAppRecordsNoRoute_10() {
    return new Promise((resolve, reject) => {
      let t1 = Date.now() / 1000;
      this.av.getRemoteAppRecordsWithoutRoute_10(this.currentSeaker).then(res => {
        console.log('请求时间： ' + (Date.now() / 1000 - t1) + 's');
        console.log(res);
        if (res instanceof Array && res.length > 0) {
          let objArray: Array<AppRecord> = [];
          res.map((i) => {
            let item: AppRecord = {
              sportType   : i.get('sportType') === undefined ? 0 : i.get('sportType'),
              userId      : i.get('userId'),
              startAt     : i.get('startAt'),
              endAt       : i.get('endAt'),
              sportRoute  : i.get('sportRoute'),
              distance    : i.get('distance'),     // m
              sportTime   : i.get('sportTime'),
              pace        : i.get('pace'),     // s
              sync        : SYNC.saved      // 0 || 1
            }
            console.log(item);
            objArray.push(item);
          });
          this.recoverAppRecordsBatchWithoutRoute(objArray);
          resolve(true);
        } else {
          resolve(false);
        }
      }).catch(err => reject(err));
    });
  }

  loadRemoteAppRecordsNoRoute_10_upToLatest(startAt: number) {
    return new Promise((resolve, reject) => {
      let t1 = Date.now() / 1000;
      this.av.getRemoteAppRecordsWithoutRoute_10_upToLatest(startAt).then(res => {
        console.log('请求时间： ' + (Date.now() / 1000 - t1) + 's');
        console.log(res);
        if (res instanceof Array && res.length > 0) {
          let objArray: Array<AppRecord> = [];
          res.map((i) => {
            let item: AppRecord = {
              sportType   : i.get('sportType') === undefined ? 0 : i.get('sportType'),
              userId      : i.get('userId'),
              startAt     : i.get('startAt'),
              endAt       : i.get('endAt'),
              sportRoute  : i.get('sportRoute'),
              distance    : i.get('distance'),     // m
              sportTime   : i.get('sportTime'),
              pace        : i.get('pace'),     // s
              sync        : SYNC.saved      // 0 || 1
            }
            console.log(item);
            objArray.push(item);
          });
          this.recoverAppRecordsBatchWithoutRoute_upToLatest(objArray, startAt);
          resolve(true);
        } else {
          resolve(false);
        }
      }).catch(err => reject(err));
    });
  }

  findAllRecords(table: string) {
    let sql = `SELECT * from ${table} ORDER BY startAt DESC;`;
    this.db.executeSql(sql, []).then(resultSet => {
      console.log('findAllRecords ok:');
      console.log(resultSet);
      let length = resultSet.rows.length;
      console.log(length);
      if (length > 0) {
        for (let i = 0; i < length; i++) {
          console.log(resultSet.rows.item(i));
        }
      } else {
        console.log('Blank table, no data.');
      }
    }, err => {
      console.error('findAllRecords err, ' + JSON.stringify(err));
    });
  }

  count(table: string) {
    return new Promise((resolve, reject) => {
      let sql = `SELECT COUNT(*) AS myLength FROM ${table};`;
      this.db.executeSql(sql, []).then(rs => {
        resolve(rs.rows.item(0).myLength);
      }, err => reject(err));
    });
  }


  // only for test: 清空表
  dropTable(table: string) {
    let sql = `DROP TABLE ${table};`;
    this.db.executeSql(sql, []).then(() => {
      console.log(`清空-${table}-表成功`);
      this.currentSeaker = null;
      this.zone.run(() => this.appRecords = []);
    }, err => {
      console.error(`清空-${table}-表失败: ` + JSON.stringify(err));
    });
  }


  // 无网后同步，非及时同步
  // ================================================================================================================================
  
  // getOneRemoteBleRecord
  getOneRemoteBleRecord (startAt: number, endAt: number) {
    return new Promise((resolve, reject) => {
      this.av.getOneRemoteBleRecordFull(startAt, endAt).then(res => {
        console.log(res);
        let bleRecord: BleRecord = null;
        if (res instanceof Array && res.length > 0) {
          // 有记录，目前仅处理一条记录
          let item = res[0];
          bleRecord = {
            sportType  : item.get('type'),
            userId     : item.get('userId'),
            startAt    : item.get('startAt'),
            stopType   : item.get('stopType'),
            data       : item.get('data').base64,
            sync       : SYNC.saved
          }
        } else {
          // 此条app运动记录没有对应的蓝牙记录, 插入一条空记录，防止以后发生无意义的网络请求
          bleRecord = {
            sportType  : null,
            userId     : null,
            startAt    : startAt,
            stopType   : null,
            data       : null,
            sync       : SYNC.saved
          }
        }
        console.log(bleRecord);
        this.recoverBleRecord(bleRecord);
        resolve(bleRecord);
      }).catch(err => reject(err));
    });
  }

  // 已同步过的条目中最新的时间戳取出，将服务器上大于此时间戳的拿下来。再排个序
  queryFirst(table: String) {
    let sql = `SELECT * FROM ${table} WHERE sync=1 ORDER BY startAt DESC LIMIT 1;`;
    return this.db.executeSql(sql, []);
  }

  // 将没有同步过的记录取出，全部上传
  syncAppRecordsLater() {
    let sql = `SELECT * FROM ${TABLES.app_records} WHERE sync=0;`;
    this.db.executeSql(sql, []).then(resultSet => {
      console.log('找出所有未同步过的本地记录 ok:');
      console.log(resultSet);
      let length = resultSet.rows.length;
      console.log('记录长度：' + length);
      if (length > 0) {
        let tmpArr: Array<any> = [];
        for (let i = 0; i < length; i++) {
          let obj = resultSet.rows.item(i);
          tmpArr.push({
            sportType  : obj.sportType,
            userId     : obj.userId,
            startAt    : obj.startAt,
            endAt      : obj.endAt,
            sportRoute : JSON.parse(obj.sportRoute),
            distance   : obj.distance,     // m
            sportTime  : obj.sportTime,
            pace       : obj.pace,     // s
            sync       : obj.sync  // 0 || 1
          });
        }

        this.av.uploadBatchAppRecords(tmpArr).then(() => {
          this.updateSyncStatusBach(TABLES.app_records, tmpArr, SYNC.saved);
        }).catch(err => console.error(err));

      } else {
        // console.log('app local No unsync data.');
        this.fs.logSaveInfo('App local records No unsync data.');
      }
    }).catch(err => console.error(err));
  }

  syncBleRecordsLater() {
    let sql = `SELECT * FROM ${TABLES.ble_records} WHERE sync=0;`;
    this.db.executeSql(sql, []).then(resultSet => {
      console.log('找出所有未同步过的本地记录 ok:');
      console.log(resultSet);
      let length = resultSet.rows.length;
      console.log('记录长度：' + length);
      if (length > 0) {
        let tmpArr: Array<any> = [];
        for (let i = 0; i < length; i++) {
          let obj = resultSet.rows.item(i);
          tmpArr.push({
            sportType  : obj.sportType,
            userId     : obj.userId,
            startAt    : obj.startAt,
            stopType   : obj.stopType,
            data       : obj.data,
            sync       : obj.sync  // 0 || 1
          });
        }

        this.av.uploadBatchBleRecords(tmpArr).then(() => {
          this.updateSyncStatusBach(TABLES.ble_records, tmpArr, SYNC.saved);
        }).catch(err => console.error(err));

      } else {
        // console.log('ble local No unsync data.');
        this.fs.logSaveInfo('Ble local records No unsync data.');
      }
    }).catch(err => console.error(err));
  }

  // 初始化的时候预先执行下拉刷新同步
  async doRefreshBackground() {
    this.fs.logSaveInfo('doRefreshBackground starts...');
    try {
      let res = await this.queryFirst(TABLES.app_records);
      let length = res.rows.length;
      if (length > 0) {
        let latestRecord = res.rows.item(0);
        console.log(latestRecord);
        try {
          let remoteHasData = await this.loadRemoteAppRecordsNoRoute_10_upToLatest(latestRecord.startAt);
          if (remoteHasData) {
            // 数据多，再次进行refresh 更新
            this.doRefreshBackground();
          } else {
            // 远程数据被同步光了，这时候停止更新 |=> 是否要上传同步记录，请思考
            console.log('远程数据被同步光了, 等待本地未同步数据上传...');
            this.fs.logSaveInfo('Remote app records has been synced out! Time to upload local nonSynced.');
            this.syncAppRecordsLater();
            this.syncBleRecordsLater();
          }
        } catch (err) {
          console.error(err);
          this.fs.logSaveErr('doRefreshBackground ' + JSON.stringify(err));
        }
      } else {
        // console.log('未查询到本地首条记录！');
        this.fs.logSaveInfo('未查询到本地首条记录！');
      }
    } catch (err) {
      console.log(err);
      this.fs.logSaveErr('doRefreshBackground ' + JSON.stringify(err));
    }
  }

}
