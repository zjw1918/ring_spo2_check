import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
// import { Camera, Crop } from 'ionic-native';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';

declare var plugins: any;

@Injectable()
export class CameraService {
  constructor(
    private crop: Crop,
    private camera: Camera,
    public platform: Platform) {}

  getPhoto(type: number): Promise<string> {
    const options: any = {
    // allowEdit: true,
    sourceType: type,
    // mediaType: Camera.MediaType.ALLMEDIA,
    mediaType: this.camera.MediaType.PICTURE,
    destinationType: this.camera.DestinationType.FILE_URI
  }

    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then(fileUri => {
        console.log(fileUri);
        
        if (this.platform.is('android')) {
          this.crop.crop(fileUri, { quality: 75 }).then(path => {
            console.log('Cropped Image Path!: ' + path);
            resolve(path);
          }).catch(err => { 
            console.error(err);
            reject(err);
           });
        }
      }).catch(err => {
        console.error(err);
        reject(err);
      });
    })
  }
  

}  