import { Injectable } from '@angular/core';
import { Platform,
  LoadingController, Loading,
  ToastController,
  AlertController, Alert } from 'ionic-angular';

import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';

import moment from 'moment';
import {FileTransfer} from "@ionic-native/file-transfer";
import {GLOBAL_STATUS} from "../utils/globalStatus";

@Injectable()
export class Utils {
  offline: boolean;
  phoneHardwareInfo;

  constructor(
    private device: Device,
    private transfer: FileTransfer,
    private platform: Platform,
    public network: Network
  ) {
    console.log('Hello Utils Provider');
    this.platform.ready().then(() => {
      // ready完在处理
      this.phoneHardwareInfo = {
        platform      : this.device.platform,
        version       : this.device.version,
        uuid          : this.device.uuid,
        manufacturer  : this.device.manufacturer,
        model         : this.device.model,
        serial        : this.device.serial,
      };
      logFormate(JSON.stringify(this.phoneHardwareInfo));
      this.offline = this.network.type === 'none';
      logFormate('是否离线？' + this.offline);

      // WifiWizard.getCurrentSSID(ssid => {
      //   console.log(ssid, ssid.toLowerCase(), ssid.toLowerCase() == `"megaemc"`);
      //   if (ssid.toLowerCase() == `"megaemc"`) {
      //     GLOBAL_STATUS._isMegaEmc = true;
      //   }
      // }, err => console.error(err));

      // this.network.onchange().subscribe((a) => {
      //   console.log(a);
      //   console.log('onchange', this.network.type);
      // });

      // 2018-09-18 15:24:01 v0.0.6 megaemc
      // this.network.onConnect().subscribe(() => {
      //   console.log('onConnect: network was connected :-) ' + this.network.type);
      //   this.offline = false;
      //   GLOBAL_STATUS._isMegaEmc = false;
      //   WifiWizard.getCurrentSSID(ssid => {
      //     console.log(ssid, ssid.toLowerCase(), ssid.toLowerCase() == `"megaemc"`);
      //     if (ssid.toLowerCase() == `"megaemc"`) {
      //       GLOBAL_STATUS._isMegaEmc = true;
      //     }
      //   }, err => console.error(err));
      // });
      // this.network.onDisconnect().subscribe(() => {
      //   console.log('onDisconnect: network was disconnected :-(' + this.network.type);
      //   this.offline = true;
      //   GLOBAL_STATUS._isMegaEmc = false;
      // });

      // WifiWizard.startScan(
      //   () => console.log('startScan ok...'),
      //   err => console.error(err));

      
      // for wjj
      var cd = 0
      setInterval(() => {
        WifiWizard.startScan(
          () => console.log('startScan ok...'),
          err => console.error(err));
        WifiWizard.getScanResults(null, list => {
          console.log('getScanResults ok', list.map(i => i.SSID))
          GLOBAL_STATUS._isMegaEmc = false;
          var now = Date.now() / 1000
          list.map(it => {
            // if (it.level > -90) {
            //   GLOBAL_STATUS._isMegaEmc = true;
            // }
            if (it['SSID'] && it['SSID'].toLowerCase().startsWith('wjj') && Math.abs(now - cd) > 18) {
              GLOBAL_STATUS._isMegaEmc = true;
              cd = now
            }
          })
          console.log('colddown: ', now - cd)
        }, err => {
          console.error('getScanResults error: ', err);
          GLOBAL_STATUS._isMegaEmc = false;
        });
      }, 3000)
    });

  }

  getTransfer() {
    return this.transfer.create();
  }

}

@Injectable()
export class ToastService {
  loader: Loading;
  alert: Alert;

  constructor(
    private alertCtrl: AlertController,
    private loaderCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  makeToast(msg: string, seconds?: number) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: seconds || 2000
    });
    toast.present();
  }

  createLoader(msg, millSeconds:number) {
    this.loader = this.loaderCtrl.create({
      content: msg,
      duration: millSeconds,
      // showBackdrop: false
    });
  }
  createInfiniteLoader(msg: string) {
    this.loader = this.loaderCtrl.create({
      content: msg,
      // showBackdrop: false
    });
  }

  createBasicAlert(title: string, msg: string, btnText: string) {
    this.alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      // enableBackdropDismiss: false,
      buttons: [btnText]
    });
    this.alert.onDidDismiss(() => this.alert = null);
  }
}

/**
 * **********************************************************************************
 * 下面的是导出普通方法
 */

/**
 * normal functions
 * @param arr
 * @param spliter
 */
export const splitArray = (arr: Array<any>, spliter) => {
    let top = [[]];
    let seeker = 0;
    arr.map(i => {
        if (i === spliter) {
            seeker++;
            top.push([])
        } else {
            top[seeker].push(i)
        }
    });
    return top.filter(i => i.length !== 0)
};

export const logFormate = (arg) => {
  console.log(`[${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}]  ${arg}`);
};

export const bytesToBase64 = (bytes: Array<number>) => {
  return btoa(String.fromCharCode.apply(null, bytes));
};
export const base64ToBytes = (str: string) => {
  return atob(str).split('').map(c => c.charCodeAt(0));
};

export const getDfuMac = (mac: string) => {
  return mac.slice(0, -2) + ('0x00' + (+( "0x" + mac.slice(-2)) + 1).toString(16)).slice(-2).toUpperCase().slice(-2);
  // return mac.slice(0, -2) + (+( "0x" + mac.slice(-2)) + 1).toString(16).toUpperCase().slice(-2);
  // 下面的是旧方法，但请勿删除
  // let macSn = +("0x" + mac.split(':').slice(-3).join('')) + 1;
  // let dfuSn = ((macSn & 0xff0000) >> 16).toString(16) + ':' + ((macSn & 0x00ff00) >> 8).toString(16) + ':' + ((macSn & 0x0000ff)).toString(16) + '';
  // return ([...mac.split(':').slice(0,3)].join(':') + ":" + dfuSn).toUpperCase();
};

export const parseBle = (base64str: string) => {
  let arr: Array<any> = base64ToBytes(base64str);
  let obj = {};
  let groupCount = arr.length / 256;
  let group: Array<Array<number>> = [];
  for (let i = 0; i < groupCount; i++) {
    let item = arr.slice(i * 256, (i + 1) * 256);
    group.push(item);
  }
  group.map(item => {
    let timestamp = item[0] | (item[1] << 8) | (item[2] << 16) | (item[3] << 24);
    let payload = item.slice(4, -4); // 后4位暂定'kcal'
    let length = payload.length;
    for (let i = 0; i < length; i++) {
      obj[timestamp++] = payload[i];
    }
  });
  return obj;
};

// high byte before, low after
export const decToArr4 = (val: number) => {
    return [
        (val & 0xff000000) >> 24,
        (val & 0x00ff0000) >> 16,
        (val & 0x0000ff00) >> 8,
        (val & 0x000000ff)
    ]
};
export const arr4ToDec = (arr: Array<any>) => {
    return (arr[0] << 24) | (arr[1] << 16) | (arr[2] << 8) | (arr[3])
};


export const parseBleSpo = (base64str: string) => {
  let preArr: Array<any> = base64ToBytes(base64str);
  let ver = arr4ToDec([preArr[0], preArr[1], preArr[2], preArr[3]]);
  // let arr = preArr.slice(4);
  let res = {};
  switch (ver) {
    case 1:
      // 版本号解析法暂定
      break;

    default:
      res = parseBleSpo_default(preArr);
      break;
  }
  return res;
};

const parseBleSpo_default = (arr: Array<any>) => {
    let o2Arr = []; //血氧数组
    let prArr = []; //心率数组
    let timeArr = []; //时间数组
    let min_spo2 = 100; //最小血氧值
    let min_spo2_time = 0; //最小血氧值出现的时间
    let average_spo2 = 0; //平均血氧

    let low95_time = 0; //小于95%血氧时间（s）
    let low90_time = 0; //小于90%血氧时间（s）
    let low85_time = 0; //小于85%血氧时间（s）
    let low80_time = 0; //小于80%血氧时间（s）
    let low70_time = 0; //小于70%血氧时间（s）
    let low60_time = 0; //小于60%血氧时间（s）

    let all_spo2 = 0; //全部血氧和，用于计算平均血氧

    let mx_1_time = 0; //记录门限1的时间
    let mx_1_spo2 = 0; //记录门限1的总血氧值
    let average_mx_1_spo2 = 0; //记录门限1的平均血氧值
    let max_mx_1_time = 0; //最长门限1的时间（若无法找到3min以上连续大于94血氧的门限1，则使用该门限）
    let max_mx_1_spo2 = 0; //最长门限1的总血氧值（若无法找到3min以上连续大于94血氧的门限1，则使用该门限）
    let dips4 = 0;   //与门限差值大于等于4%的次数
    let dips3 = 0;	 //与门限差值大于等于3%的次数
    let dips2 = 0;	 //与门限差值大于等于2%的次数


    for (let i = 0; i < arr.length;) {
        if(i%256 == 0){
            timeArr.push( arr[i] | (arr[i+1] << 8) | (arr[i+2] << 16) | (arr[i+3] << 24));
            let t = new Date(timeArr[timeArr.length-1] *1000);
            console.log(timeArr[timeArr.length-1],t.toLocaleString());
            i += 4;
            continue;
        }
        let d = (arr[i] >> 2) + 37;
        min_spo2_time = min_spo2 > d ? i: min_spo2_time;
        min_spo2 = min_spo2 > d ? d : min_spo2;
        if(d < 95){
            low95_time++;
        }else if(d < 90){
            low90_time++;
        }else if(d < 85){
            low85_time++;
        }else if(d < 80){
            low80_time++;
        }else if(d < 70){
            low70_time++;
        }else if(d < 60){
            low60_time++;
        }

        if(d > 94 && mx_1_time < 60*3){
            mx_1_time ++ ;
            mx_1_spo2 += d;
            max_mx_1_time = max_mx_1_time>mx_1_time?max_mx_1_time:mx_1_time;
            max_mx_1_spo2 = max_mx_1_time>mx_1_time?max_mx_1_spo2:mx_1_spo2;
        }else if (mx_1_time < 60*3){
            mx_1_time = 0;
            mx_1_spo2 = 0;
        }
        all_spo2 += d;
        o2Arr.push(d);
        prArr.push(arr[i+1]);
        i += 2;
    }
    if(mx_1_time < 60*3){
        mx_1_time = max_mx_1_time;
        average_mx_1_spo2 = max_mx_1_spo2/mx_1_time;
    }else{
        average_mx_1_spo2 = mx_1_spo2/mx_1_time;
    }

    let first_flag4 = (average_mx_1_spo2 - o2Arr[0]) >= 4; //数组第一位是否大于4
    let first_flag3 = (average_mx_1_spo2 - o2Arr[0]) >= 3; //数组第一位是否大于4
    let first_flag2 = (average_mx_1_spo2 - o2Arr[0]) >= 2; //数组第一位是否大于4
    dips4 = first_flag4?dips4++:dips4;
    dips3 = first_flag3?dips4++:dips3;
    dips2 = first_flag2?dips4++:dips2;
    let prev = 0;
    for(let i = 1,j=o2Arr.length;i<j;i++){
      if(average_mx_1_spo2 - o2Arr[i] >= 4 && average_mx_1_spo2 - prev < 4){
        dips4++;
      }
      if(average_mx_1_spo2 - o2Arr[i] >= 3 && average_mx_1_spo2 - prev < 3){
        dips3++;
      }
      if(average_mx_1_spo2 - o2Arr[i] >= 2 && average_mx_1_spo2 - prev < 2){
        dips2++;
      }
      prev = o2Arr[i];
    }

    average_spo2 = Math.round(all_spo2 / o2Arr.length);
    return {
        'o2Arr': o2Arr,
        'pr' : prArr,
        'time' : timeArr,
        'startTime' : new Date(timeArr[0]*1000 + 8*3600*1000).toUTCString().split(' ')[4],
        'totalTime' : new Date(o2Arr.length*1000).toUTCString().split(' ')[4],
        'min_spo2' : min_spo2,
        'min_spo2_time' : new Date(timeArr[0]*1000 + min_spo2_time*1000 + 8*3600*1000).toUTCString().split(' ')[4],
        'average_spo2' : average_spo2,
        'low95_time' : new Date(low95_time*1000).toUTCString().split(' ')[4],  //转换成00：00：00的格式
        'low90_time' : new Date(low90_time*1000).toUTCString().split(' ')[4],
        'low85_time' : new Date(low85_time*1000).toUTCString().split(' ')[4],
        'low80_time' : new Date(low80_time*1000).toUTCString().split(' ')[4],
        'low70_time' : new Date(low70_time*1000).toUTCString().split(' ')[4],
        'low60_time' : new Date(low60_time*1000).toUTCString().split(' ')[4],
        'low95_time_per' : (low95_time / o2Arr.length *100).toFixed(2),
        'low90_time_per' : (low90_time / o2Arr.length *100).toFixed(2),
        'low85_time_per' : (low85_time / o2Arr.length *100).toFixed(2),
        'low80_time_per' : (low80_time / o2Arr.length *100).toFixed(2),
        'low70_time_per' : (low70_time / o2Arr.length *100).toFixed(2),
        'low60_time_per' : (low60_time / o2Arr.length *100).toFixed(2),
        'dips4' : dips4,
        'dips3' : dips3,
        'dips2' : dips2,
        'dips4_hr' : (dips4/o2Arr.length*3600).toFixed(2),
        'dips3_hr' : (dips3/o2Arr.length*3600).toFixed(2),
        'dips2_hr' : (dips2/o2Arr.length*3600).toFixed(2),
    }
};

// 下面的是旧方法，但请勿删除
// export const parseBle = (base64str: string) => {
//   let arr: Array<any> = base64ToBytes(base64str);
//   let obj = {};
//   let groupCount = arr.length / 256;
//   let group: Array<Array<number>> = [];
//   for (let i = 0; i < groupCount; i++) {
//     let item = arr.slice(i * 256, (i + 1) * 256);
//     group.push(item);
//   }
//   group.map(item => {
//     let timestamp = item[0] | (item[1] << 8) | (item[2] << 16) | (item[3] << 24);
//     let payload = item.slice(4);
//     let length = payload.length;
//     for (let i = 0; i < length; i+=4) {
//       obj[timestamp++] = payload[i];
//     }
//   })
//   return obj;
// }

// export const

//=============================================================================================================
// baidu echart
export const drawTwoEcharts = (myChart, keys,
    values, min, max, interval, color, lineWidth,
    values2, min2, max2, interval2, color2, lineWidth2) => {
    let option = {
      // title: {
      //     show: false,
      //     text: '动态数据 + 时间坐标轴'
      // },

      grid: {
        padding: 0,
        borderWidth: 0,
        // x: '10%',
        y: '20%',
        // x2: '3%',
        // y2: '0%',
        // left: '',
        // right: '',
        // top: '40',
        bottom: '35'
      },
      color: [color, color2],

      legend: {
          data: [{
              name: '心率',
              // 强制设置图形为圆。
              icon: 'roundRect',
              // 设置文本为红色
              textStyle: {
                  color: '#787878'
              }
          }, {
              name: '配速',
              icon: 'roundRect',
              textStyle: {
                  color: '#787878'
              }
          }],
          align: 'right',
          x: 'left',
          left: 10
      },

      xAxis: [
        {
          // type: 'time',
          // show: false,
          type: 'category',
          boundaryGap: true,
          splitLine: {
              show: false
          },
          axisTick: {
            show: false
          },
          axisLine: {
            show: false,
            lineStyle: {
              color: '#787878'
            }
          },
          axisLabel: {
            textStyle: {
                color: '#787878'
            }
          },
          data: keys,
          // interval: 6
        }
        // {
        //   // type: 'time',
        //   // show: false,
        //   boundaryGap: false,
        //   splitLine: {
        //       show: false
        //   },
        //   axisTick: {
        //     show: false
        //   },
        //   axisLine: {
        //     show: false,
        //     lineStyle: {
        //       color: '#787878'
        //     }
        //   },
        //   axisLabel: {
        //     textStyle: {
        //         color: '#787878'
        //     }
        //   },
        //   data: keys2,
        //   // interval: 6
        // }
      ],
      yAxis: [
        {
          type: 'value',
          // name: '心率',
          // 显示刻度
          axisTick: {
            show: false
          },
          axisLine: {
            show: false,
            lineStyle: {
              color: '#787878'
            }
          },
          axisLabel: {
            textStyle: {
                color: '#787878'
            }
          },
          splitLine: {
              show: true,
              lineStyle: {
                  color: '#ffffff',
                  opacity: .1
              }
          },
          min: min, // 30
          max: max, // 240
          interval: interval // 30
        },
        {
          type: 'value',
          // name: '配速',
          // 显示刻度
          axisTick: {
            show: false
          },
          axisLine: {
            show: false,
            lineStyle: {
              color: '#787878'
            }
          },
          axisLabel: {
            textStyle: {
                color: '#787878'
            }
          },
          splitLine: {
              show: true,
              lineStyle: {
                  color: '#ffffff',
                  opacity: .1
              }
          },
          min: min2, // 30
          max: max2, // 240
          interval: interval2 // 30
        }
      ],
      series: [{
        name: '心率',
        type: 'line',
        lineStyle: {
          normal: {
            // color: color || '#FF0000',
            width: lineWidth || '2'
            // type: 'dashed'
          }
        },
        animation: false,
        showSymbol: false,
        // hoverAnimation: false,
        data: values,
        markPoint:{
            data:[
                {type : 'max', valueDim: "x", symbol: 'image://assets/imgs/chart_dot.png', symbolSize:14,itemStyle:{normal:{label:{position:'top',formatter:function(){return ""}}}}},
                // {type : 'min', name: '最小值', valueDim: "x", symbol: 'emptyCircle', symbolSize:3,itemStyle:{normal:{color:'#808080',label:{position:'bottom',formatter:function(){return ""}}}}}
            ]
        }
      },{
        name: '配速',
        type: 'line',
        // xAxisIndex: 1,
        yAxisIndex: 1,
        lineStyle: {
          normal: {
            // color: color2 || '#FF0000',
            width: lineWidth2 || '2'
            // type: 'dashed'
          }
        },
        animation: false,
        showSymbol: false,
        // hoverAnimation: false,
        data: values2,
        markPoint:{
            data:[
                {type : 'max', valueDim: "x", symbol: 'image://assets/imgs/chart_dot_blue.png', symbolSize:14,itemStyle:{normal:{label:{position:'top',formatter:function(){return ""}}}}},
                // {type : 'min', name: '最小值', valueDim: "x", symbol: 'emptyCircle', symbolSize:3,itemStyle:{normal:{color:'#808080',label:{position:'bottom',formatter:function(){return ""}}}}}
            ]
        }
      }]
    };
    myChart.setOption(option);
};

export const drawOneEchart = (myChart, keys, values, min, max, interval, color, lineWidth) => {
    let option = {
      // title: {
      //     show: false,
      //     text: '动态数据 + 时间坐标轴'
      // },

      grid: {
        padding: 0,
        borderWidth: 0,
        // x: '10%',
        // y: '0%',
        x2: '3%',
        // y2: '0%',
        // left: '',
        // right: '',
        top: '10%',
        bottom: '10%'
      },
      xAxis: {
        // type: 'time',
        show: false,
        boundaryGap: false,
        splitLine: {
            show: false
        },
        data: keys,
        // interval: 6
      },
      yAxis: {
        type: 'value',
        // 显示刻度
        axisTick: {
          show: false
        },
        axisLine: {
          show: false,
          lineStyle: {
            color: '#787878'
          }
        },
        axisLabel: {
          textStyle: {
              color: '#787878'
          }
        },
        splitLine: {
            show: true,
            lineStyle: {
                color: '#000000',
                opacity: .1
            }
        },
        min: min, // 30
        max: max, // 240
        interval: interval // 30
      },
      series: [{
        // name: '模拟数据',
        type: 'line',
        lineStyle: {
          normal: {
            color: color || '#FF0000',
            width: lineWidth || '2'
            // type: 'dashed'
          }
        },
        animation: false,
        showSymbol: false,
        // hoverAnimation: false,
        data: values,
        markPoint:{
            data:[
                {type : 'max', valueDim: "x", symbol: 'image://assets/imgs/chart_dot.png', symbolSize:14,itemStyle:{normal:{label:{position:'top',formatter:function(){return ""}}}}},
                // {type : 'min', name: '最小值', valueDim: "x", symbol: 'emptyCircle', symbolSize:3,itemStyle:{normal:{color:'#808080',label:{position:'bottom',formatter:function(){return ""}}}}}
            ]
        }
      }]
    };
    myChart.setOption(option);
  };
