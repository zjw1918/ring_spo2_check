import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

// native
import { File } from '@ionic-native/file';
import { Device } from '@ionic-native/device';
import { FileChooser } from '@ionic-native/file-chooser';
import { Zip } from '@ionic-native/zip';
import { FilePath } from '@ionic-native/file-path';
import { Utils } from '../providers/utils';


import moment from 'moment';
declare var cordova: any;

@Injectable()
export class FileService {
  public fs;
  public appRootPath;
  public localLogName;
  private localLogLevel = 1; // 暂时就0,1开关吧

  private globalLogBuffer: string = '';

  constructor(
    private utils: Utils,
    private platform: Platform,
    private file: File,
    private device: Device,
    private fileChooser: FileChooser,
    private zip: Zip,
    private filePath: FilePath,
  ) {
    console.log('Hello FileService Provider');
    platform.ready().then(() => {
      this.init();
    });

  }

  init() {
    console.log('当前系统：', this.device.platform);
    if (this.device.platform === 'Android') {
      // this.basePath = cordova.file.externalRootDirectory;//android sd
      this.appRootPath = cordova.file.externalDataDirectory;
      this.fs = cordova.file.externalDataDirectory; // android data app files
    } else {
      this.appRootPath = cordova.file.documentsDirectory;
      this.fs = cordova.file.documentsDirectory; // ios documents
    }
    console.log(this.fs);
  }

  initLocalLog(tmp?: string) {
    // 创建log
    this.localLogName = "log_" + moment().format("YYYY_MM_DD") + '.txt';
    console.log(this.fs + this.localLogName);
    this.writeFile(this.fs, this.localLogName, JSON.stringify(this.utils.phoneHardwareInfo) + tmp, false).then(() => {
      console.log('本地log文件创建成功。');
    }).catch(err => console.log(err));
  }

  checkFile(path, filename) {
    return this.file.checkFile(path, filename);
  }

  createDir(path, dirName, option) {
    return this.file.createDir(path, dirName, option);
  }

  createFile(path, filename, replace: boolean) {
    return this.file.createFile(path, filename, replace);
  }

  // warring! 此方法有问题，请暂时采用下面的【writeFile】方法
  // File api writeExistingFile 必须已有文件才能写入，否则报错
  // writeExistingFile(path, filename, data: any) {
  //   return this.file.writeExistingFile(path, filename, data);
  // }

  readAsText(pathSrc, filename) {
    return this.file.readAsText(pathSrc, filename);
  }

  readAsArrayBuffer(pathSrc, filename) {
    return this.file.readAsArrayBuffer(pathSrc, filename);
  }

  /**
   * option {
   *    replace?: boolean; append?: boolean; truncate?: number;
   * }
   */
  writeFile(path, filename, data: any, option) {
    return this.file.writeFile(path, filename, data, option);
  }

  deleteFile(path, filename) {
    return this.file.removeFile(path, filename);
  }

  fileToBase64(file) {
    return new Promise((resolve, reject)=>{
      let myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        resolve(myReader.result);
      }
      myReader.onerror = (e) => {
        reject(myReader.error);
      }
      myReader.readAsDataURL(file);
    });
  }

  fileToBase64All(files: Array<any>) {
    let promiseList = [];
    let nameList = [];

    for (let i = 0, length = files.length; i < length; i++) {
      promiseList.push(this.fileToBase64(files[i]));
      nameList.push(files[i].name)
    }
    return Promise.all(promiseList);
  }
  
  chooseFile() {
    return this.fileChooser.open();
  }

  resolveNativePath(uri) {
    return this.filePath.resolveNativePath(uri);
  }

  unZip(pathSrc, pathDist, cb) {
    return this.zip.unzip(pathSrc, pathDist, cb);
  }

  private logSave(arg: string, funcName: string) {
    if (this.localLogLevel === 0) {
      return;
    }
    let log = `[${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}] [${funcName}] ${arg}`;
    
    switch(funcName) {
      case 'Info':
        console.log(log);
      break;

      case 'Err':
        console.error(log);
      break;

      default:
      break;
    }

    // 送检版 暂时屏蔽写log文件
    // this.globalLogBuffer += ("\n" + log);
    // if (this.globalLogBuffer.length >= 500) {
    //   let tmp = this.globalLogBuffer;
    //   this.globalLogBuffer = '';
    //   this.writeFile(this.fs, this.localLogName, tmp, {replace: false, append: true}).then(() => {
    //     console.log('local log write ok.');
    //   }).catch(err => {
    //     console.error(err);
    //     if ('message' in err && err.message === 'NOT_FOUND_ERR') {
    //       this.initLocalLog(tmp);
    //     }
    //   });
    // }
  }
  logSaveInfo(arg: string) {
    this.logSave(arg, 'Info');
  }
  
  logSaveErr(arg: string) {
    this.logSave(arg, 'Err');
  }

  logSaveRest(arg: string) {
    return new Promise((resolve, reject) => {
      if (!this.localLogName) {
        resolve('fs not init');
        return;
      }

      let log = `[${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}]  ${arg}`;
      let tmp = this.globalLogBuffer + ("\n" + log);
      this.globalLogBuffer = '';
      this.writeFile(this.fs, this.localLogName, tmp, {replace: false, append: true}).then(() => {  // 追加文件
        console.log('local log write ok.');
        resolve(true);
      }).catch(err => {
        if ('message' in err && err.message === 'NOT_FOUND_ERR') {
          this.initLocalLog(tmp);
        }
        resolve(true);
      });

    });
  }


  // 下载相关
  // makeTransfer() {
  //   return new Transfer();
  // }
  // download(fileTransfer: Transfer, url: string, fsAndFileName: string) {
  //   // const fileTransfer = new Transfer();
    // fileTransfer.download(url, fsAndFileName).then((entry) => {
    //   console.log('download complete: ' + entry.toURL());
    // }, (error) => {
    //   // handle error
    // });
  // }


/**
 * 
readThis(inputValue: any): void {
  var file:File = inputValue.files[0];
  var myReader:FileReader = new FileReader();

  myReader.onloadend = (e) => {
    this.image = myReader.result;
  }
  myReader.readAsDataURL(file);
}
 */
}


// export const logSave = (path, filename, arg) => {
//     let content = `[${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}]  ${arg}`;
//     this.file.writeFile(this.fs, this.localLogName, content, true);
// }