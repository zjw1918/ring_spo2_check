import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Events, Platform } from 'ionic-angular';
import 'rxjs/add/operator/filter';

// native plugin
import { BLE } from '@ionic-native/ble';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

// provider
import { MyConfig } from '../providers/my-config';
import { FileService } from '../providers/file-service';
import { AVService } from '../providers/av';
import { SqliteService, BleRecord } from '../providers/sqlite-service';
import { ToastService, bytesToBase64, getDfuMac } from '../providers/utils'; // logFormate

// 3rd libs
import { crc16xmodem } from 'crc';
import moment from 'moment';
import aesjs from 'aes-js';

// config
import { CmdPort, CmdCtrl, BlePort, EventsMsg } from '../config/portAndcmd';
import {GLOBAL_STATUS} from "../utils/globalStatus";

const Interval = 12;
const DfuType = {
  bootloader: 2,
  application: 4
};
const MIN_RSSI = -90;

const cachedMac = 'cachedMac'

@Injectable()
export class BleService {
  isLoging: boolean;

  currentDeviceId: string;
  currentDevice;

  watcherIndi;
  watcherNoti;

  deviceVersion;
  fwVersion;
  battaryValue: string;
  steps: string;

  isDfuing;
  isRingRunning: boolean;
  colddown = 0;

  heartBeatInterval;

  //status
  currentStatus = {
    logStatus: false
  };

  // 2017-02-28 11:03:37 需求变更，实时心率改成单次心率检测。超时25秒
  $_live: Subject<any> = new Subject();

  constructor(
    private transfer: FileTransfer,
    private ble: BLE,
    private platform: Platform,
    private sqliteSvc: SqliteService,
    public toastSvc: ToastService,
    private zone: NgZone,
    private events: Events,
    private av: AVService,
    private fs: FileService,
    private cfg: MyConfig)
  {
    console.log('Hello BleService Provider');
    platform.ready().then(() => this.init());

    this.events.subscribe(EventsMsg.globalSteps, (step, data?) => {
      switch (step) {
        case EventsMsg.globalSteps_connect: // 连接
          console.log(`收到发起连接请求 >>> ${data}`);
          this.connect(data).subscribe(this.onConnected.bind(this), this.onDisConnected.bind(this));
          break;

        case EventsMsg.globalSteps_bindOk: // 伪绑ok
          console.log('globalSteps_bindOk');
          this.readDeviceInfo();
          break;

        case EventsMsg.globalSteps_readOk: // read device information ok => settime
          console.log('globalSteps_readOk');
          this.exec(CmdPort.setTime+'');
          break;

        case EventsMsg.globalSteps_setTimeOk:
          console.log('globalSteps_setTimeOk');
          this.exec(CmdPort.setUserInfo+'');
          break;

        case EventsMsg.globalSteps_setUserInfoOk:
          console.log('globalSteps_setUserInfoOk');
          setTimeout(() => {
            this.events.publish(EventsMsg.live);
          }, 1000);


          // 判断app自身是否处于运动状态， ring是否处于运动状态
          // let startAt = localStorage.getItem('startAt');
          // if (startAt) {
          //   // app running
          //   if (this.isRingRunning) {
          //     // ring running
          //     // do nothing

          //   } else {
          //     // ring not running
          //     this.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStart);
          //     setTimeout(() => {
          //       this.exec(CmdPort.runCtrl+'', CmdCtrl.runOn);
          //     }, 500);
          //   }

          // } else {
          //   // app not running
          //   if (this.isRingRunning) {
          //     // ring running
          //     this.exec(CmdPort.liveCtrl+'', CmdCtrl.liveStop);
          //     setTimeout(() => {
          //       this.exec(CmdPort.runCtrl+'', CmdCtrl.runOff);
          //       setTimeout(() => {
          //         this.exec(CmdPort.syncData+'', CmdCtrl.sportData);
          //       }, 500);
          //     }, 500);

          //   } else {
          //     // ring not running
          //     // both app and ring are
          //     this.exec(CmdPort.syncData+'', CmdCtrl.sportData);
          //     // it is time to check fw update
          //   }
          // }

          // setTimeout(() => {
          //   if (this.isLoging) {
          //     this.events.publish(EventsMsg.log);
          //   }
          // }, 5000);

          break;

        // case EventsMsg.globalSteps_toUpdateFw:
        //   // 检查dfu更新
        //   this.fs.logSaveInfo('Check fw updating...');
        //   if (this.cfg.remoteFwVersion) {
        //     if (this.cfg.remoteFwVersion !== this.fwVersion.split('.').pop()) {
        //       this.fs.logSaveInfo('Fw has new version. Update starts...');
        //       this.events.publish('up');
        //     } else {
        //       this.fs.logSaveInfo('Fw alread latest.');
        //     }
        //   } else {
        //     this.fs.logSaveInfo('Fw cannot update, offline!');
        //   }
        //   break;

        case '':

          break;

        default:
          break;
      }
    });
  }

  init() {
    this.ble.isEnabled().then(()=> {
      console.log('Ble is enabled.');
      setTimeout(() => {
        this.startScanForManager();
      }, 2000);
    }).catch(() => this.ble.enable());
  }

  getCurrentdevice() {
    return this.currentDeviceId;
  }

  clearCurrentDevice() {
    this.zone.run(() => {
      if (!this.isDfuing) this.currentDeviceId = null;
      // this.isDfuing ? null : this.currentDeviceId = null;
      this.currentDevice = null;
      this.deviceVersion = null;
      this.fwVersion = null;
      this.battaryValue = null;
      this.isRingRunning = null;
    });
    this.closeGlobalIndiAndNoti();
  }

  // clearBoundDevice() {
  //   this.zone.run(() => {
  //     localStorage.removeItem(StorageKey.boundDeviceRandom);
  //     localStorage.removeItem(StorageKey.boundDevice);
  //     this.boundDevice = null;
  //   });
  // }

  connect(id) {
    this.currentDeviceId = id;
    return this.ble.connect(id);
  }

  disconnect(id) {
    // this.changeBleStatus(null);
    return this.ble.disconnect(id);
  }

  startScan(arg) {
    return this.ble.startScan(arg);
  }

  stopScan() {
    return this.ble.stopScan();
  }

  read(){
    return this.ble.read(
      this.currentDeviceId,
      BlePort.ring_service_uuid,
      BlePort.ring_r_uuid
    );
  }

  // write
  write(buffer: ArrayBuffer) {
    return this.ble.write(this.currentDeviceId,
      BlePort.ring_service_uuid, BlePort.ring_w_uuid, buffer);
  }

  writeN(buffer: ArrayBuffer) {
    return this.ble.write(this.currentDeviceId,
      BlePort.ring_service_uuid, BlePort.ring_wn_uuid, buffer);
  }

  // toggle listenning
  listenOn(arg: string) {
    return this.ble.startNotification(
      this.currentDeviceId,
      BlePort.ring_service_uuid,
      arg.startsWith('i')? BlePort.ring_ind_uuid : BlePort.ring_not_uuid);
  }

  listenOff(arg: string) {
    return this.ble.stopNotification(
      this.currentDeviceId,
      BlePort.ring_service_uuid,
      arg.startsWith('i')? BlePort.ring_ind_uuid : BlePort.ring_not_uuid);
  }

  // ========================================================
  // new logic => only one global listen port===================>
  // ========================================================
  openGlobalIndiAndNoti() {
    if (this.watcherIndi) {
      return;
    }

    // init some package, for huge data recv
    let top1dict = null;
    let singleDict = null;
    let total_len = null;
    let sub_len = null;
    let app_report_pack: Uint8Array = null;
    let app_report_pack_misspart: Uint8Array = null;
    let crc_len = 2;
    let payload_len = 19;

    // indi
    this.watcherIndi = this.listenOn('indi').subscribe(buffer => {
      let a = new Uint8Array(buffer);
      // logFormate('>>> indicate ok: ' + hexArrayFormate(Array.from(a)));
      this.fs.logSaveInfo('>>> indicate ok: ' + hexArrayFormate(Array.from(a)));
      // console.log('>>> indicate ok: ', hexArrayFormate(Array.from(a)));

      switch (a[0]) { // a[0]代表命令字
        case CmdPort.fakeBind: // 伪绑定
          if (a[2] === 0) {
            if (a[3] === 0) { // 需要绑定 // 1 =》 伪绑新设备，往后取6位的随机数；0 =》 默认旧设备，后面无随机数，还用老随机数。
              let deviceRandom: string = [ a[4], a[5], a[6], a[7], a[8], a[9] ].join();
              this.fs.logSaveInfo('receive ramdom：' + deviceRandom);

              this.av.saveBoundInServer(this.currentDevice.id, deviceRandom).then(() => {
                this.zone.run(() => {
                  this.av.boundInfo.mac = this.currentDevice.id;
                  this.av.boundInfo.random = deviceRandom;
                  this.av.boundInfo.persistBoundInfo();
                });

                // this.zone.run(() => this.boundDevice = this.currentDevice.id);
                if (this.toastSvc.alert) {
                  this.toastSvc.alert.setTitle('');
                  this.toastSvc.alert.setSubTitle('绑定成功');
                  setTimeout(() => {
                    this.toastSvc.alert.dismiss();
                  }, 1500);
                }
                this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_bindOk);
              }).catch(err=> {
                this.fs.logSaveErr(JSON.stringify(err));
              });

            } else if (a[3] === 1) {
              this.fs.logSaveInfo('A bounded device，Skip binding.');
              this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_bindOk);

              this.heartBeatInterval = setInterval(() => this.exec(CmdPort.heartBeat+''), 15000);
            } else if (a[3] === 2) {
              // 提示敲击戒指（ble)
              this.fs.logSaveInfo('New device! Please knock knock knock!');

              // 此时有可能出现绑定设备信息过期的问题
              if (!this.av.boundInfo.mac) {  // 本地没有绑定信息的话，提示敲击是正常的
                if (!this.toastSvc.alert) {
                  this.toastSvc.createBasicAlert('请敲击戒指', '敲击或摇晃，以便绑定戒指。', '知道了');
                  this.toastSvc.alert.present();
                }
              } else {  // 本地有绑定信息的话，提示敲击，属于异常，作出相应提示！
                console.error(`本地有绑定设备: ${this.av.boundInfo.mac}，提示敲击，属于异常，作出相应提示！`);
                this.disconnect(this.currentDeviceId);
                this.clearCurrentDevice();
                this.toastSvc.createBasicAlert('设备匹配异常', '请重新登录', '知道了');
                this.toastSvc.alert.present();
              }
            }
          } else {
            console.error('ble indicate命令种类正确，但正忙，拒绝执行');
          }
        break;

        // huge data recv
        case CmdPort.syncData:
          // console.log('incicate 回应[首字节]正确，检查是否可以传输大数据？', a[0]);
          this.fs.logSaveInfo('Indicate response _cmd_ right, checking may trans big data? ' + a[0].toString(16));
          if (a[2] === 0) {
            this.fs.logSaveInfo('Trans permitted. ' + a[2]);
            // 初始化
            top1dict = [];
            singleDict = {};
            total_len = 0;
            sub_len = 0;
          } else if (a[2] === 0xA1) {
            console.log('设备低电，无法传输！', a[2].toString(16));
          } else if (a[2] === 2) {
            console.log('ble无运动数据可同步！' + a[2]);

            // 是否进入检查dfu升级
            // this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_toUpdateFw);

          } else if (a[2] === 0x23) {
            console.log('ble运动控制错误：' + a[2]);
          } else {
            console.log('设备正忙...无响应...未知错误码');
          }

          if (a[2] !== 0 && this.toastSvc.loader) {
            this.toastSvc.loader.dismiss();
          }

        break;
        case CmdCtrl.sportData:
          // console.log('incicate 回应[数据类型字节]正确，', a[0]);
          this.fs.logSaveInfo('Indicate response _dataType_ right.' + a[0]);
          if (a[2] === 0) {
            total_len = (a[3] << 24) | (a[4] << 16) | (a[5] << 8) | (a[6]);
            sub_len = ((a[7] << 8) | a[8]);
            // console.log('总长度：', total_len);
            // console.log('|--子长度：', sub_len);
            this.fs.logSaveInfo(`Total length: ${total_len} | Sub length: ${sub_len}`);
            //每次进入代表要传一个大包了，需要做一些初始化
            app_report_pack_misspart = new Uint8Array(16);
            singleDict = {};
          } else if(a[2] === 1) {
            //一包传完了，开始检查，有无漏包
            //a[0] === num_cmd && a[2] === 1 续传
            //a[0] === num_cmd && a[2] === 0 crc错误、有丢包
            let ref_num = Math.ceil((sub_len + crc_len) / payload_len);
            let missed_num = ref_num - Object.keys(singleDict).length;
            // console.log('应收包数：', ref_num);
            // console.log('丢包数：', missed_num);
            this.fs.logSaveInfo(`Packages should receive ${ref_num} | missed packs: ${sub_len}`);

            app_report_pack = new Uint8Array(20);
            app_report_pack[0] = CmdCtrl.sportData;
            app_report_pack[1] = a[1];//sn
            if (missed_num) {
              //有丢包，重传
              app_report_pack[2] = 0;//1 ：续传；0：丢包重传、crc错误重传
              app_report_pack[3] = missed_num;//通知丢包数
              app_report_pack.set(app_report_pack_misspart, 4);//-------->组完
              setTimeout(() => {
                this.write(app_report_pack.buffer).then(() => {
                  // logFormate('CallBack: write cmd ok! ' + hexArrayFormate(Array.from(app_report_pack)));
                  this.fs.logSaveInfo('CallBack: write cmd ok! ' + hexArrayFormate(Array.from(app_report_pack)));
                }).catch(err => console.error('命令写入失败:', err));
              }, 200);
            } else {
              //无丢包，检查crc
              let ready_for_check = [];
              for(let i in singleDict) {
                  ready_for_check = ready_for_check.concat(Array.from(singleDict[i]));
              }
              ready_for_check.splice(sub_len + 2);
              let tmp1 = ready_for_check.pop();
              let tmp2 = ready_for_check.pop();
              let crc_from_ble = (tmp1 | tmp2 << 8);

              let crc_from_app = crc16xmodem(ready_for_check);
              let crc_flag = (crc_from_ble === crc_from_app);
              if (crc_flag) {
                //crc相等，续传-------->组完
                app_report_pack[2] = 1; //1 ：续传；0：丢包重传、crc错误重传
                setTimeout(() => {
                  this.write(app_report_pack.buffer).then(() => {
                    // logFormate('write cmd ok >>> ' + hexArrayFormate(Array.from(app_report_pack)));
                    this.fs.logSaveInfo('write cmd ok! ' + hexArrayFormate(Array.from(app_report_pack)));
                  }).catch(err => console.error('命令写入失败:', err));
                }, 200);

                top1dict = top1dict.concat(ready_for_check);

                // loading 提示进度
                let percentStr = Math.floor((top1dict.length / total_len) * 100) + '%';
                console.log('接收数据中...' + percentStr);
                this.toastSvc.loader.setContent('保存数据中...' + percentStr);

                // 判断是否传完
                if (top1dict.length === total_len) {
                  // 传输完成
                  // logFormate('蓝牙大数据接收完毕，存档中...');
                  this.fs.logSaveInfo('Receiving ringSport Data ok! Saving...');
                  let bleRecord: BleRecord = {
                    sportType: 0,
                    userId: this.av.getCurrentUser().id,
                    startAt: (top1dict[3] << 24) | (top1dict[2] << 16) | (top1dict[1] << 8) | top1dict[0],
                    stopType: 0,
                    data: bytesToBase64(top1dict),
                    sync: 0
                  };
                  this.sqliteSvc.createOneBleRecord(bleRecord);

                  this.fs.writeFile(this.fs.fs, 'data_' + moment().format('MM_DD_HH_mm_ss') + '.bin', new Uint8Array(top1dict).buffer, true).then(() => {
                    // logFormate('蓝牙大数据存档完毕 :)');
                    this.fs.logSaveInfo('RingSport Data Saved ok :)');
                    setTimeout(() => {
                      top1dict = null;
                      singleDict = null;
                      total_len = null;
                      sub_len = null;
                      app_report_pack = null;
                      app_report_pack_misspart = null;
                    }, 300);

                    // 继续请求看ble有没有运动数据了。
                    setTimeout(() => {
                      this.exec(CmdPort.syncData+'', CmdCtrl.sportData);
                    }, 1300);
                  });
                }
              } else {
                //crc错，重传
                app_report_pack[2] = 0; //1 ：续传；0：丢包重传、crc错误重传
                app_report_pack[3] = 0xff;
                setTimeout(() => {
                  this.write(app_report_pack.buffer).then(() => { console.log('Callback: write cmd ok!', hexArrayFormate(Array.from(app_report_pack)))}).catch(err => console.error('命令写入失败:', err));
                }, 200);
              }
            }
          } else {
            console.error("0xEF's report package byte[2] is not 0 or 1, plz check!!");
          }
        break;

        case CmdPort.setTime: // set time
          if (a[2] === 0) {
            // console.log('Set time: ble indi status ok，continue...' + a[2]);
            this.fs.logSaveInfo('Set time: ble indi status ok, continue...' + a[2]);
            // a[3] ~ a[6] 代表时间戳
            let resTimestamp = (a[3] << 24) | (a[4] << 16) | (a[5] << 8) | a[6];
            // console.log(resTimestamp + '<=>' + new Date(resTimestamp * 1000).toLocaleString());
            this.fs.logSaveInfo(resTimestamp + '<=>' + new Date(resTimestamp * 1000).toString());
          } else {
            console.error('ble indicate命令种类正确，但正忙，拒绝执行');
          }

          this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_setTimeOk);
        break;

        case CmdPort.setUserInfo:
          if (a[2] === 0) {
            this.fs.logSaveInfo('SetUserInfo: ble indi status ok, continue...' + a[2]);
            // console.log('ble回复状态ok，可以继续执行');
            // console.log(`age:${a[3]}, gender:${a[4]}, height:${a[5]}cm, weight:${a[6]}kg, stepLength:${a[7]}cm`);
            this.fs.logSaveInfo(`age:${a[3]}, gender:${a[4]}, height:${a[5]}cm, weight:${a[6]}kg, stepLength:${a[7]}cm`);
          } else if (a[2] === 0xA1) {
            console.error('ble indicate命令种类正确，但设备低电。');
            this.toastSvc.createBasicAlert('目标设备低电', '请及时充电', '知道了');
            this.toastSvc.alert.present();
          } else {
            console.error('ble indicate命令种类正确，setUserInfo命令有异常');
          }
          this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_setUserInfoOk);
        break;

        case CmdPort.runCtrl:
        case CmdPort.runSpoLive:
          if (a[2] === 0) {
            console.log('ble回复状态ok，可以继续执行');
          } else if (a[2] === 0xA4) {
            console.error('ble正忙，拒绝执行：充电+低电');
            this.toastSvc.createBasicAlert('目标设备充电中...', '请充电完成再使用', '知道了');
            this.toastSvc.alert.present();
          } else {
            console.error('ble indicate命令种类正确，但正忙，拒绝执行, 未收录错误码。');
          }
        break;

        case CmdPort.liveCtrl:
          if (a[2] === 0) {
            console.log('ble回复状态ok，可以继续执行');
          } else if (a[2] === 0xA4) {
            console.error('ble正忙，拒绝执行：充电+低电');
            this.toastSvc.createBasicAlert('目标设备充电中...', '请充电完成再使用', '知道了');
            this.toastSvc.alert.present();
          } else {
            console.error('ble indicate命令种类正确，但正忙，拒绝执行, 未收录错误码。');
          }
        break;
        case CmdPort.heartBeat:
          // heaet response, ignore
          break;

        case CmdPort.findMe:
        case CmdPort.reset:
        case CmdPort.logSwitch:
          if (a[2] === 0) {
            console.log('ble回复状态ok，可以继续执行');
          } else {
            console.error('ble indicate命令种类正确，但正忙，拒绝执行');
          }
        break;

        default:
          console.error('ble indicate回复cmd类型有误！');
        break;
      }
    });

    this.watcherNoti = this.listenOn('noti').subscribe(buffer => {
      let a = new Uint8Array(buffer);
      if (this.currentStatus.logStatus) {
        this.events.publish(EventsMsg.log, a);
      } else {
        console.log(a.toString());
        switch (a[0]) {
          case CmdPort.liveCtrl: // 3 血氧, 4 心率； | 计步
            // wifi变动的需求 2017-09-27 10:39:10
            console.log('GLOBAL_STATUS: ' + GLOBAL_STATUS._isMegaEmc);
            console.log('GLOBAL_STATUS: ' + JSON.stringify(GLOBAL_STATUS.bufSpo) + ' - ' + JSON.stringify(GLOBAL_STATUS.bufHr));
            if (a[2] === 0) {
              // pass
            } else {
              a[3] = a[4];
              a[4] = a[5];
            }

            // for wjj
            if (GLOBAL_STATUS._isMegaEmc) {
              this.exec('0xe2')
              console.log('exec reset')
            }
            
            // for emc
            // if (GLOBAL_STATUS._isMegaEmc) { // 连上wifi的话就用正常值
            //   GLOBAL_STATUS.bufSpo.push(a[3]);
            //   GLOBAL_STATUS.bufHr.push(a[4]);
            //   if (GLOBAL_STATUS.bufSpo.length > 3) {
            //     GLOBAL_STATUS.bufSpo.shift();
            //     GLOBAL_STATUS.bufHr.shift();
            //   }
            // } else { // 断开wifi的话，就用buf值或默认值
            //   if (GLOBAL_STATUS.bufSpo.length > 0) {
            //     // 第一次wifi方案中的bufVal，不太适用
            //     // a[3] = GLOBAL_STATUS.bufSpo[Math.floor(Math.random() * GLOBAL_STATUS.bufSpo.length)];
            //     // a[4] = GLOBAL_STATUS.bufHr[Math.floor(Math.random() * GLOBAL_STATUS.bufHr.length)];
            //     // 修改bufVal方案
            //     let tmp = GLOBAL_STATUS.bufSpo[GLOBAL_STATUS.bufSpo.length - 1] + Math.round(Math.random());
            //     a[3] = tmp > 100 ? 100: tmp;
            //     a[4] = GLOBAL_STATUS.bufHr[GLOBAL_STATUS.bufHr.length - 1] + Math.round(Math.random());
            //   } else {
            //     a[3] = GLOBAL_STATUS.defaultSpo[Math.floor(Math.random() * GLOBAL_STATUS.defaultSpo.length)];
            //     a[4] = GLOBAL_STATUS.defaultHr[Math.floor(Math.random() * GLOBAL_STATUS.defaultHr.length)];
            //   }
            // }
            this.$_live.next(a);

            /**
             * 下面是正常业务，暂时注释掉
             */
            // this.$_live.next(a);
          break;

          case CmdPort.notiBatt:
          // cmd 0 0 (a[3]) (a[4])
            this.zone.run(() => {
              this.battaryValue = a[3] + '';
              this.events.publish(EventsMsg.updateDeviceManagerPage, {battary: this.battaryValue});
            });
          break;

          case CmdPort.notiStep:
            this.zone.run(() => {
              this.steps = ((a[3] << 8) | a[4]) + '';
            });
          break;

          default:
            // defult noti is for huge data recv
            if (top1dict !== null) { // 加一个判断，保证是在接收大数据的过程中才开这一步
              let sn = a[0];
              if (!singleDict[sn]) {
                console.log(sn);
                singleDict[sn] = a.subarray(1);
                app_report_pack_misspart[Math.floor(sn / 8)] |= 1 << (sn % 8);
              }
            }
          break;
        }
      }
    });
  }

  closeGlobalIndiAndNoti() {
    if (this.watcherIndi) {
      this.watcherIndi.unsubscribe();
      this.watcherNoti.unsubscribe();

      this.watcherIndi = null;
      this.watcherNoti = null;
    }
  }

/*==========================================================================================
 exec hub method， ble普通单条指令集散
 ===========================================================================================*/
  exec(cmdStr: string, cmdSwitch?:number) {
    let cmd_num: number = +cmdStr;
    let cmdPackage: Uint8Array = null;

    switch (cmd_num) {
      case CmdPort.setUserInfo:  // 用户身体情况， mock
        cmdPackage = new Uint8Array([CmdPort.setUserInfo, 0, 0, 25, 0, 170, 60, 0]); // 30, 0, 170, 60, 0
        break;

      case CmdPort.runCtrl:
        cmdPackage = new Uint8Array([CmdPort.runCtrl, 0, 0, 0x53]);
        break;
      case CmdPort.runSpoLive:
        cmdPackage = new Uint8Array([CmdPort.runSpoLive, 0, 0, 0x53]);
        break;
      
      case CmdPort.liveCtrl:
        cmdPackage = new Uint8Array([CmdPort.liveCtrl, 0, 0, cmdSwitch]);
        break;

      case CmdPort.logSwitch:
        cmdPackage = new Uint8Array([CmdPort.logSwitch, 0, 0, cmdSwitch]);
        this.currentStatus.logStatus = cmdSwitch === 0 ? true : false;
        break;

      case CmdPort.syncData:
        // 构造命令包
        cmdPackage = new Uint8Array(20);
        let sleepId = new Uint8Array(new ArrayBuffer(16)); // 16 length : sleepId
        cmdPackage[0] = cmd_num;
        cmdPackage[1] = 0;          //  sn
        cmdPackage[2] = 0;          //  status
        cmdPackage[3] = cmdSwitch;  //  0xEF，表示要获取何种数据类型
        cmdPackage.set(sleepId, 4); // 从下标为4的字节开始往后写16字节, 4~19 sleepId
        break;

      case CmdPort.setTime:
        let d = Math.floor(Date.now() / 1000);
        cmdPackage = new Uint8Array(7);
        cmdPackage[0] = CmdPort.setTime;//0xe0
        cmdPackage[1] = 0;//sn
        cmdPackage[2] = 0;//status
        cmdPackage[3] = (d & 0xff000000) >> 24;
        cmdPackage[4] = (d & 0x00ff0000) >> 16;
        cmdPackage[5] = (d & 0x0000ff00) >> 8;
        cmdPackage[6] = d & 0x000000ff;
        break;

      case CmdPort.reset:
        cmdPackage = new Uint8Array([CmdPort.reset, 0, 0, 0]);
        break;
      case CmdPort.findMe:
        cmdPackage = new Uint8Array([CmdPort.findMe, 0, 0, 0]);
        break;
      case CmdPort.heartBeat:
        cmdPackage = new Uint8Array([CmdPort.heartBeat, 0, 0, 1]);
        break;

      case CmdPort.fakeBind:  // 伪绑定
        // let tmp = [];
        // tmp.push(0xb0);   // cmd
        // tmp.push(0);      // sn
        // tmp.push(0);      // status
        // // <USRID+AES(MAC)[后6字节]，len 18B>和<USRID+AES(随机数)[后6字节]，len 18B>
        // // let userId = this.av.getCurrentUser().id; // eg: 5837288dc59e0d00577c5f9a
        // // let userId: any = "5837288dc59e0d00577c5f9a"; // eg: 5837288dc59e0d00577c5f9a
        // let userId = "5837288dc59e0d00577c5f9a".match(/.{1,2}/g).map(i => +('0x' + i));
        // userId.map(i => tmp.push(i)); // tmp => 2 + 12

        // // 判断是否有随机数：有，用随机数加密；无，用mac加密。加密完传aes的后6位
        // let tmpContentArr = null;

        // // if (this.av.boundInfo.random) { //deviceRandom eg: 012345
        // //   tmpContentArr = this.av.boundInfo.random.split(',').map(i => +i);
        // //   console.log('有随机数，', hexArrayFormate(tmpContentArr));
        // // } else { // mac: 11:22:33:44:55
        // //   tmpContentArr = this.currentDeviceId.split(':').map(i => +('0x' + i));
        // //   console.log('无随机数，', hexArrayFormate(tmpContentArr));
        // // }
        // tmpContentArr = this.currentDeviceId.split(':').map(i => +('0x' + i));
        // console.log('无随机数，', hexArrayFormate(tmpContentArr));

        // // key eg: 31 32 33 34 35 36 4d 65 67 61 2a 2a 2a 2a 2a 2a
        // let preparedKey = [
        //   tmpContentArr[0], tmpContentArr[1], tmpContentArr[2], tmpContentArr[3], tmpContentArr[4], tmpContentArr[5],
        //   'M'.charCodeAt(0), 'e'.charCodeAt(0), 'g'.charCodeAt(0), 'a'.charCodeAt(0),
        //   '*'.charCodeAt(0), '*'.charCodeAt(0), '*'.charCodeAt(0),
        //   '*'.charCodeAt(0), '*'.charCodeAt(0), '*'.charCodeAt(0),
        // ];
        // let preparedContent = [
        //   tmpContentArr[0], tmpContentArr[1], tmpContentArr[2], tmpContentArr[3], tmpContentArr[4], tmpContentArr[5],
        //   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        // ];

        // let aesEcb = new aesjs.ModeOfOperation.ecb(preparedKey);
        // let encryptedBytes = aesEcb.encrypt(preparedContent);
        // let encryptedBytesArray = Array.from(encryptedBytes).slice(-5); // aes mac完，取后x位
        // tmp = tmp.concat(encryptedBytesArray);

        let tmp = [0xb0, 0x00, 0x00, 0x35, 0x70, 0x4e, 0x68, 0x76, 0x58, 0x73, 0x6c, 0x42, 0x36, 0x54, 0x51, 0x60, 0x00, 0x33, 0x90, 0x71];
        cmdPackage = new Uint8Array(tmp);
        break;

      default:
        break;
    }

    // console.log(`预备写入ble执行命令(${cmdStr}):`, cmdPackage);
    // console.log(timestampS() + 'Start write >>> ' +);
    // logFormate('Start write >>> ' + hexArrayFormate(Array.from(cmdPackage)));
    this.fs.logSaveInfo('Start write >>> ' + hexArrayFormate(Array.from(cmdPackage)));
    setTimeout(() => {
      this.write(cmdPackage.buffer).then(() => {
        // logFormate('Write ok: ' + hexArrayFormate(Array.from(cmdPackage)));
        this.fs.logSaveInfo('Write ok: ' + hexArrayFormate(Array.from(cmdPackage)));
      }).catch(err => console.error('命令写入失败:', err));
    }, 200);
  }


  // 接收ble大数据 | deprecated
  // getHugeDataFromBle(cmdNum: string, syncNum: string) {
  //   let cmd_num = parseInt(cmdNum);
  //   let sync_num = parseInt(syncNum);

  //   // let t1, t2, t_all_1, t_all_2 = 0;
  //   let top1dict = [];
  //   let singleDict = {};
  //   let total_len = 0;
  //   let sub_len = 0;
  //   let app_report_pack;
  //   let app_report_pack_misspart;
  //   let crc_len = 2;
  //   let payload_len = 19;
  //   let resend_flag = false;

  //   // 依次开启indication、notification
  //   this.listenOn('indi').subscribe(
  //     (buffer) => {
  //       let a = new Uint8Array(buffer);
  //       console.log('大数据接收： indication收到的回应： ', a);

  //       switch (a[0]) {
  //           case cmd_num:
  //             console.log('incicate 回应[首字节]正确，检查是否可以传输大数据？', a[0]);
  //             if (a[2] === 0) {
  //               //first success
  //               console.log('可以传输...', a[2]);
  //             } else {
  //               console.log('设备正忙，无法传输！', a[2]);
  //               /**
  //                * todo 清理
  //                */
  //             }
  //             break;

  //           case sync_num:
  //             console.log('incicate 回应[数据类型字节]正确，', a[0]);
  //               if (a[2] === 0) {
  //                   total_len = (a[3] << 24) | (a[4] << 16) | (a[5] << 8) | (a[6]);
  //                   sub_len = ((a[7] << 8) | a[8]);
  //                   console.log('总长度：', total_len);
  //                   console.log('|--子长度：', sub_len);

  //                   //每次进入代表要传一个大包了，需要做一些初始化
  //                   app_report_pack_misspart = new Uint8Array(16);
  //                   singleDict = {};

  //               } else if(a[2] === 1){

  //                   //一包传完了，开始检查，有无漏包
  //                   //a[0] === num_cmd && a[2] === 1 续传
  //                   //a[0] === num_cmd && a[2] === 0 crc错误、有丢包
  //                   let ref_num = Math.ceil((sub_len + crc_len) / payload_len);
  //                   let missed_num = ref_num - Object.keys(singleDict).length;
  //                   console.log('应收包数：', ref_num);
  //                   console.log('丢包数：', missed_num);

  //                   app_report_pack = new Uint8Array(20);
  //                   app_report_pack[0] = sync_num;
  //                   app_report_pack[1] = a[1];//sn
  //                   if (missed_num) {
  //                       //有丢包，重传
  //                       resend_flag = true;
  //                       app_report_pack[2] = 0;//1 ：续传；0：丢包重传、crc错误重传
  //                       app_report_pack[3] = missed_num;//通知丢包数
  //                       app_report_pack.set(app_report_pack_misspart, 4);//-------->组完
  //                       this.write(app_report_pack.buffer).then(() => { console.log('write cmd ok >>> ', Array.from(app_report_pack).map(i => ("0" + i.toString(16)).slice(-2)).join(','))}).catch(err => console.error('命令写入失败:', err));
  //                   } else {
  //                       //无丢包，检查crc
  //                       let ready_for_check = [];
  //                       for(let i in singleDict){
  //                           //下面这个方法我也不知道怎么不对，还是老老实实用for循环拷贝吧
  //                           // ready_for_check = ready_for_check.concat(Array.apply([], singleDict[i]));
  //                           // 老方法，弃用
  //                           // for (let j = 0; j < singleDict[i].length; j++) {
  //                           //     ready_for_check.push(singleDict[i][j]);
  //                           // }
  //                           ready_for_check = ready_for_check.concat(Array.from(singleDict[i]));
  //                       }
  //                       ready_for_check.splice(sub_len + 2);
  //                       let tmp1 = ready_for_check.pop();
  //                       let tmp2 = ready_for_check.pop();
  //                       let crc_from_ble = (tmp1 | tmp2 << 8);

  //                       let crc_from_app = crc16xmodem(ready_for_check);
  //                       let crc_flag = (crc_from_ble === crc_from_app);
  //                       if (crc_flag) {
  //                           //crc相等，续传-------->组完
  //                           app_report_pack[2] = 1; //1 ：续传；0：丢包重传、crc错误重传
  //                           this.write(app_report_pack.buffer).then(() => { console.log('write cmd ok >>> ', Array.from(app_report_packcmdPackage).map(i => ("0" + i.toString(16)).slice(-2)).join(','))}).catch(err => console.error('命令写入失败:', err));

  //                           top1dict = top1dict.concat(ready_for_check);
  //                           resend_flag = false;

  //                           // loading 提示进度
  //                           // this.changeLogStatus('接收数据中...' + Math.floor((top1dict.length / total_len) * 100) + '%');
  //                           console.log('接收数据中...' + Math.floor((top1dict.length / total_len) * 100) + '%');

  //                           //判断是否传完
  //                           if (top1dict.length === total_len) {
  //                             // 传输完成
  //                             this.listenOff('noti').then(() => console.log('noti close ok')).catch(err => console.error('noti close failed.', err));
  //                             setTimeout(() => {
  //                               this.listenOff('indi').then(() => console.log('noti close ok')).catch(err => console.error('noti close failed.', err));
  //                             }, 500);

  //                             this.fs.writeFile(this.fs.fs, 'data_' + moment().format('MM_DD_HH_mm_ss') + '.bin', new Uint8Array(top1dict).buffer, true);
  //                               // call tmpCreateRecord to create a record
  //                               // window.localStorage.removeItem('monitorStatus');

  //                               // let recordTime = new Date().getTime();
  //                               // let recordName = 'zg_o2_upload_'+ new Date(recordTime).Format("yyyy-MM-dd-hh-mm-ss");

  //                               // let res = new Uint8Array(top1dict);
  //                               // $cordovaFile.writeFile(filePath, recordName +'.bin', res.buffer, true)
  //                               //     .then(function (success) {
  //                               //       $cordovaToast.showShortBottom('data has been saved as .bin');
  //                               //       // let localRecords = window.localStorage.getItem('localRecords');
  //                               //       // localRecords.push(recordTime);
  //                               //       // window.localStorage.removeItem('sleepId');
  //                               //       window.localStorage.removeItem('monitorStatus');
  //                               //       $rootScope.settings.monitorSetting.monitorStatus = 0;
  //                               //     }, function (error) {
  //                               //       alert(error);
  //                               //     });
  //                           }
  //                       } else {
  //                           //crc错，重传
  //                           app_report_pack[2] = 0; //1 ：续传；0：丢包重传、crc错误重传
  //                           app_report_pack[3] = 0xff;
  //                           resend_flag = true;
  //                           this.write(app_report_pack.buffer).then(() => { console.log('write cmd ok >>> ', Array.from(app_report_pack).map(i => ("0" + i.toString(16)).slice(-2)).join(','))}).catch(err => console.error('命令写入失败:', err));
  //                       }
  //                   }
  //               } else {
  //                   console.error("0xEF's report package byte[2] is not 0 or 1, plz check!!");
  //                   return;
  //               }
  //               break;

  //           default :
  //               console.error("cmd report error.");
  //               return;
  //       }
  //     },
  //     err => console.log(err)
  //   );

  //   setTimeout(() => {
  //     this.listenOn('noti').subscribe(
  //       (buffer) => {
  //         let a = new Uint8Array(buffer);
  //         let sn = a[0];
  //         if (!singleDict[sn]) {
  //             console.log(sn);
  //             singleDict[sn] = a.subarray(1);
  //             app_report_pack_misspart[Math.floor(sn / 8)] |= 1 << (sn % 8);
  //         }
  //       },
  //       err => console.log(err)
  //     );
  //   }, 500);

  //   // 构造命令包
  //   let data = new Uint8Array(20);
  //   let sleepId = new Uint8Array(new ArrayBuffer(16)); // 16 length 的sleepId
  //   data[0] = cmd_num;
  //   data[1] = 0;//sn
  //   data[2] = 0;//status
  //   data[3] = sync_num;// 0xEF，表示要获取何种数据类型
  //   //4~19 sleepId
  //   data.set(sleepId, 4); // 从下标为4的字节开始往后写16字节
  //   setTimeout(() => {
  //     this.write(data.buffer).then(() => console.log('cmd write ok:', data)).catch(err => console.error('cmd write 失败:', err));
  //   }, 1000);
  // }

  // 设备伪绑定，给设备设定userInfo, 一连上最先做
  // gender, age, height, weight, username, macOrRandomCode
  // setUserInfo() {
  //   let t8 = new Uint8Array(20);
  //   this.write(t8.buffer);
  // }

  // 读取设备信息
  readDeviceInfo() {
    return new Promise((ok, no) => {
      this.read().then(buffer => {
        let a = new Uint8Array(buffer);
        // logFormate('Read Device raw data: ' + hexArrayFormate(Array.from(a)));
        this.fs.logSaveInfo('Read Device raw data: ' + hexArrayFormate(Array.from(a)));

        let hw1 = (a[0] & 0xf0) >> 4;
        let hw2 = (a[0] & 0x0f);
        let sw1 = (a[1] & 0xf0) >> 4;
        let sw2 = (a[1] & 0x0f);
        let sw3 = (a[2] << 8) | a[3];
        let bl1 = (a[4] & 0xf0) >> 4;
        let bl2 = (a[4] & 0x0f);
        let sn = 'H01A' + ( (a[5] * 100 + a[6]) * 10000 + ( (a[7] << 24) | (a[8] << 16) | (a[9] << 8) | a[10] ) );

        let gSENSOR_4404_flag = byteToBits(a[11]).reverse(); // xyz
        let x = gSENSOR_4404_flag[0];
        let y = gSENSOR_4404_flag[1];
        let z = gSENSOR_4404_flag[2];
        let deviceCheck = 'I2C['  + (x ? 'y' : 'n') +'] '
                         +'GS['   + (y ? 'y' : 'n') +'] '
                         +'4404[' + (z ? 'y' : 'n') +'] ';
        let sport_flag = a[12] === 0 ? 'off' : (a[12] === 1 ? 'on' : 'pause');
        this.isRingRunning = a[12] === 0 ? false : (a[12] === 1 ? true : true);

        this.battaryValue = a[13] + '';
        this.events.publish(EventsMsg.updateDeviceManagerPage, {battary: this.battaryValue});

        let deviceInfo = `
          HW: v${hw1}.${hw2} SW: v${sw1}.${sw2}.${sw3}
          BL: v${bl1}.${bl2} SN: ${sn} 
          hwCheck: ${deviceCheck} run: ${sport_flag}
        `;

        this.fs.logSaveInfo(deviceInfo);
        this.zone.run(() => {
          this.steps = ((a[14] << 8) | a[15]) + '';
          this.deviceVersion = deviceInfo;
          this.fwVersion = `v${sw1}.${sw2}.${sw3}`;
        });
        // logFormate('read battary, steps: ' + this.battaryValue + ',' + this.steps);
        this.fs.logSaveInfo('Read battary, steps: ' + this.battaryValue + ',' + this.steps);
        this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_readOk, `v${sw1}.${sw2}.${sw3}`);
        ok(deviceInfo);
      }).catch(err => no(err));
    });
  }

  // 实时心率显示
  // startBroadcastLive() {
  //   // return new Observable(observer => {
  //     this.listenOn('indi').subscribe(buffer => {
  //       console.log('app get indi:');
  //       console.log(new Uint8Array(buffer));
  //     }, err => console.error(err));

  //     setTimeout(() => {
  //       this.listenOn('noti').subscribe(
  //         buffer => {
  //           let a = new Uint8Array(buffer);
  //           console.log(a);
  //           this.changeLiveStatus(a);
  //         },
  //         err => {console.error(err)}
  //       );
  //     }, 500);

  //     setTimeout(() => {
  //       this.write(CmdBuffer.startLivebuffer.buffer).then(() => {
  //         console.log('cmd_startLive ok:', CmdBuffer.startLivebuffer);
  //       }).catch(err => console.error(err));
  //     }, 1000);
  // }

  // stopBroadcastLive() {
  //   this.write(CmdBuffer.stopLivebuffer.buffer).then(() => {
  //     console.log('cmd_stopLive ok:' + CmdBuffer.stopLivebuffer);
  //   }).catch(err => console.error(err));
  // }



  /**************************************************************************
   * dfu
   **************************************************************************/
  readDfu(){
    // return this.ble.read(
    return this.ble.read(
      this.currentDeviceId,
      BlePort.dfu_service_uuid,
      BlePort.dfu_version_uuid
    );
  }

  writeDfuCtrl(buffer: ArrayBuffer) {
    return this.ble.write(this.currentDeviceId,
      BlePort.dfu_service_uuid, BlePort.dfu_ctrl_uuid, buffer);
  }
  writeDfuPack(buffer: ArrayBuffer) {
    return this.ble.write(this.currentDeviceId,
      BlePort.dfu_service_uuid, BlePort.dfu_pack_uuid, buffer);
  }
  writeDfuPackNoResp(buffer: ArrayBuffer) {
    return this.ble.writeWithoutResponse(this.currentDeviceId,
      BlePort.dfu_service_uuid, BlePort.dfu_pack_uuid, buffer);
  }

  listenDfuOn() {
    return this.ble.startNotification(
      this.currentDeviceId,
      BlePort.dfu_service_uuid,
      BlePort.dfu_ctrl_uuid);
  }

  listenDfuOff() {
    return this.ble.stopNotification(
      this.currentDeviceId,
      BlePort.dfu_service_uuid,
      BlePort.dfu_ctrl_uuid);
  }

  scanDfu() {
    // let bindMac = localStorage.getItem(StorageKey.boundDevice);
    // let dfuMac = getDfuMac(bindMac); // 此处如果没有bindMac，怎么办？
    let dfuMac = getDfuMac(this.currentDeviceId);

    // logFormate('目标dfu Mac: ' + dfuMac);
    this.fs.logSaveInfo('Target dfu mac: ' + dfuMac);
    return new Promise((resolve, reject) => {
      this.ble.startScan([]).subscribe(peripheral => {
        console.log(peripheral);
        // 以前的判断条件不精确
        // if (('name' in peripheral) && (peripheral.name.toLowerCase().indexOf('dfu') !== -1)) {
        if (dfuMac === peripheral.id.toUpperCase()) {
          console.log('找到设备： ', peripheral);
          this.ble.stopScan();
          this.currentDeviceId = peripheral.id;
          setTimeout(() => {
            resolve(peripheral.id);
          }, 1000);
        }
      }, err => reject(err))
    });
  }

  // choose dfu file, unzip, read bin and dat file
  dfuPrepareFile(path) {
    return new Promise((resolve, reject) => {
      let dfuDir = this.fs.fs + '/DFU/';
      let updateType: string;

      this.fs.unZip(path, dfuDir, (progress) => console.log('Unzipping, ' + Math.round((progress.loaded / progress.total) * 100) + '%'))
        .then(result => {
          if(result === 0) {
            console.log('解压成功');

            this.fs.readAsText(dfuDir, 'manifest.json').then(res => {
              console.log('read dfu files: ', res);
              if (typeof res === 'string') {
                let indexObj = JSON.parse(res);
                let binFile, datFile;

                if ('application' in indexObj.manifest) {
                  updateType = 'application';
                  binFile = indexObj.manifest.application.bin_file;
                  datFile = indexObj.manifest.application.dat_file;
                } else if ('bootloader' in indexObj.manifest) {
                  updateType = 'bootloader';
                  binFile = indexObj.manifest.bootloader.bin_file;
                  datFile = indexObj.manifest.bootloader.dat_file;
                }

                // 读bin_file文件，获取：文件总长度，和每20字节分包的结果
                let a = this.fs.readAsArrayBuffer(dfuDir, binFile).then(buffer => {
                  if (buffer instanceof ArrayBuffer) {
                    console.log(buffer.byteLength);
                    let devidedBuffer =  cutBuffer(buffer, 20);
                    devidedBuffer = devidedBuffer.map((i) => new Uint8Array(i));
                    return [buffer.byteLength, devidedBuffer];
                  }
                });
                // read dat_file, get init header package
                let b =  this.fs.readAsArrayBuffer(dfuDir, datFile).then(buffer => {
                  if (buffer instanceof ArrayBuffer) {
                    let a = new Uint8Array(buffer);
                    return a;
                  }
                });

                // 上面两个文件都读完了之后，开始连接蓝牙，发包
                Promise.all([a, b]).then(res => {
                  console.log(res);
                  // updateType, sentLength, payload, initPack
                  resolve([ updateType, res[0][0], res[0][1], res[1] ]);
                }).catch(err => reject(err));
              }
            }).catch(err => reject(err));
          } else if(result === -1) {
            reject('解压失败');
          }
        }).catch(err => reject(err));
    })
  }

  /**
   * start dfu
   */
  // private payload;
  // startDfu(updateType, sentLength, payload, initPack) {
  //   // this.dfuT1 = Date.now();
  //   this.payload = payload;
  //   this.isDfuing = true;
  //   console.log('初始化DFU中...');
  //   // this.events.publish(EventsMsg.updateProgressFw, '初始化升级...');
  //   if (this.currentDevice['services'] && this.currentDevice.services.length === 3) {
  //     // 设备目前正处于dfu模式，直接进行dfu
  //     this.runDfu(updateType, sentLength, initPack);
  //   } else {
  //     // 设备目前处于普通模式，需要重启，以进入dfu模式
  //     this.readDfu().then(buffer => {
  //       console.log('Version: ', new Uint8Array(buffer));
  //       this.listenDfuOn().subscribe(
  //         () => {},
  //         err => console.error(err)
  //       );

  //       setTimeout(() => {
  //         this.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
  //           console.log('第1次写入成功: ', [0x01, DfuType[updateType]]);
  //         }).catch(err => console.error(err));

  //         setTimeout(() => {
  //           this.secondConnect(updateType, sentLength, payload, initPack);
  //         }, 800);
  //       }, 1000);
  //     }).catch(err => console.error(err));
  //   }
  // }
  // secondConnect(updateType, sentLength, payload, initPack) {
  //   this.scanDfu().then(deviceId => {
  //     this.connect(deviceId).subscribe(peripheral => {
  //       // this.ble.changeBleStatus(peripheral);
  //       console.log('第2次连上OK: ', peripheral);
  //       setTimeout(() => {
  //         this.runDfu(updateType, sentLength, initPack);
  //       }, 2000);
  //     }, err => console.error(err));
  //   }).catch(err => console.error(err));
  // }

  // runDfu(updateType, sentLength, initPack) {
  //   console.log('DFU process started >>>');
  //   this.readDfu().then(buffer => {
  //     console.log('版本：', new Uint8Array(buffer));
  //     this.listenDfuOn().subscribe((buffer) => {
  //       if (buffer) {
  //         let info = new Uint8Array(buffer);
  //         console.log('ble 返回的状态：', info);
  //         switch (info.toString()) {
  //           case [0x10, 0x01, 0x01].toString():
  //             console.log('进入第一个noti判断了。');
  //             this.writeDfuCtrl(new Uint8Array([0x02, 0x00]).buffer).then(()=>{
  //               console.log('Writing Initialize DFU Parameters...', [2, 0]);
  //               this.writeDfuPack(initPack.buffer).then(()=>{
  //                 console.log('Sent header package OK: ', initPack);
  //                 this.writeDfuCtrl(new Uint8Array([0x02, 0x01]).buffer).then(() => {
  //                   console.log('Sent cmd OK: ', [2, 1]);
  //                 }).catch(err => console.error(err));
  //               }).catch(err => console.error(err));
  //             }).catch(err => console.error(err));
  //             break;
  //           case [0x10, 0x02, 0x01].toString():
  //               this.writeDfuCtrl(new Uint8Array([0x03]).buffer).then(()=>{
  //                 console.log('Sent cmd OK: ', [3]);

  //                 // ble, 终于要给你丫送数据了，接着吧
  //                 this.sendPayload();
  //               }).catch(err => console.error(err));
  //               break;
  //           case [0x10, 0x03, 0x01].toString():
  //             // this.payloadCount = 0;
  //             this.writeDfuCtrl(new Uint8Array([0x04]).buffer).then(()=>{
  //               console.log('Sent cmd OK: ', [4]);
  //             }).catch(err => console.error(err));
  //             break;
  //           case [0x10, 0x04, 0x01].toString():
  //             this.writeDfuCtrl(new Uint8Array([0x05]).buffer).then(()=>{
  //               console.log('Sent cmd OK: ', [5]);
  //               this.listenDfuOff().then(() => console.log('关闭notify成功，DFU完成'))
  //                 .catch(err => console.error(err));

  //               // this.dfuT2 = Date.now();
  //               // this.dfuLog += '共用时：' + (this.dfuT2 - this.dfuT1)/1000 + 's';
  //               // console.log('共用时：' + (this.dfuT2 - this.dfuT1)/1000 + 's');

  //               // this.dfuLog = null;
  //               this.currentDevice = null;
  //             }).catch(err => console.error(err));
  //             break;

  //           default:
  //             if (info[0] === 0x11) {
  //               let alreadyReceiveLength = info[1] | (info[2] << 8) | (info[3] << 16) | (info[4] << 24);
  //               console.log('ble报告-已收到：', alreadyReceiveLength, '总：' + sentLength);
  //               this.sendPayload();
  //             } else {
  //               console.error('dfu出现未识别命令');
  //             }
  //             break;
  //           }
  //         }
  //       }, err => console.error(err));

  //     setTimeout(() => {
  //       this.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
  //         console.log('第2次写入成功:', [0x01, DfuType[updateType]]);
  //         let data;
  //         if (updateType.startsWith('a')) {
  //           data = new Uint32Array([0, 0, sentLength]);
  //         } else if(updateType.startsWith('b')) {
  //           data = new Uint32Array([0, sentLength, 0]);
  //         }
  //         this.writeDfuPack(data.buffer).then(() => console.log('Sent length pack OK.', data)).catch(err => console.error(err));
  //       }).catch(err => console.error(err));
  //     }, 1000);
  //   }).catch(err => console.error(err));
  // }

  // sendPayload () {
  //   let t1 = Date.now();
  //   let count = 0;
  //   let totalLength = this.payload.length;

  //   let onError = (err) => {
  //     console.error(err);
  //   }
  //   let callBack = () => {
  //     let tt1 = Date.now();
  //     // this.progressLoader.setContent(Math.ceil(count / totalLength * 100) + '%');
  //     if (!this.payload[count]) {
  //       console.log('All sent.');
  //       this.payload = null;
  //       this.isDfuing = false;
  //       this.events.publish(EventsMsg.updateDeviceManagerPage, {fwUpdate: true});
  //       console.log((Date.now() - t1)/1000 + 's');
  //       return;
  //     }
  //     this.writeDfuPackNoResp(this.payload[count++].buffer).then(() => {
  //       console.log(Date.now() - tt1);
  //       callBack();
  //     }).catch(onError);

  //     // this.ble.writeWithoutResponse(this.currentDeviceId, BlePort.dfu_service_uuid, BlePort.dfu_pack_uuid, this.payload[count++].buffer).then(() => {
  //     //   console.log(Date.now() - tt1);
  //     //   callBack();
  //     // }).catch(onError);
  //   }
  //   this.writeDfuPackNoResp(this.payload[count++].buffer).then(() => callBack()).catch(onError);
  //   // this.ble.writeWithoutResponse(this.currentDeviceId, BlePort.dfu_service_uuid, BlePort.dfu_pack_uuid, this.payload[count++].buffer).then(() => {
  //   //   callBack();
  //   // }).catch(onError);
  // }


  /********************************************************************************************************************************************
   * Below is advanced method   *****************************************************************************************************************
   * checkFwUpdate              *****************************************************************************************************************
   ********************************************************************************************************************************************/
  // progressLoader: Loading;
  // checkFwUpdate() {
  //   this.progressLoader = this.toastSvc.createLoader('正在检查固件更新...');
  //   this.progressLoader.present();

  //   this.av.queryUpdate('FwUpdate').then(data => {
  //     let remoteVersion = data.get('version');

  //     if (this.fwVersion) {
  //       console.log(remoteVersion, this.fwVersion);
  //       if (this.fwVersion.split('.').pop() !== remoteVersion) {
  //         let url = data.get('file').get('url');
  //         let name = 'DFU.zip'; // data.get('file').get('name');
  //         let transfer = new Transfer();
  //         transfer.download(url, this.fs.fs + name).then((entry) => {

  //           // this.zone.run(() => {this.progressBarLoader.set(1)});
  //           let path = entry.toURL();
  //           console.log('download complete: ' + path);
  //           this.dfuPrepareFile(path).then(res => {
  //             let updateType = res[0];
  //             let sentLength = res[1];
  //             let payload = res[2];
  //             let initPack = res[3];

  //             this.startDfu(updateType, sentLength, payload, initPack);

  //           }).catch(err => console.log(err));

  //         }, (err) => {
  //           console.error(err);
  //           // this.isDownloading = false;
  //         });
  //         transfer.onProgress((progressEvent) => {
  //           if (progressEvent.lengthComputable) {
  //             // this.zone.run(() => {this.progressBarLoader.set(progressEvent.loaded / progressEvent.total)});
  //             // this.events.publish(EventsMsg.updateProgressFw, Math.ceil(progressEvent.loaded * 100 / progressEvent.total) + '%');
  //             this.progressLoader.setContent(Math.ceil(progressEvent.loaded * 100 / progressEvent.total) + '%');
  //             console.log(Math.ceil(progressEvent.loaded * 100 / progressEvent.total) + '%');
  //           } else {
  //               // loadingStatus.increment();
  //           }
  //         })
  //       } else {
  //         this.events.publish(EventsMsg.updateDeviceManagerPage, {fwUpdate: 'no-update'});
  //       }
  //     }
  //   });
  // }

  checkFwUpdatePromise() {
    return new Promise((resolve, reject) => {
      this.av.queryUpdate('FwUpdate').then(data => {
        let remoteVersion = data.get('version');
        if (this.fwVersion) {
          console.log(remoteVersion, this.fwVersion);
          if (this.fwVersion.split('.').pop() !== remoteVersion) {
            let url = data.get('file').get('url');
            let name = 'DFU.zip'; // data.get('file').get('name');
            const transfer: FileTransferObject = this.transfer.create();
            transfer.download(url, this.fs.fs + name).then((entry) => {

              // this.zone.run(() => {this.progressBarLoader.set(1)});
              let path = entry.toURL();
              console.log('download complete: ' + path);
              this.dfuPrepareFile(path).then(res => {
                resolve(res);
              }).catch(err => reject(err));

            }, (err) => {
              reject(err);
            });
          } else {
            reject('already-new')
          }
        }
      }).catch(err => reject(err));
    });
  }

  /**
   * device manager page
   */

  // startScanForManager_back() {
  //   this.zone.run(() => {
  //     this.isScanning = true;
  //   });
  //   let localBoundDevice = localStorage.getItem(StorageKey.boundDevice);

  //   this.startScan([])
  //   .filter(v => {
  //     return (
  //       // v.id === 'F6:A6:CD:1B:35:D2') // 2017-03-22 11:02:43 测试 指定mac
  //       ('name' in v )
  //       && (v.name.toLowerCase().indexOf('ring') !== -1)
  //       && (v.rssi > MIN_RSSI))
  //   })
  //   .map(v => {
  //     v.advertising = new Uint8Array(v.advertising);
  //     return v;
  //   }).subscribe(v => {
  //     // this.ble.stopScan();
  //     // 找到符合条件的第一个设备，从设备广播里获取mac
  //     console.log(v);
  //     let a = v.advertising;
  //     let mac = [a[15], a[16], a[17], a[18], a[19], a[20]].reverse().map(i => ('0' + i.toString(16)).slice(-2)).join(':').toUpperCase();
  //     console.log('找到符合条件的第一个设备: ' + mac);
  //     if (localBoundDevice) {
  //       // 本地有已绑定的设备，对比
  //       console.log('对比设备信息中...(本地)' + localBoundDevice + ' vs ' + mac);
  //       if (mac == localBoundDevice) {
  //         this.stopScan();
  //         clearTimeout(this.scanningTimeouter);
  //         setTimeout(() => {
  //           console.log('绑定匹配ok，准备发出连接请求');
  //           this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, mac);
  //         }, 1300);
  //       } else {
  //         console.log("非绑定设备，已忽略。");
  //       }
  //     } else {
  //       // 本地没有任何设备
  //       this.stopScan();
  //       clearTimeout(this.scanningTimeouter);
  //       setTimeout(() => {
  //         this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, mac);
  //       }, 1300);
  //     }
  //   }, err => console.error(err));

  //   this.scanningTimeouter = setTimeout(() => {
  //     if (this.isScanning) {
  //       clearInterval(this.scanningTimeouter);
  //       this.scanningTimeouter = null;
  //       // this.isScanning = false;
  //       this.stopScan().then(() => {
  //           console.log("Scan complete");
  //       }).catch((err) => {
  //           console.error("stopScan failed" + JSON.stringify(err));
  //       });

  //       setTimeout(() => {
  //         console.log('扫描超时，等待2秒重新扫描...');
  //         this.startScanForManager();
  //       }, 2000);
  //     }
  //   }, 20000);
  // }

  scanMaxCounter: number = 0;
  isScanning;
  scanningTimeouter;
  startScanForManager() {
    this.zone.run(() => {
      this.isScanning = true;
    });
    // let localBoundDevice = this.av.boundInfo.mac;
    // this.fs.logSaveInfo('New Scan starts... | Already bound device: ' + localBoundDevice);

    let tmpDevicesDict = {};
    this.ble.startScanWithOptions([], { reportDuplicates: true }).filter(v => {
      if (('name' in v ) && (v.name.toLowerCase().indexOf('ring') !== -1)) {
        this.fs.logSaveInfo('All rings around: ' + v.id + '   ' + v.rssi);
      }
      return (('name' in v )
        && (v.name.toLowerCase().indexOf('ring') !== -1)
        && (v.rssi > MIN_RSSI))
    }).map(v => {
      let a = new Uint8Array(v.advertising);
      v.advertising = a;
      v.mac = [a[15], a[16], a[17], a[18], a[19], a[20]].reverse().map(i => ('0' + i.toString(16)).slice(-2)).join(':').toUpperCase();
      return v;
    })
    // .filter(v => {
    //   // return localBoundDevice ? localBoundDevice === v.mac : true;
    //   if (localBoundDevice) {
    //     if (localBoundDevice === v.mac) {
    //       this.ble.stopScan();
    //       clearTimeout(this.scanningTimeouter);
    //       setTimeout(onScanOver, 1000);
    //       return true;
    //     }
    //     return false;
    //     // return localBoundDevice === v.mac;
    //   }
    //   return true;
    // })
    .subscribe(device => {
      tmpDevicesDict[device.mac] = device;
      // tmpDevices.push(device);
      // logFormate(device.mac + '   ' + device.rssi);
      this.fs.logSaveInfo('Filtered ring: ' + device.id + '   ' + device.rssi);
    }, err => console.error(err));

    let onScanOver = () => {
      this.ble.stopScan();
      let keys = Object.keys(tmpDevicesDict);
      if (keys.length > 0) {
        // 按照rssi强度从大到小排序
        let tmpDevices = keys.map(key => tmpDevicesDict[key]);
        tmpDevices.sort((a, b) => {
          return b.rssi - a.rssi;
        });

        // logFormate('本次扫描到的设备：' + JSON.stringify(tmpDevices.map(device => [ device.mac, device.rssi ])));
        this.fs.logSaveInfo('Scan stop! | Rings found: ' + JSON.stringify(tmpDevices.map(device => [ device.mac, device.rssi ])));


        // 关键环节需求更新： 无绑定状态下，发起绑定，需要先联网查询该mac是否已经绑定！！
        // if (!localBoundDevice) {
        //   this.av.queryMac(tmpDevices[0].mac).then(res => {
        //     if (res.length === 0) {
        //       // this mac is open to be bound
        //       this.fs.logSaveInfo('this mac is open to be bound :)');
        //       this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, tmpDevices[0].mac);
        //     } else {
        //       this.clearScanner();
        //       this.toastSvc.createBasicAlert('绑定未成功', '目标设备已被他人绑定，需对方解绑。请靠近自己的戒指并重新连接！', '知道了');
        //       this.toastSvc.alert.present();
        //       this.fs.logSaveInfo(`Bind Err: ${tmpDevices[0].mac} ! Already bound by others.`);
        //     }
        //   }).catch(err => {
        //     this.clearScanner();
        //     this.toastSvc.createBasicAlert('绑定未成功', '网络出了点问题', '知道了');
        //     this.toastSvc.alert.present();
        //     console.error(err);
        //     this.fs.logSaveErr(`Bind Err: ${tmpDevices[0].mac} ! Offline.`);
        //   })
        // } else {
        //   // 本地有绑定
        //   this.fs.logSaveInfo('Fire an intent to connect to: ' + localBoundDevice);
        //   this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, tmpDevices[0].mac);
        // }
        this.fs.logSaveInfo('Fire an intent to connect to: ' + tmpDevices[0].mac);
        this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, tmpDevices[0].mac);
        // this.clearScanner();
      } else {
        // logFormate('本次20s扫描到设备数：' + 0);
        this.fs.logSaveInfo('Scan stop! | After 20s 0 rings found');
        // todo 扫描超时

        if (this.scanMaxCounter >= 15) {
          // 酒过三巡app还没找到ring， 那就不再扫描了
          // logFormate('酒过三巡app还没找到ring ' + this.scanMaxCounter);
          this.fs.logSaveInfo('Scan 15 times out. Stop Scan.' + this.scanMaxCounter);
          this.clearScanner();
          return;
        }

        setTimeout(() => {
          this.startScanForManager();
          this.scanMaxCounter++;
        }, 1000);
      }
    };
    this.scanningTimeouter = setTimeout(onScanOver, 5 * 1000);
  }

  listDevices: Array<any>;
  watcherIntervalScan;
  startScanInfinite() {
    this.zone.run(() => {
      this.isScanning = true;
    });
    this.fs.logSaveInfo('New Scan starts...');

    this.listDevices = [];
    let tmpDevicesDict = {};
    this.ble.startScanWithOptions([], { reportDuplicates: true })
    .filter(v => {
      if (('name' in v ) && (v.name.toLowerCase().indexOf('ring') !== -1)) {
        this.fs.logSaveInfo('All rings around: ' + v.id + '   ' + v.rssi);
      }
      return (('name' in v )
        && (v.name.toLowerCase().indexOf('ring') !== -1)
        && (v.rssi > MIN_RSSI))
    })
    .map(v => {
      let a = new Uint8Array(v.advertising);
      v.advertising = a;
      v.mac = [a[15], a[16], a[17], a[18], a[19], a[20]].reverse().map(i => ('0' + i.toString(16)).slice(-2)).join(':').toUpperCase();
      return v;
    }).subscribe(device => {
      tmpDevicesDict[device.mac] = device;
    }, err => console.error(err));

    let counter = 0;
    this.watcherIntervalScan = setInterval(() => {
      counter++;
      if (counter > 80) {
        this.clearScannerInfinite();
      }

      this.zone.run(() => {
        this.listDevices = Object.keys(tmpDevicesDict).map(key => {
          let tmpDevice = tmpDevicesDict[key];
          return {
            mac : tmpDevice.mac,
            name: tmpDevice.name,
            rssi: tmpDevice.rssi
          }
        });
      });
    }, 1000);
  }

  clearScannerInfinite() {
    clearInterval(this.watcherIntervalScan);
    this.watcherIntervalScan = null;
    this.ble.stopScan();
    this.zone.run(() => {
      this.isScanning = false;
    });
  }

  clearScanner() {
    clearTimeout(this.scanningTimeouter);
    this.ble.stopScan();
    this.zone.run(() => {
      this.isScanning = false;
      this.scanMaxCounter = 0;
    });
  }

  // private scanWithNoBound() {

  // }

  // private scanWithBound() {

  // }

  // startScanForManager() {
  //   // let tmpDevices :Array<any> = [];
  //   this.zone.run(() => {
  //     this.isScanning = true;
  //   });
  //   let localBoundDevice = localStorage.getItem(StorageKey.boundDevice);

  //   this.startScan([])
  //   .filter(v => {
  //     return (
  //       // v.id === 'F6:A6:CD:1B:35:D2') // 2017-03-22 11:02:43 测试 指定mac
  //       ('name' in v )
  //       && (v.name.toLowerCase().indexOf('ring') !== -1)
  //       && (v.rssi > MIN_RSSI))
  //   })
  //   .map(v => {
  //     v.advertising = new Uint8Array(v.advertising);
  //     return v;
  //   }).subscribe(v => {
  //     // this.ble.stopScan();
  //     // 找到符合条件的第一个设备，从设备广播里获取mac
  //     console.log(v);
  //     let a = v.advertising;
  //     let mac = [a[15], a[16], a[17], a[18], a[19], a[20]].reverse().map(i => ('0' + i.toString(16)).slice(-2)).join(':').toUpperCase();
  //     console.log('找到符合条件的第一个设备: ' + mac);
  //     if (localBoundDevice) {
  //       // 本地有已绑定的设备，对比
  //       console.log('对比设备信息中...(本地)' + localBoundDevice + ' vs ' + mac);
  //       if (mac == localBoundDevice) {
  //         this.stopScan();
  //         clearTimeout(this.scanningTimeouter);
  //         setTimeout(() => {
  //           console.log('绑定匹配ok，准备发出连接请求');
  //           this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, mac);
  //         }, 1300);
  //       } else {
  //         console.log("非绑定设备，已忽略。");
  //       }
  //     } else {
  //       // 本地没有任何设备
  //       this.stopScan();
  //       clearTimeout(this.scanningTimeouter);
  //       setTimeout(() => {
  //         this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, mac);
  //       }, 1300);
  //     }
  //   }, err => console.error(err));

  //   this.scanningTimeouter = setTimeout(() => {
  //     if (this.isScanning) {
  //       clearInterval(this.scanningTimeouter);
  //       this.scanningTimeouter = null;
  //       // this.isScanning = false;
  //       this.stopScan().then(() => {
  //           console.log("Scan complete");
  //       }).catch((err) => {
  //           console.error("stopScan failed" + JSON.stringify(err));
  //       });

  //       setTimeout(() => {
  //         console.log('扫描超时，等待2秒重新扫描...');
  //         this.startScanForManager();
  //       }, 2000);
  //     }
  //   }, 20000);
  // }

  /**
   * global connect callbacks
   * onConnected
   */
  onConnected(peripheral) {
    this.fs.logSaveInfo('Ble connect ok. mac: ' + peripheral.id);
    localStorage.setItem(cachedMac, peripheral.id)
    this.zone.run(() => {
      this.clearScanner();
      this.currentDevice = peripheral;
      this.currentDeviceId = peripheral.id;
      let a = new Uint8Array(peripheral.advertising);

      // console.log('Battary in advertising: ', a[35]);
      this.fs.logSaveInfo('Battary in advertising: ' + a[35]);
    });

    // 验证绑定
    this.openGlobalIndiAndNoti();
    setTimeout(() => {
      this.exec(CmdPort.fakeBind+'');
    }, 1000);
  }

  onDisConnected(err) {
    GLOBAL_STATUS.bufSpo = [];
    GLOBAL_STATUS.bufHr = [];

    this.zone.run(() => {
      this.isScanning = true;
    });
    // console.error('注意：蓝牙断开', JSON.stringify(err));
    clearInterval(this.heartBeatInterval);
    this.fs.logSaveInfo('!! Ble disconnected. mac: ' + JSON.stringify(err));
    this.clearCurrentDevice();
    this.events.publish(EventsMsg.updateDeviceManagerPage, {battary: '0'});
    if (!this.isDfuing) {
      setTimeout(() => {
        // console.log('断开后，等待2秒重新扫描...');
        // this.fs.logSaveInfo('After 2s, start rescanning...');
        // this.startScanForManager();

        var mac = localStorage.getItem(cachedMac);
        if (mac) {
          console.log('断开后，等待2秒重新连接...');
          this.events.publish(EventsMsg.globalSteps, EventsMsg.globalSteps_connect, mac);
        } else {
          console.log('断开后，等待2秒重新扫描...');
          this.startScanForManager();
        }
      }, 2000);
    }

  }

}

// outer function====================================================================================
const byteToBits = (octet) => {
  let bits = [];
  for (let i = 7; i >= 0; i--) {
      let bit = octet & (1 << i) ? 1 : 0;
      bits.push(bit);
  }
  return bits;
};

const cutBuffer = (arr: ArrayBuffer, offset: number) => {
  let obj = [];
  let t = Math.ceil(arr.byteLength / offset);

  for (let i = 0; i < t; i++){
      obj.push(arr.slice(offset*(i), offset*(i+1)));
  }
  return obj;
};

const hexArrayFormate = (arr: Array<any>) => {
  return arr.map(i => ("0" + i.toString(16)).slice(-2)).join(',');
};


// old

  // readDeviceInfo_old() {
  //   let double_hex = (str) => {
  //       return str.length === 1 ? '0' + str : str;
  //   }
  //   return new Promise((ok, no) => {
  //     this.read().then(buffer => {
  //       let a = new Uint8Array(buffer);

  //       let hw1 = (a[0] & 0xf0) >> 4;
  //       let hw2 = (a[0] & 0x0f);
  //       let sw1 = (a[1] & 0xf0) >> 4;
  //       let sw2 = (a[1] & 0x0f);
  //       let sw3 = (a[2] << 8) | a[3];
  //       let mac = double_hex(a[4].toString(16)) + ':'
  //               + double_hex(a[5].toString(16)) + ':'
  //               + double_hex(a[6].toString(16)) + ':'
  //               + double_hex(a[7].toString(16)) + ':'
  //               + double_hex(a[8].toString(16)) + ':'
  //               + double_hex(a[9].toString(16)) + ':';
  //       let sn = 'H01A' + ( (a[10] * 100 + a[11]) * 10000 + ( (a[12] << 24) | (a[13] << 16) | (a[14] << 8) | a[15] ) );

  //       let deviceInfo = `
  //         HW: v${hw1}.${hw2} SW: v${sw1}.${sw2}.${sw3}
  //         MAC: ${mac} SN: ${sn}
  //       `;

  //       this.deviceVersion = deviceInfo;
  //       ok(deviceInfo);
  //     }).catch(err => no(err));
  //   });
  // }

//==
  // stopBroadcastLive() {
  //   console.log('clear live (hr,o2) ok');
  //   this.write(CmdBuffer.stopLivebuffer.buffer).then(() => {
  //     console.log('cmd_stopLive ok:' + CmdBuffer.stopLivebuffer);
  //   }).catch(err => console.error(err));

  //   setTimeout(() => {
  //     this.listenOff('indi').then(() => {
  //       console.log('stop ind ok');
  //     }).catch(err => {console.error(err)});
  //   }, 500);

  //   setTimeout(() => {
  //     this.listenOff('noti').then(() => {
  //       console.log('stop not ok');
  //     }).catch(err => {console.error(err)});
  //   }, 1000);
  // }

//==
  // exec(cmdStr: string, cmdCtrl?) {
  //   let cmd_num: number = +cmdStr;
  //   let cmdPackage: Uint8Array = null;

  //   switch (cmd_num) {
  //     case CmdPort.setUserInfo:
  //       cmdPackage = new Uint8Array([CmdPort.setUserInfo, 0, 0, 25, 0, 170, 60, 0]); // 30, 0, 170, 60, 0
  //       break;

  //     case CmdPort.runCtrl:
  //       cmdPackage = new Uint8Array([CmdPort.runCtrl, 0, 0, cmdCtrl]);
  //       break;

  //     case CmdPort.setTime:
  //       let d = Math.floor(Date.now() / 1000);
  //       cmdPackage = new Uint8Array(7);
  //       cmdPackage[0] = CmdPort.setTime;//0xe0
  //       cmdPackage[1] = 0;//sn
  //       cmdPackage[2] = 0;//status
  //       cmdPackage[3] = (d & 0xff000000) >> 24;
  //       cmdPackage[4] = (d & 0x00ff0000) >> 16;
  //       cmdPackage[5] = (d & 0x0000ff00) >> 8;
  //       cmdPackage[6] = d & 0x000000ff;
  //       break;

  //     case CmdPort.reset:
  //       cmdPackage = new Uint8Array([CmdPort.reset, 0, 0, 0]);
  //       break;
  //     case CmdPort.findMe:
  //       cmdPackage = new Uint8Array([CmdPort.findMe, 0, 0, 0]);
  //       break;

  //     case CmdPort.fakeBind:  // 伪绑定
  //       let tmp = [];
  //       tmp.push(0xb0);   // cmd
  //       tmp.push(0);      // sn
  //       tmp.push(0);      // status
  //       // <USRID+AES(MAC)[后6字节]，len 18B>和<USRID+AES(随机数)[后6字节]，len 18B>
  //       let userId = this.av.getCurrentUser().id; // eg: 5837288dc59e0d00577c5f9a
  //       userId = userId.match(/.{1,2}/g).map(i => +('0x' + i));
  //       userId.map(i => tmp.push(i)); // tmp => 2 + 12

  //       // 判断是否有随机数：有，用随机数加密；无，用mac加密。加密完传aes的后6位
  //       let deviceRandom: string = this.av.getCurrentUser().get('deviceRandom');
  //       let tmpContentArr = null;

  //       if (deviceRandom) { //deviceRandom eg: 012345
  //         tmpContentArr = deviceRandom.split(',').map(i => +i);
  //         console.log('有随机数，', tmpContentArr);

  //       } else { // mac: 11:22:33:44:55
  //         tmpContentArr = this.currentDeviceId.split(':').map(i => +('0x' + i));
  //         console.log('无随机数，', tmpContentArr);
  //       }

  //       // 31 32 33 34 35 36 4d 65 67 61 2a 2a 2a 2a 2a 2a
  //       let preparedKey = [
  //         tmpContentArr[0], tmpContentArr[1], tmpContentArr[2], tmpContentArr[3], tmpContentArr[4], tmpContentArr[5],
  //         'M'.charCodeAt(0), 'e'.charCodeAt(0), 'g'.charCodeAt(0), 'a'.charCodeAt(0),
  //         '*'.charCodeAt(0), '*'.charCodeAt(0), '*'.charCodeAt(0),
  //         '*'.charCodeAt(0), '*'.charCodeAt(0), '*'.charCodeAt(0),
  //       ];
  //       let preparedContent = [
  //         tmpContentArr[0], tmpContentArr[1], tmpContentArr[2], tmpContentArr[3], tmpContentArr[4], tmpContentArr[5],
  //         0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  //       ];

  //       let key = aesjs.util.convertStringToBytes(preparedKey);
  //       let textBytes = aesjs.util.convertStringToBytes(preparedContent);

  //       let aesEcb = new aesjs.ModeOfOperation.ecb(key);
  //       let encryptedBytes = aesEcb.encrypt(textBytes);
  //       let encryptedBytesArray = Array.from(encryptedBytes).slice(-5); // aes mac完，取后x位
  //       tmp = tmp.concat(encryptedBytesArray);

  //       cmdPackage = new Uint8Array(tmp);
  //       console.log('握手命令包： ', cmdPackage);
  //       break;

  //     default:
  //       break;
  //   }

  //   let indiWatcher = this.listenOn('indi').subscribe(buffer => {
  //     let a = new Uint8Array(buffer);
  //     console.log('app收到的indicate回复:', a);
  //     console.log("比较发收命令: " + a[0] + 'vs' + cmd_num);

  //     switch (a[0]) { // a[0]代表命令字
  //       case CmdPort.setUserInfo:
  //         if (a[2] === 0) {
  //           console.log('ble回复状态ok，可以继续执行');
  //           console.log(`age:${a[3]}, gender:${a[4]}, height:${a[5]}cm, weight:${a[6]}kg, stepLength:${a[7]}cm`);
  //         } else {
  //           console.error('ble indicate命令种类正确，运动命令有异常');
  //         }
  //         break;

  //       case CmdPort.runCtrl:
  //         if (a[2] === 0) {
  //           console.log('ble回复状态ok，可以继续执行');
  //         } else if (a[2] === 0x23) {
  //           console.error('ble indicate命令种类正确，运动命令有异常');
  //         }
  //         break;

  //       case CmdPort.setTime: // set time
  //         if (a[2] === 0) {
  //           console.log('ble回复状态ok，可以继续执行');
  //           // a[3] ~ a[6] 代表时间戳
  //           let resTimestamp = (a[3] << 24) | (a[4] << 16) | (a[5] << 8) | a[6];
  //           console.log(resTimestamp + '<=>' + new Date(resTimestamp * 1000).toLocaleString());

  //         } else {
  //           console.error('ble indicate命令种类正确，但正忙，拒绝执行');
  //         }
  //         break;

  //       case CmdPort.findMe:
  //       case CmdPort.reset:
  //         if (a[2] === 0) {
  //           console.log('ble回复状态ok，可以继续执行');
  //         } else {
  //           console.error('ble indicate命令种类正确，但正忙，拒绝执行');
  //         }
  //         break;

  //       case CmdPort.fakeBind: // 伪绑定
  //         if (a[2] === 0) {
  //           if (a[3] === 0) { // 需要绑定 // 1 =》 伪绑新设备，往后取6位的随机数；0 =》 默认旧设备，后面无随机数，还用老随机数。
  //             let deviceRandom: string = [ a[4], a[5], a[6], a[7], a[8], a[9] ].join();
  //             let currentUser = this.av.getCurrentUser();
  //             currentUser.set('deviceRandom', deviceRandom);
  //             currentUser.save();
  //           } else if (a[3] === 1) {
  //             console.log('这是一台已绑定设备，跳过绑定步骤。');
  //           }
  //         } else {
  //           console.error('ble indicate命令种类正确，但正忙，拒绝执行');
  //         }
  //         break;

  //       default:
  //         console.error('ble indicate回复cmd类型有误！');
  //         break;
  //     }

  //     // 单指令结束，清除监听
  //     indiWatcher.unsubscribe();

  //   }, err => console.error(err));

  //   console.log(`预备写入ble执行命令(${cmdStr}):`, cmdPackage);
  //   setTimeout(() => {
  //     this.write(cmdPackage.buffer).then(() => {
  //       console.log('命令写入成功:', cmdPackage);
  //     }).catch(err => console.error('命令写入失败:', err));
  //   }, 300);
  // }

@Injectable()
export class DfuService {
  // dfuProgressIndicator: Loading;
  dfuOver: boolean;

  constructor(
    private transfer: FileTransfer,
    // private navCtrl: NavController,
    private events: Events,
    private zone: NgZone,
    private toastSvc: ToastService,
    private fs: FileService,
    private av: AVService,
    private ble: BleService)
  {
    events.subscribe("up", () => {
      console.log('DfuService receive event msg to checkFwUpdate...');
      setTimeout(() => {
        this.checkFwUpdatePromise();
      }, 1000);
      // .then().catch(err => {
      //   console.error(err);
      //   this.toastSvc.loader.dismiss();
      // });
    })
  }

  /**
   * checkFwUpdate online
   */
  async checkFwUpdatePromise() {
    this.toastSvc.createInfiniteLoader('固件更新中...');
    this.toastSvc.loader.present();

    try {
      let queryRes = await this.av.queryUpdate('FwUpdate');
      let url = queryRes.get('file').get('url');
      let name = 'DFU.zip'; // depricated data.get('file').get('name');
      const transfer: FileTransferObject = this.transfer.create();
      let entry = await transfer.download(url, this.fs.fs + name);
      let path = entry.toURL();
      this.fs.logSaveInfo('Download DFU file completed: ' + path);
      let res = await this.ble.dfuPrepareFile(path);
      this.startDfu(res[0], res[1], res[2], res[3]);
    } catch (err) {
      this.fs.logSaveErr('checkFwUpdatePromise: ' + JSON.stringify(err));
    }
  }

  private payload;
  startDfu(updateType, sentLength, payload, initPack) {
    this.dfuOver = false;
    this.payload = payload;
    this.ble.isDfuing = true;
    console.log('初始化DFU中...');
    if (this.ble.currentDevice['services'] && this.ble.currentDevice.services.length === 3) {
      // 设备目前正处于dfu模式，直接进行dfu
      this.runDfu(updateType, sentLength, initPack);
    } else {
      // 设备目前处于普通模式，需要重启，以进入dfu模式
      this.ble.readDfu().then(buffer => {
        console.log('Version: ', new Uint8Array(buffer));
        this.ble.listenDfuOn().subscribe(
          () => {},
          err => console.error(err)
        );

        setTimeout(() => {
          this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
            console.log('第1次写入成功: ', [0x01, DfuType[updateType]]);
          }).catch(err => console.error(err));

          setTimeout(() => {
            this.secondConnect(updateType, sentLength, payload, initPack);
          }, 800);
        }, 1000);
      }).catch(err => console.error(err));
    }
  }
  secondConnect(updateType, sentLength, payload, initPack) {
    this.ble.scanDfu().then(deviceId => {
      this.ble.connect(deviceId).subscribe(peripheral => {
        console.log('第2次连上OK: ', peripheral);
        setTimeout(() => {
          this.runDfu(updateType, sentLength, initPack);
        }, 2000);
      }, err => {
        console.error(err);
        if (!this.dfuOver) {
          setTimeout(() => {
            console.log('dfutag二次连接失败，500ms重连');
            this.secondConnect(updateType, sentLength, payload, initPack);
          }, 500);
        }
      });
    }).catch(err => console.error(err));
  }

  runDfu(updateType, sentLength, initPack) {
    console.log('DFU process started >>>');
    this.ble.readDfu().then(buffer => {
      console.log('版本：', new Uint8Array(buffer));
      this.ble.listenDfuOn().subscribe((buffer) => {
        if (buffer) {
          let info = new Uint8Array(buffer);
          console.log('ble 返回的状态：', info);
          switch (info.toString()) {
            case [0x10, 0x01, 0x01].toString():
              console.log('进入第一个noti判断了。');
              this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x00]).buffer).then(()=>{
                console.log('Writing Initialize DFU Parameters...', [2, 0]);
                this.ble.writeDfuPack(initPack.buffer).then(()=>{
                  console.log('Sent header package OK: ', initPack);
                  this.ble.writeDfuCtrl(new Uint8Array([0x02, 0x01]).buffer).then(() => {
                    console.log('Sent cmd OK: ', [2, 1]);
                  }).catch(err => console.error(err));
                }).catch(err => console.error(err));
              }).catch(err => console.error(err));
              break;
            case [0x10, 0x02, 0x01].toString():
                this.ble.writeDfuCtrl(new Uint8Array([0x08, Interval, 0x00]).buffer).then(() => {
                    console.log('Sent cmd OK: ', [0x08, Interval, 0x00]);
                    this.ble.writeDfuCtrl(new Uint8Array([0x03]).buffer).then(()=>{
                        console.log('Sent cmd OK: ', [3]);

                        // ble, 终于要给你丫送数据了，接着吧
                        this.sendPayload();
                    }).catch(err => console.error(err));
                }).catch(err => console.error(err));
                break;
            case [0x10, 0x03, 0x01].toString():
              this.ble.writeDfuCtrl(new Uint8Array([0x04]).buffer).then(()=>{
                console.log('Sent cmd OK: ', [4]);
              }).catch(err => console.error(err));
              break;
            case [0x10, 0x04, 0x01].toString():
              this.ble.writeDfuCtrl(new Uint8Array([0x05]).buffer).then(()=>{
                console.log('Sent cmd OK: ', [5]);
                this.ble.listenDfuOff().then(() => console.log('关闭notify成功，DFU完成'))
                  .catch(err => console.error(err));
              }).catch(err => console.error(err));
              break;

            default:
              if (info[0] === 0x11) {
                let alreadyReceiveLength = info[1] | (info[2] << 8) | (info[3] << 16) | (info[4] << 24);
                console.log('ble报告-已收到：', alreadyReceiveLength, '总：' + sentLength);
                // this.sendPayload();
              } else {
                console.error('dfu出现未识别命令');
              }
              break;
            }
          }
        }, err => console.error(err));

      setTimeout(() => {
        this.ble.writeDfuCtrl(new Uint8Array([0x01, DfuType[updateType]]).buffer).then(() => {
          console.log('第2次写入成功:', [0x01, DfuType[updateType]]);
          let data;
          if (updateType.startsWith('a')) {
            data = new Uint32Array([0, 0, sentLength]);
          } else if(updateType.startsWith('b')) {
            data = new Uint32Array([0, sentLength, 0]);
          }
          this.ble.writeDfuPack(data.buffer).then(() => console.log('Sent length pack OK.', data)).catch(err => console.error(err));
        }).catch(err => console.error(err));
      }, 1000);
    }).catch(err => console.error(err));
  }

  sendPayload () {
    let t1 = Date.now();
    let count = 0;
    let totalLength = this.payload.length;

    let onError = (err) => {
      console.error(err);
    };
    let callBack = () => {

      this.zone.run(() => {
        this.toastSvc.loader.setContent(Math.ceil(count / totalLength * 100) + '%');
      });
      if (!this.payload[count]) {
        console.log('All sent.');
        this.toastSvc.loader.dismiss();
        this.ble.isDfuing = false;
        this.payload = null;
        this.dfuOver = true;
        console.log((Date.now() - t1)/1000 + ' s');
        setTimeout(() => {
          this.ble.startScanForManager();
        }, 2000);
        return;
      }
      let tt1 = Date.now();
      this.ble.writeDfuPackNoResp(this.payload[count++].buffer).then(() => {
        console.log(Date.now() - tt1);
        callBack();
      }).catch(onError);
    };
    this.ble.writeDfuPackNoResp(this.payload[count++].buffer).then(() => callBack()).catch(onError);
  }

}
